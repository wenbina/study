##一：基础
###1：线程与进程
####1：概念
进程：当一个程序被运行，从磁盘加载这个程序的代码至内存，这时就开启了一个进程。
线程：一个进程之内可以分为一到多个线程。
####2：对比
1：进程基本上相互独立的，而线程存在于进程内，是进程的一个子集 进程拥有共享的资源，如内存空间等，供其内部的线程共享
2：线程通信相对简单，因为它们共享进程内的内存，一个例子是多个线程可以访问同一个共享变量 线程更轻量，线程上下文切换成本一般上要比进程上下文切换低

###2：并行并发
####1：概念
并行：是同一时间应对（dealing with）多件事情的能力
并发：是同一时间动手做（doing）多件事情的能力


###3：创建线程方式
####1：通过Thread
Thread t = new Thread("t1") {
            @Override
            public void run() {
                log.debug("running");
            }
        };       
        t.start();
        log.debug("running");
####2：使用Runnable配合Thread
Runnable r = () -> log.debug("running");
new Thread(r, "t1").start();  
####两者对比：
方法1 是把线程和任务合并在了一起
方法2 是把线程和任务分开了，用 Runnable 更容易与线程池等高级 API 配合，用 Runnable 让任务类脱离了 Thread 继承体系，更灵活。
####3：FutureTask 配合 Thread
// 1. 使用 FutureTask 传入 Callable 接口方式创建
        FutureTask<Integer> future = new FutureTask<Integer>(() -> {
            log.debug("running...");
            return 100;
        });
// 2. 传入 future, 因为 FutureTask 这个类是实现了 RunnableFuture 接口，RunnableFuture 继承了 Runnable 接口
        Thread t1 = new Thread(future, "t1");
        t1.start();
####注意：
Future 就是对于具体的 Runnable 或者 Callable 任务的执行结果进行取消、查询是否完成、获取结果。必要时可以通过 get 方法获取执行结果，该方法会阻塞直到任务返回结果。
public interface Future<V> {
	// 取消任务
	boolean cancel(boolean mayInterruptIfRunning);
	// 获取任务执行结果
	V get() throws InterruptedException, ExecutionException;
	// 获取任务执行结果，带有超时时间限制
	V get(long timeout, TimeUnit unit) throws InterruptedException,ExecutionException,TimeoutException;
	// 判断任务是否已经取消
	boolean isCancelled();
	// 判断任务是否已经结束
	boolean isDone();
}
FutureTask 类是 Future 接口和 Runable 接口的实现弥补 runnable 创建线程没有返回值的缺陷

###3：常用方法
####1：star
启动一个新线程，在新线程中运行 run 方法中的代码；
start 方法只是让线程进入就绪状态，里面代码不一定立刻运行，只有当 CPU 将时间片分给线程时，才能进入运行状态，执行代码。每个线程的 start 方法只能调用一次，调用多次就会出现 IllegalThreadStateException
####2：run
新线程启动会调用的方法；
如果在构造 Thread 对象时传递了 Runnable 参数，则线程启动后会调用 Runnable 中的 run 方法，否则默认不执行任何操作。但可以创建 Thread 的子类对象，来覆盖默认行为
####3：join
等待线程运行结束
####4：getId
获取线程长整型的 id
####5：getName
获取线程名
####6：setName(String)
修改线程名
####7：getPriority
获取线程优先级
####8：setPriority(int)
修改线程优先级；
java中规定线程优先级是1~10 的整数，较大的优先级能提高该线程被 CPU 调度的机率
####9：getState
获取线程状态；
Java 中线程状态是用 6 个 enum 表示，分别为：NEW, RUNNABLE, BLOCKED, WAITING, TIMED_WAITING, TERMINATED
####10：isInterrupted
判断是否被打断；
不会清除打断标记
####11：interrupted
判断当前线程是否被打断
会清除打断标记
####12：currentThread
获取当前正在执行的线程
####13：sleep(long n)
让当前执行的线程休眠n毫秒，休眠时让出 cpu 的时间片给其它线程
####14：yield()
提示线程调度器让出当前线程对CPU的使用

###4：方法比较
####1：start与run
使用 start 方式，CPU 会为创建的线程分配时间片，线程进入运行状态，然后线程调用 run 方法执行逻辑。
直接使用 run 的方式，虽然会创建了线程，但是它是直接调用方法，而不是像 start 方式那样触发的，这个线程对象会处一直处在新建状态，run 方法是主线程调用，而不是新线程。
####2：sleep与yield
sleep (使线程阻塞)
调用 sleep 会让当前线程从 Running 进入 Timed Waiting 状态（阻塞）
其它线程可以使用 interrupt 方法打断正在睡眠的线程，这时 sleep 方法会抛出 InterruptedException
睡眠结束后的线程未必会立刻得到执行
yield （让出当前线程）
调用 yield 会让当前线程从 Running 进入 Runnable 就绪状态（仍然有可能被执行），然后调度执行其它线程
线程优先级
线程优先级会提示调度器优先调度该线程，但它仅仅是一个提示，调度器可以忽略它；如果cpu比较忙，那么优先级高的线程会获得更多的时间片，但cpu闲时，优先级几乎没作用
####3：join
用于等待某个线程结束。哪个线程内调用join方法，就等待哪个线程结束，然后再去执行其他线程。
如在主线程中调用t1.join()，则是主线程等待t1线程结束，join 采用同步。
####4：interrupt
如果一个线程在在运行中被打断，打断标记会被置为 true
如果是打断因sleep wait join 方法而被阻塞的线程，会将打断标记置为 false

###4：线程状态
####1：从操作系统划分
####2：从java api层面


##二：线程安全问题
###1：共享模式带来的问题
####1：产生原因
线程出现问题的根本原因是因为线程上下文切换，导致线程里的指令没有执行完就切换执行其它线程了。
####2：临界区
一段代码块内如果存在对共享资源的多线程读写操作，称这段代码块为临界区
####3：竞态条件
多个线程在临界区内执行，由于代码的执行序列不同而导致结果无法预测，称之为发生了竞态条件

###2：解决
1：阻塞式解决方案：synchronized ，Lock
2：非阻塞式解决方案：原子变量

###3：变量的线程安全分析
####1：成员变量和静态变量的线程安全分析
如果变量没有在线程间共享，那么线程对该变量操作是安全的
如果变量在线程间共享
（1）如果只有读操作，则线程安全
（2）如果有读写操作，则这段代码就是临界区，需要考虑线程安全问题




三：synchronized
1：概念
2：原理
reentrantLock


四：锁
五：死锁：
1：概念
2：产生原因
3：解决方式
公平锁与非公平锁
可重入锁
读写锁
自旋锁
乐观锁
悲观锁
锁膨胀
偏向锁
锁消除




六：线程通信
1：虚假唤醒
生产者与消费者


七：线程安全集合
copyOnWriteArrayList
copyOnWriteArraySet
concurrentHashMap


阻塞队列
同步队列


线程池
1：七大核心参数
2：工作原理
3：分类
4：阻塞队列
5：拒绝策略


callable


countdownLatch
cyclicBarrier
semaphore


fork join
aqs
1：概念：用来构建锁或其他同步器组件的重量级基础框架及整个JUC体系的基石，通过内置的FIFO队列来完成资源获取线程的排队工作，
        并通过一个int类型变量表示持有锁的状态




volatile
可见性、原子性、有序性
指令重排
ABA问题
cas
happens-before


守护线程
1：概念
默认情况下，java进程需要等待所有的线程结束后才会停止，但是有一种特殊的线程，叫做守护线程，在其他线程全部结束的时候即使守护线程还未结束代码未执行完java进程也会停止。
普通线程t1可以调用 t1.setDeamon(true); 方法变成守护线程。
注意 垃圾回收器线程就是一种守护线程 Tomcat 中的 Acceptor 和 Poller 线程都是守护线程，所以 Tomcat 接收到 shutdown 命令后，不会等 待它们处理完当前请求

ThreadLocal



Mnitor
1：概念
2：工作原理



