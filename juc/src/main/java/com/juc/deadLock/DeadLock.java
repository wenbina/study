package com.juc.deadLock;

import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/23 9:04
 * 死锁
 */
public class DeadLock {

    // 资源1
    private static final Object RESOURCE1 = new Object();

    // 资源2
    private static final Object RESOURCE2 = new Object();


    public static void main(String[] args) {
        deadLockMethod();
    }

    /**
     * 死锁方法
     * 两个线程竞争共享资源
     * 其中一个线程先获取到一个共享资源A，释放锁时又需要另一个共享资源B
     * 另外一个线程先获取到共享资源B，释放时又需要共享资源A
     * 两个线程都不释放自己占有的资源导致产生死锁
     */
    private static void deadLockMethod() {
        new Thread(() -> {
            synchronized (RESOURCE1) {
                System.out.println(Thread.currentThread().getName() + "获取到资源1");
                try {
                    TimeUnit.SECONDS.sleep(1);
                    synchronized (RESOURCE2) {
                        System.out.println(Thread.currentThread().getName() + "获取到资源2");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"线程1").start();

        new Thread(() -> {
            synchronized (RESOURCE2) {
                System.out.println(Thread.currentThread().getName() + "获取到资源2");
                try {
                    TimeUnit.SECONDS.sleep(1);
                    synchronized (RESOURCE1) {
                        System.out.println(Thread.currentThread().getName() + "获取到资源1");
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        },"线程2").start();
    }
}
