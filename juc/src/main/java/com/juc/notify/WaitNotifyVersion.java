package com.juc.notify;

import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/8 8:40
 * 等待通知案例
 * 需求1：
 * 外卖来了，通知小明小红吃饭，否则继续等待
 * 异常：java.lang.IllegalMonitorStateException
 * wait和notify需要在synchronized中
 */
public class WaitNotifyVersion {

    //外卖是否送到
    private static Boolean flag = false;

    // 外卖：锁对象
    private static final Object TACK_OUT = new Object();

    public static void main(String[] args) {
        waitNotify();
    }


    /**
     * 外卖来了，通知小明小红吃饭，否则继续等待
     * 分析：
     * 1：创建两个线程分别代表小明小红
     * 2：外卖没来调用wait方法进行等待
     * 3：外卖来了调用notifyAll进行唤醒
     */
    private static void waitNotify() {

        // 小明
        new Thread(() -> {
            synchronized (TACK_OUT) {
                while (!flag) {
                    try {
                        System.out.println("【" + Thread.currentThread().getName() + "】：外卖还没到，还是继续打游戏吧");
                        TACK_OUT.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("【" + Thread.currentThread().getName() + "】：外卖到了，可以吃饭了");
            }
        }, "小明").start();

        // 小红
        new Thread(() -> {
            synchronized (TACK_OUT) {
                while (!flag) {
                    try {
                        System.out.println("【" + Thread.currentThread().getName() + "】：外卖还没到，还是继续看电视吧");
                        TACK_OUT.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("【" + Thread.currentThread().getName() + "】：外卖到了，可以吃饭了");
            }
        }, "小红").start();


        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        new Thread(() -> {
            synchronized (TACK_OUT) {
                flag = true;
                // 唤醒小明小红
                TACK_OUT.notifyAll();
            }
        }, "外卖小哥").start();
    }



}
