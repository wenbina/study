package com.juc.notify;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: w
 * @Date: 2021/9/23 8:31
 * 外卖来了通知小明拿外卖
 * 快递来了通知小红拿快递
 */
public class AwaitSignalVersion {

    private final static ReentrantLock LOCK = new ReentrantLock();

    // 外卖
    static Condition takeOutCondition = LOCK.newCondition();

    // 快递
    static Condition deliveryCondition = LOCK.newCondition();

    // 外卖是否到了
    private static Boolean hasTakeOut = false;

    // 快递是否到了
    private static Boolean hasDelivery = false;


    public static void main(String[] args) {
        awaitSignal();
    }

    /**
     * 采用ReentrantLock的精确唤醒
     */
    private static void awaitSignal() {
        new Thread(AwaitSignalVersion::takeOutMethod,"小明").start();
        new Thread(AwaitSignalVersion::deliveryMethod,"小红").start();
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(AwaitSignalVersion::takeOuter,"外卖小哥").start();
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        new Thread(AwaitSignalVersion::deliverier,"快递小哥").start();
    }


    // 外卖方法
    private static void takeOutMethod() {
        try {
            LOCK.lock();
            while (!hasTakeOut) {
                System.out.println(Thread.currentThread().getName() + "外卖还没到，先打打游戏吧...");
                takeOutCondition.await();
            }
            System.out.println(Thread.currentThread().getName() + "外卖到了，还是吃个外卖先吧...");
            LOCK.unlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 快递方法
    private static void deliveryMethod() {
        try {
            LOCK.lock();
            while (!hasDelivery) {
                System.out.println(Thread.currentThread().getName() + "快递还没到，先看看电视吧...");
                deliveryCondition.await();
            }
            System.out.println(Thread.currentThread().getName() + "快递到了，先去取个快递吧...");
            LOCK.unlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 外卖员
    private static void takeOuter() {
        try {
            LOCK.lock();
            hasTakeOut = true;
            takeOutCondition.signal();
            LOCK.unlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 快递员
    private static void deliverier() {
        try {
            LOCK.lock();
            hasDelivery = true;
            deliveryCondition.signal();
            LOCK.unlock();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
