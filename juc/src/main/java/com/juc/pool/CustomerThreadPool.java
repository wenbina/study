package com.juc.pool;

import java.util.concurrent.*;

/**
 * @Author: w
 * @Date: 2021/9/25 12:01
 * 自定义线程池
 */
public class CustomerThreadPool {

    public static void main(String[] args) {
        //customerThreadPoolAbortPolicy(new ThreadPoolExecutor.AbortPolicy());
        //customerThreadPoolAbortPolicy(new ThreadPoolExecutor.CallerRunsPolicy());
        customerThreadPoolAbortPolicy(new ThreadPoolExecutor.DiscardOldestPolicy());
        customerThreadPoolAbortPolicy(new ThreadPoolExecutor.DiscardPolicy());
    }

    /**
     * AbortPolicy：该拒绝策略直接抛出异常
     * CallerRunsPolicy：不抛弃任务也不抛出异常，而是将多余任务返回给调用它的线程
     * DiscardOldestPolicy：抛弃队列中等待最久的任务，然后把当前任务加入队列中尝试再次执行
     * DiscardPolicy：直接丢弃任务，不处理也不抛异常
     */
    private static void customerThreadPoolAbortPolicy(RejectedExecutionHandler handler) {
        ExecutorService pool = new ThreadPoolExecutor(
                2,
                5,
                1L,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(3),
                Executors.defaultThreadFactory(),
                handler);

        // 模拟10个用户办理业务
        try {
            for (int i = 1; i <= 10 ; i++) {
                pool.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "办理业务");
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }


}
