package com.juc.pool;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/23 22:46
 */
public class MyThreadPool {

    public static void main(String[] args) {
        fixedThreadPool();
        singleThreadExecutor();
        cachedThreadPool();
    }

    /**
     * newFixedThreadPool
     * 一池多线程
     */
    private static void fixedThreadPool() {
        ExecutorService pool = Executors.newFixedThreadPool(5);

        try {
            // 模拟10个线程处理业务
            for (int i = 1; i <= 10; i++) {
                pool.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "处理业务");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }

    /**
     * newSingleThreadExecutor
     * 一池一线程
     */
    private static void singleThreadExecutor() {
        ExecutorService pool = Executors.newSingleThreadExecutor();

        try {
            // 模拟10个线程处理业务
            for (int i = 1; i <= 10; i++) {
                pool.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "处理业务");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }

    /**
     * newcachedThreadPool
     * 一池N线程：适用任务多，执行时间短
     */
    private static void cachedThreadPool() {
        ExecutorService pool = Executors.newCachedThreadPool();

        try {
            // 模拟10个线程处理业务
            for (int i = 1; i <= 10; i++) {
                pool.submit(() -> {
                    System.out.println(Thread.currentThread().getName() + "处理业务");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            pool.shutdown();
        }
    }


}
