package com.juc.aqs.customer;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @Author: w
 * @Date: 2021/9/16 8:57
 */
public class TestCustomerLock {

    public static void main(String[] args) {
        ReentrantLock reentrantLock = new ReentrantLock();
        reentrantLock.lock();

        CustomerLock customerLock = new CustomerLock();

        Consumer consumer1 = new Consumer();
        consumer1.setId(1L);
        consumer1.setName("消费者一号");

        Consumer consumer2 = new Consumer();
        consumer2.setId(2L);
        consumer2.setName("消费者二号");

        for (int i = 0; i < 2; i++) {
            customerLock.lock(consumer1);
        }

        customerLock.unlock(consumer1);

        for (int i = 0; i < 2; i++) {
            customerLock.lock(consumer2);
        }
    }
}
