package com.juc.aqs.customer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/9/16 8:38
 * 自定义锁
 * 1：状态位：state
 * 2：持有锁线程
 * 3：队列
 *
 * 功能：可重入，重入后state+1
 *
 * 先不考虑并发安全
 */
public class CustomerLock {

    // 状态位
    private int state = 0;

    // 当前持有锁线程
    private Consumer ownerThread = null;

    // 抢锁失败队列
    private Set<Consumer> threadQueue = new HashSet<>();


    /**
     * 加锁方法
     * 1：抢锁
     * 2：抢锁成功：状态位+1，当前持有锁线程设置为当前线程
     * 3：抢锁失败，进入等待队列中进行等待
     * 4：可重入实现：每获取一次锁状态位+1
     */
    public void lock(Consumer thread) {
        this.robLock(thread);
    }

    /**
     * 释放锁
     */
    public void unlock(Consumer thread) {
        if (state > 1 && ownerThread != null) {
            state --;
            if (state == 0) {
                // 可重入
                ownerThread = null;
            }
        }
    }

    // 抢锁
    private void robLock(Consumer thread) {
        // 判断当前锁是否被抢占
        if (0 == state && ownerThread == null) {
            // 当前锁未被占用
            // 设置状态位
            state++;
            // 设置当前持有锁线程
            ownerThread = thread;
        } else {
            // 判断可重入
            if (thread == ownerThread) {
                state++;
            } else {
                // 当前锁被占用了，放入队列中进行等待
                threadQueue.add(thread);
            }
        }
    }

}
