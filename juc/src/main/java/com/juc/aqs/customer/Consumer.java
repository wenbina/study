package com.juc.aqs.customer;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/9/16 8:59
 */
@Data
public class Consumer implements Serializable {

    private Long id;

    private String name;
}
