package com.juc.producerAndConsumer;

import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/23 9:21
 * 生产者消费者模型
 * 需求：面包工厂生产面包，面包数不为0时需要进行生产，面包数大于0时停止生产
 */
public class ProducerAndConsumer {

    public static void main(String[] args) {
        BreedFactory breedFactory = new BreedFactory();
        while (true) {
            for (int i = 0; i < 2; i++) {
                new Thread(breedFactory::produceBread, "生产者").start();
            }
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < 2; i++) {
                new Thread(breedFactory::consumerBread, "消费者").start();
            }
        }
    }

}

// 面包工厂
class BreedFactory {

    private Integer count = 0;

    private final Object LOCK = new Object();

    // 生产面包
    public void produceBread() {
        try {
            synchronized (LOCK) {
                while (count >= 10) {
                    System.out.println("面包停止生产");
                    LOCK.wait();
                }
                count ++;
                System.out.println(Thread.currentThread().getName() + "面包正在生产，当前面包数为：" + count);
                LOCK.notifyAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void consumerBread() {
        try {
            synchronized (LOCK) {
                while (count == 0) {
                    System.out.println("面包停止消费");
                    LOCK.wait();
                }
                count --;
                System.out.println(Thread.currentThread().getName() + "面包正在消费，当前面包数为：" + count);
                LOCK.notifyAll();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
