package com.juc.basic;

import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/8/30 9:15
 * 常用方法
 */
public class UsualMethods {

    public static void main(String[] args) {
        joinMethod();
    }

    // join方法：等待另一个线程执行完毕
    private static void joinMethod() {
        Thread thread = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("join线程打印");
        }, "join线程");
        try {
            thread.start();
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("主线程打印");
    }
}
