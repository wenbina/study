package com.juc.basic;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.*;

/**
 * @Author: w
 * @Date: 2021/8/30 8:49
 * 创建线程的几种方式
 * 1：通过Thread
 * 2：使用Runnable配合Thread
 * 3：FutureTask 配合 Thread
 * 4：线程池
 */
@Slf4j
public class CreateThread {

    public static void main(String[] args) {
        createByThread();
        createByRunnable();
        createByFutureTask();
        createByThreadPool();
    }

    // 通过Thread方式
    private static void createByThread() {
        new Thread("Thread") {
            @Override
            public void run() {
                //log.debug("通过thread方式创建线程");
                System.out.println("通过thread方式创建线程");
            }
        }.start();
    }

    // 使用runnable配合thread
    private static void createByRunnable() {
        new Thread(() -> {
            System.out.println("通过runnable方式创建线程");
        },"Runnable").start();
    }

    // 使用FutureTask配合Thread
    private static void createByFutureTask() {
        FutureTask<String> task = new FutureTask<>(() -> {
            System.out.println("通过FutureTask方式创建线程");
            return "success";
        });
        new Thread(task,"FutureTask").start();
    }

    // 通过线程池创建
    private static void createByThreadPool() {
        ExecutorService pool = Executors.newCachedThreadPool();
        Future<String> task = pool.submit(() -> {
            System.out.println("通过线程池方式创建线程");
            return "success";
        });
    }
}
