package com.juc.blockingQueue;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/22 22:23
 */
public class BlockingQueueMethods {

    public static void main(String[] args) throws InterruptedException {
        addAndRemove();
        offerAndPoll();
        putAndTake();
        offerTimeAndPollTime();
    }

    /**
     * 该方法在阻塞队列满时add元素、阻塞队列空时remove元素会抛异常
     */
    private static void addAndRemove() {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        // 添加元素
        System.out.println(blockingQueue.add("a"));
        System.out.println(blockingQueue.add("b"));
        System.out.println(blockingQueue.add("c"));
        //抛出异常
        //System.out.println(blockingQueue.add("d"));

        // 移除元素
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        //抛出异常
        //System.out.println(blockingQueue.remove());

        // 先进先出：先拿到队首元素
        System.out.println(blockingQueue.peek());
    }

    /**
     * 插入方法，成功返回true，失败返回false
     * 移除方法，成功返回队列元素，队列里没元素就返回null
     */
    private static void offerAndPoll() {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        // 添加元素
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));
        //不抛出异常，返回false
        System.out.println(blockingQueue.offer("d"));

        // 移除元素
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        //不抛出异常，返回false
        System.out.println(blockingQueue.poll());

        // 拿到队首元素，无元素会抛异常
        //System.out.println(blockingQueue.element());
    }


    /**
     * 当阻塞队列满时，生产者线程继续往队列里put元素时，队列会一直阻塞生产线程直到put数据或响应中断退出
     * 当阻塞队列为空时，消费者线程试图从队列里take元素，队列会一直阻塞消费者线程直到队列可用为止
     */
    private static void putAndTake() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        // 添加元素：队列满时线程会阻塞
        blockingQueue.put("a");
        blockingQueue.put("b");
        blockingQueue.put("c");
        //blockingQueue.put("c");

        // 移除元素：队列空时线程会阻塞
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        //System.out.println(blockingQueue.take());
    }

    /**
     *
     */
    private static void offerTimeAndPollTime() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<>(3);
        // 添加元素：时间到了就线程退出
        blockingQueue.offer("a",1, TimeUnit.SECONDS);
        blockingQueue.offer("b",1, TimeUnit.SECONDS);
        blockingQueue.offer("c",1, TimeUnit.SECONDS);
        blockingQueue.offer("d",1, TimeUnit.SECONDS);

        // 移除元素：时间到了就线程退出
        System.out.println(blockingQueue.poll(1, TimeUnit.SECONDS));
        System.out.println(blockingQueue.poll(1, TimeUnit.SECONDS));
        System.out.println(blockingQueue.poll(1, TimeUnit.SECONDS));
        System.out.println(blockingQueue.poll(1, TimeUnit.SECONDS));
    }

}
