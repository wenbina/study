package com.basic.string.stringStudy.apply;

/**
 * @Author: w
 * @Date: 2021/7/28 23:10
 */
public class CompareString {

    public static void main(String[] args) {
        Person person1 = new Person();
        person1.setName("zs");
        Person person2 = new Person();
        person2.setName("zs");

        // true：person1.getName()指向常量池zs地址，person2.getName()也是 所以为true
        System.out.println(person1.getName() == person2.getName());

        // true：person1.getName()指向常量池zs地址，zs就是常量值地址 所以为true
        System.out.println("zs" == person1.getName()); // true
    }


}

class Person {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
