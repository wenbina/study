package com.basic.string.stringStudy.methods;

/**
 * @Author: w
 * @Date: 2021/7/29 17:50
 * 字符串常用方法
 */
public class StrMethods {

    public static void main(String[] args) {
        String str = "hello";
        strChatAtMethod(str);
        strConcatMethod(str);
        strContainsMethod(str);
        strEqualsIgnoreCaseMethod(str);
        strIndexOfMethod(str);
        strLastIndexOfMethod(str);
        strReplaceMethod(str);
        strReplaceAllMethod(str);
        strSubstringMethod(str);
        strTrimMethod(str);
    }


    /**
     * 返回字符串所在下标的值
     */
    private static void strChatAtMethod(String str) {
        System.out.println(str.charAt(2));
    }

    /**
     * 字符串连接：将指定的字符串连接到该字符串的末尾
     */
    private static void strConcatMethod(String str) {
        System.out.println(str.concat("拼接..."));
    }

    /**
     * 字符串包含方法：当且仅当此字符串包含指定的char值序列时才返回true。
     */
    private static void strContainsMethod(String str) {
        System.out.println(str.concat("hello"));
    }

    /**
     * 忽略大小写比较两个字符串内容是否相等
     */
    private static void strEqualsIgnoreCaseMethod(String str) {
        System.out.println("Hello".equalsIgnoreCase(str));
    }

    /**
     * 返回指定字符第一次出现的字符串内的索引
     */
    private static void strIndexOfMethod(String str) {
        System.out.println(str.indexOf("l"));
    }

    /**
     * 返回指定字符的最后一次出现的字符串中的索引
     */
    private static void strLastIndexOfMethod(String str) {
        System.out.println(str.lastIndexOf("l"));
    }

    /**
     * 字符串替换
     */
    private static void strReplaceMethod(String str) {
        String replaceStr = str.replace("h", "k");
        System.out.println(replaceStr);
    }

    /**
     * 字符串替换全部
     */
    private static void strReplaceAllMethod(String str) {
        String replaceAllStr = str.replaceAll("l", "i");
        System.out.println(replaceAllStr);
    }

    /**
     * 字符串切割
     */
    private static void strSubstringMethod(String str) {
        String substringStr = str.substring(0, 5);
        System.out.println(substringStr);
    }

    /**
     * 字符串去空格
     */
    private static void strTrimMethod(String str) {
        str = " hello ";
        String trim = str.trim();
        System.out.println(trim);
    }




}
