package com.basic.string.stringBufferStudy.basic;

/**
 * @Author: w
 * @Date: 2021/7/29 22:23
 */
public class StringBufferBasic {

    public static void main(String[] args) {
        /**
         * 1：StringBuffer的直接父类是AbstractStringBuilder
         * 2：StringBuffer实现了Serializable，即StringBuffer得对象可以串行化
         * 3：在父类中AbstractStringBuilder有属性 char[] value，不是final；该value数组存放字符串内容，引用存放在堆中
         * 4：StringBuffer是一个final类，不能被继承
         * 5：因为StringBufefr字符串内容是存在char[] value，所有变化不用每次更换地址，所以效率高于String
          */
        //StringBuffer sb = new StringBuffer("hello");
        //constructMethod();
        transToString();
    }


    /**
     * StringBuffer的构造方法
     */
    private static void constructMethod() {

        // StringBuffer构造器默认会创建一个大小为16的char[]，用于存放字符串内容
        StringBuffer sb1 = new StringBuffer();

        // 也可以通过构造器指定char[]大小
        StringBuffer sb2 = new StringBuffer(100);

        // 通过给一个String创建StringBuffer；char[]大小就是字符串长度 + 16
        StringBuffer sb3 = new StringBuffer("hello");
    }

    /**
     * SringBuffer转String
     */
    private static void transToString() {

        // 方式1：通过toString方法
        StringBuffer sb1 = new StringBuffer("hello");
        String str1 = sb1.toString();

        // 方式2：通过String的构造器
        String str2 = new String(sb1);

        System.out.println(str1);
        System.out.println(str2);
    }
}
