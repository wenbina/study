StringBuffer
1：概念
（1）线程安全，可变的字符序列。 字符串缓冲区就像一个String ，但可以修改。字符串缓冲区可以安全地被多个线程使用。
（2）StringBuffer的主要StringBuffer是append和insert方法，它们被重载以便接受任何类型的数据。 每个都有效地将给定的数据转换为字符串，然后将该字符串的字符附加或插入到字符串缓冲区。 
    append方法总是在缓冲区的末尾添加这些字符; insert方法将insert添加到指定点。 
（3）每个字符串缓冲区都有一个容量。 只要字符串缓冲区中包含的字符序列的长度不超过容量，就不必分配新的内部缓冲区数组。 如果内部缓冲区溢出，则会自动变大。 
（4）除非另有说明，否则将null参数传递给null中的构造函数或方法将导致抛出NullPointerException 。

2：构造方法
（1）无参构造：构造一个没有字符的字符串缓冲区，初始容量为16个字符。 
    StringBuffer() 
（2）构造一个没有字符的字符串缓冲区和指定的初始容量。
    StringBuffer(int capacity) 
 (3) 构造一个初始化为指定字符串内容的字符串缓冲区: 字符串缓冲区的初始容量为16加上字符串参数的长度。 
    StringBuffer(String str)
    
3：相关知识
（1）StringBuffer是一个final类，不能被继承；它的父类是AbstractStringBuilder：有属性 char[] value，不是final；该value数组存放字符串内容，引用存放在堆中
（2）因为StringBuffer字符串内容是存在char[] value，所有变化不用每次更换地址，所以效率高于String


    
4：注意点：
（1）StringBuffer可以append一个null字符串，底层会追加为null；但是通过有参构造方法传入一个null值则会报空指针异常

 
 

