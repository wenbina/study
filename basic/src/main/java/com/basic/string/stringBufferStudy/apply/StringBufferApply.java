package com.basic.string.stringBufferStudy.apply;

/**
 * @Author: w
 * @Date: 2021/7/29 22:53
 * StringBuffer使用
 */
public class StringBufferApply {

    public static void main(String[] args) {
        apply1();
    }

    /**
     * 1：
     * StringBuffer sb = new StringBuffer();
     * sb.append(str);
     *   if (str == null)
     *     return appendNull();
     *   private AbstractStringBuilder appendNull() {
     *         int c = count;
     *         ensureCapacityInternal(c + 4);
     *         final char[] value = this.value;
     *         value[c++] = 'n';
     *         value[c++] = 'u';
     *         value[c++] = 'l';
     *         value[c++] = 'l';
     *         count = c;
     *         return this;
     *     }
     *  当字符串为null时，StringBuffer通过append方法会将null转成null字符数组进行追加
     *
     *  2：
     *  StringBuffer sb1 = new StringBuffer(str);
     *  super(str.length() + 16);
     *         append(str);
     *  由于先会调用str.length()；str为null，所以抛出空指针异常
     */
    private static void apply1() {
        String str = null;
        StringBuffer sb = new StringBuffer();
        sb.append(str);
        System.out.println(sb.length());
        System.out.println(sb);
        StringBuffer sb1 = new StringBuffer(str);
        System.out.println(sb1);
    }
}
