package com.basic.string.stringBufferStudy.methods;

/**
 * @Author: w
 * @Date: 2021/7/29 22:45
 * StringBuffer常见方法
 */
public class StringBufferMethods {

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer("hello");

        appendMethod(sb);
        deleteMethod(sb);
        replaceMethod(sb);
        indexOfMethod(sb);
        insertMethod(sb);
        lengthMethod(sb);
    }

    /**
     * 追加
     */
    private static void appendMethod(StringBuffer sb) {
        sb.append(",java");
        System.out.println(sb.toString());
    }

    /**
     * 删除：删除索引0~2处的字符串[0,2)
     */
    private static void deleteMethod(StringBuffer sb) {
        sb.delete(0, 2);
        System.out.println(sb.toString());
    }

    /**
     * 修改：替换[0,2)处的字符串
     */
    private static void replaceMethod(StringBuffer sb) {
        sb.replace(0,2,"hah");
        System.out.println(sb.toString());
    }

    /**
     * 查：查找指定子串在字符串第一次出现的索引，找不到就返回-1
     */
    private static void indexOfMethod(StringBuffer sb) {
        System.out.println(sb.indexOf("l"));
    }

    /**
     * 插入：在索引为2的位置插入字符串，原理索引为2的内容自动后移
     */
    private static void insertMethod(StringBuffer sb) {
        sb.insert(2,"g");
        System.out.println(sb.toString());
    }

    /**
     * 长度
     */
    private static void lengthMethod(StringBuffer sb) {
        System.out.println(sb.length());
    }


}
