package com.basic.classAndObject.finalStudy.apply;

/**
 * @Author: w
 * @Date: 2021/7/26 22:48
 * 计算面积
 * 需求：计算圆形面积，要求圆周率为3.14；
 *
 * 赋值方式：定义时；构造方法；代码块
 */
public class CalculateArea {

    // 圆周率：定义时
    //private final static Double VALUE = 3.14;

    private final Double VALUE;

    // 代码块
    /*{
        VALUE = 3.14;
    }*/

    // 构造方法
    public CalculateArea() {
        VALUE = 3.14;
    }

    private Double calculate(Double r) {
        return VALUE * r * r;
    }

}
