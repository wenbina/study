package com.basic.reflection.basic.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/9 22:40
 */
@Data
public class Cat {

    private String name;

    public void cry() {
        System.out.println("小猫叫...");
    }
}
