package com.basic.reflection.basic.service;

import com.basic.reflection.basic.entity.Cat;

import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.Properties;

/**
 * @Author: w
 * @Date: 2021/8/9 22:46
 * 反射案例：通过读取配置文件，获取需要的类以及调用配置文件中的方法
 * 好处：遵循开闭原则，可以在不修改源码的情况下，修改配置文件实现调用不同的方法
 */
public class ReflectionDemo {

    public static void main(String[] args) throws Exception{

        // 读取配置文件
        Properties properties = new Properties();
        properties.load(new FileInputStream("E:\\ownspace\\study\\basic\\src\\main\\java\\com\\basic\\reflection\\basic\\re.properties"));
        // 获取类的全路径
        String classfullpath = properties.get("classfullpath").toString();
        // 获取类的方法
        String method = properties.get("method").toString();

        // 通过反射机制调用方法
        // 加载类：返回Class类型的对象
        Class cla = Class.forName(classfullpath);
        // 通过cla对象得到需要加载的实例对象
        Cat cat = (Cat)cla.newInstance();
        // 通过cla对象得到需要调用的方法
        Method cry = cla.getMethod(method);
        // 调用方法
        cry.invoke(cat);
    }
}
