package com.basic.reflection.basic.getClass;

/**
 * @Author: w
 * @Date: 2021/8/26 22:40
 * 获取class对象
 * 1：使用类的class属性来获取该类对应的Class对象。举例: Student.class将 会返回Student类对应的Class对象
 * 2：调用对象的getClass方法，返回该对象所属类对应的Class对象该方法是Object类中的方法，所有的Java对象都可以调用该方法
 * 3：使用Class类中的静态方法forName(StringclassName),该方法需要传入字符串参数，该字符串参数的值是某个类的全路径，也就是完整包名的路径
 *
 * 一个类在内存中只有一个字节码文件
 */
public class GetClass {

    public static void main(String[] args) throws ClassNotFoundException {
        // 方式一：通过class对象获取
        Class c1 = Student.class;

        // 方式二：通过getClass获取
        Student s = new Student();
        Class c2 = s.getClass();

        // 方式三：通过forName获取
        Class c3 = Class.forName("com.basic.reflection.basic.getClass.Student");

        System.out.println(c1.hashCode());
        System.out.println(c2.hashCode());
        System.out.println(c3.hashCode());


    }
}

class Student {

    private String name;

    int age;

    public String address;
}
