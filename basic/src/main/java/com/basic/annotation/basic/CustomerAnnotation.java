package com.basic.annotation.basic;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @Author: w
 * @Date: 2021/8/25 22:21
 * 自定义注解
 */
@MyAnnotation
public class CustomerAnnotation {

    @MyAnnotation1(age = 3)
    public void test() {

    }

}

// 表示我们的注解可以用在什么地方
@Target(value = {ElementType.TYPE,ElementType.METHOD})
// 表示我们的注解在什么地方有效
@Retention(value = RetentionPolicy.RUNTIME)
@interface MyAnnotation {

}

@Target(value = {ElementType.TYPE,ElementType.METHOD})
// 表示我们的注解在什么地方有效
@Retention(value = RetentionPolicy.RUNTIME)
@interface MyAnnotation1 {

    // 注解参数：参数类型 + 参数名()
    String name() default "";

    int age();

    long id() default -1; // 如果默认值为-1，代表不存在

    String[] schools() default {"清华","北大"};
}
