package com.basic.annotation.reflection;

/**
 * @Author: w
 * @Date: 2021/8/25 23:04
 * class类的创建方式
 */
public class ClassDemo {

    public static void main(String[] args) throws ClassNotFoundException {
        Person person = new Student();
        System.out.println("这个人是：" + person.name);

        // 方式一：通过对象获得
        Class cla1 = person.getClass();

        // 方式二：forName获取
        Class cla2 = Class.forName("com.basic.annotation.reflection.Student");

        // 方式三：通过类名.class获得
        Class cla3 = Student.class;

        System.out.println(cla1.hashCode());
        System.out.println(cla2.hashCode());
        System.out.println(cla3.hashCode());

    }
}

class Person {

    public String name;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
    }
}

class Student extends Person {
   public Student() {
       this.name = "学生";
   }
}

class Teacher extends Person {
    public Teacher() {
        this.name = "老师";
    }
}
