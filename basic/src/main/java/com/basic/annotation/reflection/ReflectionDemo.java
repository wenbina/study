package com.basic.annotation.reflection;

/**
 * @Author: w
 * @Date: 2021/8/25 22:52
 */
public class ReflectionDemo {

    public static void main(String[] args) throws ClassNotFoundException {
        // 通过反射获取类的class对象
        Class cla = Class.forName("com.basic.annotation.reflection.User");
        System.out.println("Class对象：" + cla);

        // 一个类只有一个class对象；一个类被加载后类的整个结构都会被封装在Class对象中
        Class cla1 = Class.forName("com.basic.annotation.reflection.User");
        Class cla2 = Class.forName("com.basic.annotation.reflection.User");
        Class cla3 = Class.forName("com.basic.annotation.reflection.User");
        System.out.println(cla1.hashCode());
        System.out.println(cla2.hashCode());
        System.out.println(cla3.hashCode());

    }
}

class User {
    private String username;

    private Integer age;
}
