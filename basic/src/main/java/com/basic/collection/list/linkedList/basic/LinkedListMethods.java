package com.basic.collection.list.linkedList.basic;

import java.util.LinkedList;

/**
 * @Author: w
 * @Date: 2021/8/1 11:17
 * LinkedList常用方法
 */
public class LinkedListMethods {

    public static void main(String[] args) {
        addMethod();
        addIndexMethod();
        addFirstMethod();
        addLastMethod();
        getMethod();
        getFirstMethod();
        getLastMethod();
        removeMethod();
        removeFirstMethod();
        removeLastMethod();
    }

    /**
     * add：将指定的元素追加到此列表的末尾
     */
    private static void addMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.add("你好");
        String methodName = "=============add方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    /**
     * add：在此列表中的指定位置插入指定的元素。
     */
    private static void addIndexMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.add(0,"哈哈哈");
        String methodName = "=============addIndex方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    /**
     * addFirst：在该列表开头插入指定的元素。
     */
    private static void addFirstMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.addFirst("哈哈哈");
        String methodName = "=============addFirst方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    /**
     * addLast：将指定的元素追加到此列表的末尾。
     */
    private static void addLastMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.addLast("哈哈哈");
        String methodName = "=============addLast方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    /**
     * get：返回此列表中指定位置的元素。
     */
    private static void getMethod() {
        LinkedList<String> strings = initLinkedList();
        System.out.println("=============get方法===========");
        System.out.println(strings.get(0));
    }

    /**
     * getFirst：返回此列表中的第一个元素。
     */
    private static void getFirstMethod() {
        LinkedList<String> strings = initLinkedList();
        System.out.println("=============get方法===========");
        System.out.println(strings.getFirst());
    }

    /**
     * getLast：返回此列表中的最后一个元素。
     */
    private static void getLastMethod() {
        LinkedList<String> strings = initLinkedList();
        System.out.println("=============get方法===========");
        System.out.println(strings.getLast());
    }

    /**
     * remove：检索并删除此列表的头（第一个元素）。
     */
    private static void removeMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.add("哈哈哈");
        strings.remove();
        String methodName = "=============remove方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    /**
     * removeFirst：从此列表中删除并返回第一个元素。
     */
    private static void removeFirstMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.add("哈哈哈");
        strings.removeFirst();
        String methodName = "=============removeFirst方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    /**
     * removeLast：从此列表中删除并返回最后一个元素。
     */
    private static void removeLastMethod() {
        LinkedList<String> strings = initLinkedList();
        strings.add("哈哈哈");
        strings.removeLast();
        String methodName = "=============removeLast方法===========";
        templatePrintlnMethod(strings,methodName);
    }

    // 初始化新集合
    private static LinkedList<String> initLinkedList() {
        LinkedList<String> strings = new LinkedList<>();
        strings.add("你好");
        return strings;
    }

    // 模板输出方法
    private static void templatePrintlnMethod(LinkedList<String> linkedList,String methodName) {
        System.out.println(methodName);
        System.out.println(linkedList);
    }



}
