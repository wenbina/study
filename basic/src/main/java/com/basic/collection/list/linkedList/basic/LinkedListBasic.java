package com.basic.collection.list.linkedList.basic;

import java.util.LinkedList;

/**
 * @Author: w
 * @Date: 2021/8/1 11:16
 */
public class LinkedListBasic {

    public static void main(String[] args) {

        /**
         * 1：无参构造器：初始化一个没有容量的Node对象
         *     public LinkedList() {
         *     }
         */
        LinkedList<String> strings = new LinkedList<>();

    }
}
