一：ArrayList源码分析
1：构造方法
（1）无参构造：会默认初始化为一个空数组
    // 变量
    private static final Object[] DEFAULTCAPACITY_EMPTY_ELEMENTDATA = {};
    // 构造方法
    public ArrayList() {
        // 将空数组赋给集合
        this.elementData = DEFAULTCAPACITY_EMPTY_ELEMENTDATA;
    }
    
    
（2）带容量构造方法：会初始化一个传入容量大小的数组
    // 变量创建一个空数组
    private static final Object[] EMPTY_ELEMENTDATA = {};
    // 构造方法
    public ArrayList(int initialCapacity) {
        if (initialCapacity > 0) {
            // 当传入容量大于0的时候会创建一个传入容量大小的数组
            this.elementData = new Object[initialCapacity];
        } else if (initialCapacity == 0) {
            // 传入容量为0则相当
            this.elementData = EMPTY_ELEMENTDATA;
        } else {
            throw new IllegalArgumentException("Illegal Capacity: " + initialCapacity);
        }
    }
    
    
2：add方法
    // add方法
    public boolean add(E e) {
        // 确定容量
        ensureCapacityInternal(size + 1);  // Increments modCount!!
        // 集合大小加1
        elementData[size++] = e;
        return true;
    }
    // 确定容量方法
    private void ensureCapacityInternal(int minCapacity) {
            ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
    }
    // 确定扩容方法
    private void ensureExplicitCapacity(int minCapacity) {
            // modCount：指集合的修改次数
            modCount++;
            if (minCapacity - elementData.length > 0)
                // 若集合需要的最小容量大于集合现在的大小则进行扩容
                grow(minCapacity);
    }
    // 计算容量方法
    private static int calculateCapacity(Object[] elementData, int minCapacity) {
            if (elementData == DEFAULTCAPACITY_EMPTY_ELEMENTDATA) {
                // 返回一个默认容量与集合所需最小容量的最大值
                return Math.max(DEFAULT_CAPACITY, minCapacity);
            }
            return minCapacity;
    }
    // 扩容方法
    private void grow(int minCapacity) {
            // 将集合大小赋值给一个变量（旧容量）
            int oldCapacity = elementData.length;
            // 新容量为：旧容量*2+1，也就是原容量的1.5倍
            int newCapacity = oldCapacity + (oldCapacity >> 1);
            if (newCapacity - minCapacity < 0)
                // 扩容后的容量大于最小容量：扩容容量不变
                newCapacity = minCapacity;
            if (newCapacity - MAX_ARRAY_SIZE > 0)
                // 扩容容量大于最大集合容量时
                newCapacity = hugeCapacity(minCapacity);
            // 数组复制并扩容方法：扩容后多出的数据默认为null
            elementData = Arrays.copyOf(elementData, newCapacity);
    }
    
    
3：remove方法
    public E remove(int index) {
        rangeCheck(index);
        // 修改次数增加
        modCount++;
        // 将移除元素记录下来
        E oldValue = elementData(index);
        int numMoved = size - index - 1;
        // 防止移除数据下标越界
        if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index, numMoved);
        elementData[--size] = null; // clear to let GC do its work
        return oldValue;
    }
    

