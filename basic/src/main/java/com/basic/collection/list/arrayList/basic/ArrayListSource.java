package com.basic.collection.list.arrayList.basic;

import java.util.ArrayList;

/**
 * @Author: w
 * @Date: 2021/7/31 16:34
 * ArrayList源码
 */
public class ArrayListSource {

    public static void main(String[] args) {
        createArrayList();
    }

    /**
     * ArrayList创建
     */
    private static void createArrayList() {
        // 调用无参构造方法创建ArrayList
        ArrayList<String> strings = new ArrayList<>();

        // 往集合中添加元素，List会进行扩容
        for (int i = 0; i < 10; i++) {
            strings.add("数据" + (i+1));
        }

        strings.remove(1);
    }


}
