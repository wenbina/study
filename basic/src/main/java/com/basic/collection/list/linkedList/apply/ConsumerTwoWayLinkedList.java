package com.basic.collection.list.linkedList.apply;

/**
 * @Author: w
 * @Date: 2021/8/1 11:49
 * 自定义双向链表
 */
public class ConsumerTwoWayLinkedList {

    public static void main(String[] args) {
        // 创建三个节点并连接起来
        Node node1 = new Node("张三");
        Node node2 = new Node("李四");
        Node node3 = new Node("王五");

        node1.next = node2;
        node2.next = node3;
        node3.prev = node2;
        node2.prev = node1;

        // 首节点执行node1
        Node first = node1;
        // 尾节点指向node3
        Node last = node3;

        // 从头到尾遍历
        while (true) {
            if (first == null) {
                break;
            }
            // 输出first
            System.out.println(first.item);
            first = first.next;
        }

        // 从尾到头遍历
        while (true) {
            if (last == null) {
                break;
            }
            // 输出last
            System.out.println(last.item);
            last = last.prev;
        }

        // 添加Node
        Node node4 = new Node("刘六");
        node3.next = node4;
        node4.prev = node3;
        first = node1;
        while (true) {
            if (first == null) {
                break;
            }
            // 输出first
            System.out.println(first.item);
            first = first.next;
        }



    }


}

/**
 * Node类：表示双向链表的一个节点
 */
class Node {

    public Object item;  // 用于存放数据

    public Node prev;  // 指向上一个节点

    public Node next;  // 指向下一个节点

    public Node(Object item) {
        this.item = item;
    }
}
