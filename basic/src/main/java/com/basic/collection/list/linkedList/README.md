1：LinkedList基本介绍
（1）LinkedList底层实现了双向链表，他在里面维护了两个属性first和last分别指向首节点和尾节点
（2）每个节点（Node对象），里面又维护了prev、next、item三个属性，其中通过prev指向前一个，通过next指向后一个节点。最终实现双向链表
（3）所以LinkedList的元素添加和删除不是通过数组完成的，相对来说效率较高
（4）可以添加任意元素（元素可以重复），包括null
（5）线程不安全

2：LinkedList的常用方法
add：将指定的元素追加到此列表的末尾
add：在此列表中的指定位置插入指定的元素。
addFirst：在该列表开头插入指定的元素。
addLast：将指定的元素追加到此列表的末尾。
get：返回此列表中指定位置的元素。
getFirst：返回此列表中的第一个元素。
getLast：返回此列表中的最后一个元素。
remove：检索并删除此列表的头（第一个元素）。
removeFirst：从此列表中删除并返回第一个元素。
removeLast：从此列表中删除并返回最后一个元素。

3：源码分析
注意：由于LinkedList底层不是数组结构，所以不存在扩容说法：增加和删除是通过修改底层维护的prev和next属性来实现的
（1）构造器
    // 无参构造器：初始化一个没有容量的Node对象
    public LinkedList() {
    }
