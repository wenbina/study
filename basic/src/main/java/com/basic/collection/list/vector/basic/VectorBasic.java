package com.basic.collection.list.vector.basic;

import java.util.Vector;

/**
 * @Author: w
 * @Date: 2021/7/31 22:04
 *
 */
public class VectorBasic {

    public static void main(String[] args) {
        createVector();
    }

    /**
     * 创建Vector
     */
    private static void createVector() {
        Vector<String> vector = new Vector<>(1);
        for (int i = 0; i < 2; i++) {
            vector.add("你好啊" + (i+1));
        }
    }
}
