package com.basic.collection.list.arrayList.basic;

import java.util.ArrayList;

/**
 * @Author: w
 * @Date: 2021/7/31 16:25
 */
public class ArrayListBasic {

    public static void main(String[] args) {
        arrayListAdd();
    }


    /**
     * ArrayList可以添加重复的、或者是null数据
     */
    private static void arrayListAdd() {
        ArrayList<String> strings = new ArrayList<>();
        strings.add("集合");
        strings.add("集合");
        strings.add(null);

        for (String string : strings) {
            System.out.println(string);
        }
    }
}
