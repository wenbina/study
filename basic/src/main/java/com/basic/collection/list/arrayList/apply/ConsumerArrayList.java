package com.basic.collection.list.arrayList.apply;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.List;

/**
 * @Author: w
 * @Date: 2021/7/31 23:54
 * 自定义ArrayList
 */
public class ConsumerArrayList<E> extends AbstractList<E> implements List<E> {

    // 默认容量
    private static final int DEFAULT_CAPACITY = 10;

    // 构造方法默认创建数组
    private static final Object[] DEFAULT_EMPTY_ELEMENT_DATA = {};

    // ArrayList底层数据结构：数组
    transient Object[] elementData;

    // 大小
    private int size;

    // 数组改变次数
    private static int modCount;

    /**
     * 无参构造方法
     * 默认创建容量为0的数组
     */
    public ConsumerArrayList() {
        this.elementData = DEFAULT_EMPTY_ELEMENT_DATA;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public E get(int index) {
        return null;
    }

    /**
     * 添加数据
     * 1：确保集合容量是否够
     * 2：将需要添加的数据加入到集合中
     */
    public boolean add(E e) {
        this.ensureCapacityInternal(size + 1);
        // 将添加值放入数组
        elementData[size++] = e;
        return true;
    }

    /**
     * 确保容量是否够
     * 判断是否需要进行扩容：1：先计算添加进的元素所需容量，然后调用是否需要进行扩容方法
     */
    private void ensureCapacityInternal(int minCapacity) {
        // 是否需要进行扩容
        ensureExplicitCapacity(calculateCapacity(elementData,minCapacity));
    }

    /**
     * 是否需要进行扩容
     */
    private void ensureExplicitCapacity(int minCapacity) {
        // 记录数组改变次数
        modCount ++;
        // 如果数组所需最小容量大于当前数组容量就进行扩容
        if (minCapacity > elementData.length)
            grow(minCapacity);
    }

    /**
     * 计算集合最小容量
     * 判断需要计算容量的数组是否与当前创建的数组一样
     */
    private static int calculateCapacity(Object[] elementData, int minCapacity) {
        if (elementData == DEFAULT_EMPTY_ELEMENT_DATA) {
            // 是的话：比较当前数组需要容量minCapacity与默认容量大小，返回更大值
            return Math.max(DEFAULT_CAPACITY,minCapacity);
        }
        return minCapacity;
    }

    /**
     * 扩容
     */
    private void grow(int minCapacity) {
        // 将当前数组大小存到一个临时变量中
        int oldCapacity = elementData.length;
        // 计算扩容量：扩容为原来的1.5倍
        int newCapacity = oldCapacity + (oldCapacity >> 1);
        // 比较扩容量与数组所需的最小容量
        if (newCapacity < minCapacity) {
            // 扩容量小于数组所需的最小容量：将数组所需的最小容量赋给扩容量值
            newCapacity = minCapacity;
        }
        // 将原数组复制到新数组，扩大容量的数据默认给null；
        elementData = Arrays.copyOf(elementData,newCapacity);
    }



}
