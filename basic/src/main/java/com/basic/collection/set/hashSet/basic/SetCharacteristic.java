package com.basic.collection.set.hashSet.basic;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @Author: w
 * @Date: 2021/8/1 16:25
 * set的特点
 * （1）不能存放重复元素
 * （2）可以存放null
 * （3）存放数据是无序的
 */
public class SetCharacteristic {

    public static void main(String[] args) {
        //characteristic();
        traversalSet();
    }

    /**
     * set特点
     */
    private static void characteristic() {
        HashSet<String> strings = new HashSet<>();

        strings.add("你好");
        strings.add("你好");
        strings.add("集合");
        strings.add("学习");
        strings.add(null);

        System.out.println(strings);
    }

    /**
     * 遍历set
     * 方式一：使用迭代器
     * 方式二：增强for：底层是迭代器
     */
    private static void traversalSet() {
        HashSet<String> strings = new HashSet<>();
        strings.add("你好");
        strings.add("集合");
        strings.add("学习");

        // 方式一：迭代器
        Iterator<String> iterator = strings.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        // 方式二：增强for：底层是迭代器
        for (String string : strings) {
            System.out.println(string);
        }
    }
}
