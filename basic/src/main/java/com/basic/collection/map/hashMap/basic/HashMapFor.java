package com.basic.collection.map.hashMap.basic;

import java.util.*;

/**
 * @Author: w
 * @Date: 2021/8/2 22:37
 * 遍历HashMap的方法
 */
public class HashMapFor {

    public static void main(String[] args) {
        method1();
        method2();
        method3();
        method4();
        method5();
        method6();
    }

    /**
     * 方法一：
     * 先取出所有key，然后通过增强for循环遍历
     */
    private static void method1() {
        Map<String, Object> map = initHashMap();
        // 取出所有key值
        Set<String> keySet = map.keySet();
        for (String s : keySet) {
            System.out.println(s);
        }
    }

    /**
     * 方法二：使用迭代器取出
     */
    private static void method2() {
        Map<String, Object> map = initHashMap();
        // 取出所有key值
        Set<String> keySet = map.keySet();
        Iterator<String> iterator = keySet.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    /**
     * 方法三：取出全部value进行增强for循环遍历
     */
    private static void method3() {
        Map<String, Object> map = initHashMap();
        // 取出所有key值
        Collection<Object> values = map.values();
        for (Object value : values) {
            System.out.println(value);
        }
    }

    /**
     * 方法四：取出全部value进行迭代器遍历
     */
    private static void method4() {
        Map<String, Object> map = initHashMap();
        // 取出所有key值
        Collection<Object> values = map.values();
        Iterator<Object> iterator = values.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }


    /**
     * 方法五：通过entrySet获取
     */
    private static void method5() {
        Map<String, Object> map = initHashMap();
        Set<Map.Entry<String, Object>> entries = map.entrySet();
        for (Map.Entry<String, Object> entry : entries) {
            System.out.println(entry.getKey() + "---" + entry.getValue());
        }
    }

    /**
     * 方法六：通过entrySet获取然后使用迭代器遍历
     */
    private static void method6() {
        Map<String, Object> map = initHashMap();
        Set<Map.Entry<String, Object>> entries = map.entrySet();
        Iterator<Map.Entry<String, Object>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Object> next = iterator.next();
            System.out.println(next.getKey() + "--" + next.getValue());
        }
    }



    private static Map<String,Object> initHashMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("张三","你好");
        map.put("李四","你好");
        map.put("王五","你好");
        map.put("赵六","你好");
        map.put("田七","你好");
        return map;
    }
}
