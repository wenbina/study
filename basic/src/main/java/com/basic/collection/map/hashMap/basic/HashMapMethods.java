package com.basic.collection.map.hashMap.basic;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: w
 * @Date: 2021/8/2 22:25
 * hashMap的常用方法
 */
public class HashMapMethods {

    public static void main(String[] args) {
        putMethod();
        removeMethod();
        getMethod();
        sizeMethod();
        isEmptyMethod();
        clearMethod();
        containsKeyMethod();
    }

    /**
     * put：添加元素
     */
    private static void putMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("put方法");
        map.put("李四","你好");
        System.out.println(map);
    }

    /**
     * remove：根据键删除映射关系
     */
    private static void removeMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("remove方法");
        map.remove("张三");
        System.out.println(map);
    }

    /**
     * get：根据键获取值
     */
    private static void getMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("get方法");
        System.out.println(map.get("张三"));
    }

    /**
     * size：获取元素个数
     */
    private static void sizeMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("size方法");
        System.out.println(map.size());
    }

    /**
     * isEmpty：判断个数是否为0
     */
    private static void isEmptyMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("isEmpty方法");
        System.out.println(map.isEmpty());
    }

    /**
     * clear：清除
     */
    private static void clearMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("clear方法");
        map.clear();
        System.out.println(map);

    }

    /**
     * containsKey：查找键是否存在
     */
    private static void containsKeyMethod() {
        Map<String, Object> map = initHashMap();
        printlnTemplate("containsKey方法");
        System.out.println(map.containsKey("张三"));
    }

    private static Map<String,Object> initHashMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("张三","你好");
        return map;
    }

    private static void printlnTemplate(String methodName) {
        System.out.println("=================="+methodName+"=====================");
    }
}
