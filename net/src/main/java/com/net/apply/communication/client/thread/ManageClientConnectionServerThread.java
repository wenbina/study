package com.net.apply.communication.client.thread;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: w
 * @Date: 2021/8/8 11:32
 * 管理客户端连接服务端线程
 */
public class ManageClientConnectionServerThread {

    // 将多个线程放入hashMap中
    private static Map<String,ClientConnectionServerThread> map = new HashMap<>();

    // 将某个线程加入集合中
    public static void addThread(String username,ClientConnectionServerThread clientConnectionServerThread) {
        map.put("Thread-" + username,clientConnectionServerThread);
    }

    // 从集合中取出某个线程
    public static ClientConnectionServerThread getThreadByUsername(String username) {
        return map.get("Thread-" + username);
    }

    // 从集合中移除某个线程
    public static void removeThread(String username) {
        map.remove("Thread-" + username);
    }

}
