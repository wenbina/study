package com.net.apply.communication.client.thread;

import com.net.apply.communication.entity.Message;
import lombok.Data;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/8 11:24
 */
@Data
public class ClientConnectionServerThread extends Thread {

    // 该线程需要持有socket
    private Socket socket;

    public ClientConnectionServerThread(Socket socket) {
        this.socket = socket;
    }

    /**
     * 该线程需要一直保持与服务器通信
     */
    @Override
    public void run() {
        while (true) {
            try {
                // 读取服务端回送的消息
                ObjectInputStream oip = new ObjectInputStream(socket.getInputStream());
                Message message = (Message)oip.readObject(); // 如果服务端没有数据回送，会阻塞在这
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}
