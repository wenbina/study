package com.net.apply.communication.server.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.net.apply.communication.server.thread.ManageServerConnectionClientThread;
import com.net.apply.communication.server.thread.ServerConnectionClientThread;
import com.net.apply.communication.entity.Message;
import com.net.apply.communication.entity.User;
import com.net.apply.communication.enums.MessageTypeEnum;
import com.net.apply.communication.server.service.ServerLoginService;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: w
 * @Date: 2021/8/8 10:39
 */
public class ServerLoginServiceImpl implements ServerLoginService {

    private ServerSocket serverSocket;

    /**
     * 用户登陆
     */
    @Override
    public void login() {
        Message message = new Message();
        // 监听9999端口，获取客户端发送过来的消息
        try {
            serverSocket = new ServerSocket(9999);  // 端口可以在配置文件中写入
            System.out.println("【服务端】：正在等待客户端连接...");
            // 需要一直监听这个端口
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("【服务端】：连接成功...");
                // 读取客户端发送的消息
                ObjectInputStream oip = new ObjectInputStream(socket.getInputStream());
                User user = (User)oip.readObject();
                List<User> users = initUserDataSource();
                // 校验账号密码
                boolean isTrue = checkUsernameAndPassword(user.getUsername(), user.getPassword(), users);
                if (isTrue) {
                    message.setMessageType(MessageTypeEnum.LOGIN_SUCCESS.type);
                }else {
                    message.setMessageType(MessageTypeEnum.LOGIN_FAIL.type);
                    oip.close();
                }
                // 将消息写回给客户端
                returnMsg(message,socket,user.getUsername());
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 校验方法
     */
    private boolean checkUsernameAndPassword(String username,String password,List<User> users) {
        boolean result = false;
        if (CollectionUtil.isNotEmpty(users)) {
            for (User databaseUser : users) {
                if (databaseUser.getUsername().equals(username) && databaseUser.getPassword().equals(password)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    /**
     * 通过socket获取输出流，将数据返回给服务端
     */
    private void returnMsg(Message message,Socket socket,String username) {
        try {
            // 通过socket获取输出流
            ObjectOutputStream oop = new ObjectOutputStream(socket.getOutputStream());
            // 把数据写入输出流中
            oop.writeObject(message);
            // 创建一个线程保持与客户端通信
            ServerConnectionClientThread connectionClientThread = new ServerConnectionClientThread(socket, username);
            connectionClientThread.start();
            // 将该线程对象放入集合中
            ManageServerConnectionClientThread.addThread(username,connectionClientThread);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    // 数据源：此后可以连接数据库提供
    private List<User> initUserDataSource() {

        List<User> users = new ArrayList<>();

        User user = new User();
        user.setUsername("zs");
        user.setPassword("123");
        users.add(user);

        User user1 = new User();
        user1.setUsername("ls");
        user1.setPassword("123");
        users.add(user1);

        User user2 = new User();
        user2.setUsername("ww");
        user2.setPassword("123");
        users.add(user2);
        return users;
    }
}

class TestServer {
    public static void main(String[] args) {
        ServerLoginServiceImpl serverLoginService = new ServerLoginServiceImpl();
        serverLoginService.login();
    }
}

