package com.net.apply.communication.enums;

/**
 * @Author: w
 * @Date: 2021/8/8 11:09
 * 消息类型枚举类
 */
public enum MessageTypeEnum {

    LOGIN_SUCCESS("success","登陆成功"),
    LOGIN_FAIL("fail","登陆失败");

    public String type;

    public String desc;

    MessageTypeEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
