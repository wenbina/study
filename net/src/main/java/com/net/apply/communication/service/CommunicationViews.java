package com.net.apply.communication.service;

import com.net.apply.communication.client.serveice.ClientLoginService;
import com.net.apply.communication.entity.User;
import lombok.Data;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/8/8 10:05
 * 界面显示
 */
@Data
public class CommunicationViews {

    // 控制是否显示登陆页面
    private boolean loop = true;

    // 客户端登陆服务
    private ClientLoginService clientLoginService;

    // 用户
    private User user = new User();

    // 端口
    private int port;

    // 页面1：登陆页面
    public void view1() {
        while (loop) {
            System.out.println("=================欢迎登陆网络通讯系统================");
            System.out.println("\t\t\t\t\t 1 登陆系统");
            System.out.println("\t\t\t\t\t 9 退出系统");
            System.out.println("请选择您要进行的操作");

            Scanner scanner = new Scanner(System.in);
            String selection = scanner.nextLine();
            switch (selection) {
                case "1":
                    System.out.println("请输入账号");
                    String username = scanner.nextLine();
                    System.out.println("请输入密码");
                    String password = scanner.nextLine();
                    System.out.println("账号：" + username + "；密码" + password);
                    // 将账号密码发送到服务端去校验合法性；校验合法进入页面2
                    user.setUsername(username);
                    user.setPassword(password);
                    if (clientLoginService.login(user,port)) {
                        view2();
                    }else {
                        System.out.println("账号或密码有误");
                    }
                    break;
                case "9":
                    System.out.println("正在退出系统...");
                    try {
                        TimeUnit.SECONDS.sleep(1);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    loop = false;
                    break;
            }
        }
    }

    /**
     * 拉取在线用户列表
     * 功能
     * 1：显示在线用户列表
     * 2：群发信息
     * 3：私聊信息
     * 4：发送文件
     * 5：退出系统
     */
    public void view2() {
        while (loop) {
            System.out.println("=================欢迎登陆网络通讯系统================");
            System.out.println("\t\t\t\t\t 1 显示在线用户列表");
            System.out.println("\t\t\t\t\t 2 群发信息");
            System.out.println("\t\t\t\t\t 3 私聊信息");
            System.out.println("\t\t\t\t\t 4 发送文件");
            System.out.println("\t\t\t\t\t 9 退出系统");
            System.out.println("请选择您要进行的操作");
            Scanner scanner = new Scanner(System.in);
            String selection = scanner.nextLine();
            switch (selection) {
                case "1":
                    System.out.println("=========当前在线用户列表=========");
                    System.out.println("李四");
                    System.out.println("王五");
                    System.out.println("赵六");
                    break;
                case "2":
                    System.out.println("群发消息");
                    break;
                case "3":
                    System.out.println("私聊信息");
                    break;
                case "4":
                    System.out.println("发送文件");
                    break;
                case "9":
                    loop = false;
                    break;
            }
        }
    }
}

class A {
    public static void main(String[] args) {
        CommunicationViews communicationViews = new CommunicationViews();
        communicationViews.setPort(9999);
        communicationViews.view1();
    }
}

