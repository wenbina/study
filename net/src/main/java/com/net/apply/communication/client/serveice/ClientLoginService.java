package com.net.apply.communication.client.serveice;

import com.net.apply.communication.entity.User;

/**
 * @Author: w
 * @Date: 2021/8/8 10:57
 */
public interface ClientLoginService {

    boolean login(User user,Integer port);
}
