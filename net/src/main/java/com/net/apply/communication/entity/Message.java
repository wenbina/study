package com.net.apply.communication.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/8 11:07
 */
@Data
public class Message implements Serializable {

    private String sender;

    private String receiver;

    private String content;

    private String sendTime;

    private String messageType;  // 消息类型

}
