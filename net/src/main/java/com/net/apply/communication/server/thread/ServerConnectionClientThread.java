package com.net.apply.communication.server.thread;

import com.net.apply.communication.entity.Message;

import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/8 12:15
 */
public class ServerConnectionClientThread extends Thread {

    // 该线程需要持有socket
    private Socket socket;

    private String username;

    public ServerConnectionClientThread(Socket socket,String username) {
        this.socket = socket;
        this.username = username;
    }

    /**
     * 该线程需要一直保持与客户端通信
     */
    @Override
    public void run() {
        while (true) {
            try {
                // 读取客户端端回送的消息
                ObjectInputStream oip = new ObjectInputStream(socket.getInputStream());
                Message message = (Message)oip.readObject(); // 如果服务端没有数据回送，会阻塞在这
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }
}
