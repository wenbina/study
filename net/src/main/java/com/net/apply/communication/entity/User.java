package com.net.apply.communication.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/8 10:36
 */
@Data
public class User implements Serializable {

    private String username;

    private String password;

}
