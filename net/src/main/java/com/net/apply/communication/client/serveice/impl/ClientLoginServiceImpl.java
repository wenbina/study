package com.net.apply.communication.client.serveice.impl;

import com.net.apply.communication.client.serveice.ClientLoginService;
import com.net.apply.communication.client.thread.ClientConnectionServerThread;
import com.net.apply.communication.client.thread.ManageClientConnectionServerThread;
import com.net.apply.communication.entity.Message;
import com.net.apply.communication.entity.User;
import com.net.apply.communication.enums.MessageTypeEnum;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/8 10:57
 */
public class ClientLoginServiceImpl implements ClientLoginService {

    private Socket socket;

    /**
     * 需要将用户对象通过socket传到服务端
     */
    @Override
    public boolean login(User user,Integer port) {
        boolean result = false;
        // 连接服务端，发送user对象
        try {
            socket = new Socket(InetAddress.getLocalHost(),port);
            // 获取objectOutputStream对象
            ObjectOutputStream oop = new ObjectOutputStream(socket.getOutputStream());
            // 将user对象写入输出流
            oop.writeObject(user);
            // 读取从服务端返回Message对象
            ObjectInputStream oip = new ObjectInputStream(socket.getInputStream());
            Message message = (Message)oip.readObject();
            if (MessageTypeEnum.LOGIN_SUCCESS.type.equals(message.getMessageType())) {
                // 创建一个和服务器端保持通信的线程
                ClientConnectionServerThread connectionServerThread = new ClientConnectionServerThread(socket);
                // 启动客户端的线程
                connectionServerThread.start();
                // 为了后面客户端的扩展，将线程放入集合中管理
                ManageClientConnectionServerThread.addThread(user.getUsername(),connectionServerThread);
                result = true;
            } else {
                System.out.println("登陆失败");
                // 关闭socket
                socket.close();
                result = false;
            }

        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return result;
    }
}

class TestClient {
    public static void main(String[] args) {
        ClientLoginServiceImpl clientLoginService = new ClientLoginServiceImpl();
        User user = new User();
        user.setUsername("zs");
        user.setPassword("123");
        int port = 9999;
        boolean login = clientLoginService.login(user, port);
        System.out.println("是否登陆成功：" + login);
    }
}
