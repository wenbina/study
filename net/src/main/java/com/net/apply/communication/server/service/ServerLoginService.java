package com.net.apply.communication.server.service;

import com.net.apply.communication.entity.Message;
import com.net.apply.communication.entity.User;

/**
 * @Author: w
 * @Date: 2021/8/8 10:40
 */
public interface ServerLoginService {

    void login();
}
