1：基本介绍
（1）通信两端都要有socket，是两台机器间通信的端点
（2）网络通信其实就是socket之间的通信
（3）socket允许程序把网络连接当成一个流，数据在两个socket之间通过IO传输
（4）一般主动发起通信的应用程序属客户端，等待通信请求的为服务端

2：使用socket进行网络通信
（1）服务端
  （1）创建serverSocket对象：指定需要监听的ip地址与端口号
  （2）通过serverSocket创建socket对象
  （3）通过socket对象获取输入输出流
  （4）通过输入/输出流读取/写入数据
（2）客户端
  （1）创建socket对象：指定需要连接的ip和端口号
  （2）通过socket对象获取输入输出流
  （3）通过输入/输出流读取/写入数据
  
常见异常
java.net.SocketException: Connection reset
此异常是因为客户端或服务端某一端断开连接，而另一端又一直在读取断开端传过来的数据导致的


注意：客户端与服务端输入输出流必须要对应，不然会出现没有信息读取的异常


3：同步阻塞
用户线程与内核的交互方式，应用端请求对应一个线程处理，整个过程中accept(接收)和read(读取)方法都会阻塞直至整个动作完成：
该方式如果在高并发的场景下，客户端的请求响应会存在严重的性能问题，并且占用过多资源

4：分类
（1）按数据的流向：输入、输出
    输入流：设备---》内存
    输出流：内存---》设备
（2）按数据的类型：字节、字符
    字节流：以字节为单位
    字符流：以字符为单位
（3）分类之后的超类
    字节输入流            字节输出流
    InputStream         OutputStream
    字符输入流            字符输出流
    Reader              Writer

5：数据传输
我们必须明确一点的是，一切文件数据(文本、图片、视频等)在存储时，都是以二进制数字的形式保存，是一个一个的字节，那么传输时一样如此。所以，字节流可以传输任意文件数据。
在操作流的时候，我们要时刻明确，无论使用什么样的流对象，底层传输的始终为二进制数据。

6：字节输出流（OutputStream）
（1）基本方法
1、 close() ：关闭此输出流并释放与此流相关联的任何系统资源。
2、 flush() ：刷新此输出流并强制任何缓冲的输出字节被写出。
3、 write(byte[] b)：将 b.length个字节从指定的字节数组写入此输出流。
4、 write(byte[] b, int off, int len) ：从指定的字节数组写入 len字节，从偏移量 off开始输出到此输出流。 也就是说从off个字节数开始读取一直到len个字节结束
5、 write(int b) ：将指定的字节输出流。
（2）子类
1、FileOutputStream：文件输出流，用于将数据写出到文件。
  （1）构造方法
     （1）public FileOutputStream(File file)：根据File对象为参数创建对象。
     （2）public FileOutputStream(String name)： 根据名称字符串为参数创建对象。
   推荐第二种构造方法
   FileOutputStream outputStream = new FileOutputStream("abc.txt");
   就以上面这句代码来讲，类似这样创建字节输出流对象都做了三件事情：
   1、调用系统功能去创建文件【输出流对象才会自动创建】
   2、创建outputStream对象
   3、把outputStream对象指向这个文件
  （2）写出数据（通过write）
     （1）public void write(int b)
     （2）public void write(byte[] b)
     （3）public void write(byte[] b,int off,int len)  //从`off`索引开始，`len`个字节
   流操作完毕后，必须释放系统资源，调用close方法，千万记得。
   


