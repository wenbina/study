package com.net.socket.communication.threads;

import cn.hutool.core.collection.CollectionUtil;
import com.net.socket.communication.entity.Msg;
import com.net.socket.communication.enums.MsgEnum;

import java.io.*;
import java.net.Socket;
import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/9 8:38
 */
public class ConnectClientThread extends Thread {

    private Socket socket;

    public ConnectClientThread (Socket socket) {
        this.socket = socket;
    }

    /**
     * 保持与客户端通信
     */
    @Override
    public void run() {
        while (true) {
            try {
                // 通过socket获取输入流读取客户端发送的消息
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                // 客户端发送信息
                Msg clientMsg = (Msg)ois.readObject();
                // 服务端返回信息
                Msg returnMsg = this.handleRequest(clientMsg);
                // 创建输出流回送消息给客户端
                this.sendMsgToClient(socket,returnMsg);

            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }


    /**
     * 处理请求
     */
    private Msg handleRequest(Msg clientMsg) {
        Msg msg = new Msg();
        StringBuffer sb = new StringBuffer();
        if (MsgEnum.FRIEND_LIST.type.equals(clientMsg.getMsgType())) {
            System.out.println("【服务端】：正在拉取好友列表...");
            // 拉取好友列表
            Set<String> friendSet = ManageConnectClientThreads.threadMap.keySet();
            if (CollectionUtil.isNotEmpty(friendSet)) {
                sb.append("您的好友【 ");
                for (String friend : friendSet) {
                    sb.append(friend + " ");
                }
                sb.append("】，正在线上 ");
            }else {
                sb.append("您还没有好友在线上");
            }
        } else if(MsgEnum.CHAT_WITH_FRIEND.type.equals(clientMsg.getMsgType())) {
            // 与好友聊天
            sb.append("正在准备聊天室进行聊天...");
        } else if(MsgEnum.SEND_FILE.type.equals(clientMsg.getMsgType())) {
            // 下载文件
            downloadDocument(clientMsg.getContent());
        }
        msg.setContent(sb.toString());
        return msg;
    }

    /**
     * 创建输出流回送消息给客户端
     */
    private void sendMsgToClient(Socket socket, Msg msg) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            // 将数据写入数据通道中
            oos.writeObject(msg);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * 下载文件
     */
    private void downloadDocument(Object clientMsg) {
        System.out.println("【服务端】：" + clientMsg.toString());
        // 将客户端文件转成字节数组
        byte[] bytes = clientMsg.toString().getBytes();
        // 创建输出流将文件保存到指定目录
        String destPath = "E:\\ownspace\\study\\net\\src\\main\\java\\com\\net\\socket\\communication\\service\\communicate\\receive\\a.jpg";
        this.saveDocument(destPath,bytes);
    }

    /**
     * 保存文件到指定位置
     */
    private void saveDocument(String destPath, byte[] bytes) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destPath));
            bos.write(bytes);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
