package com.net.socket.communication.service.communicate.impl;

import com.net.socket.communication.entity.Msg;
import com.net.socket.communication.entity.User;
import com.net.socket.communication.enums.MsgEnum;
import com.net.socket.communication.service.communicate.Client;
import com.net.socket.communication.threads.ConnectServerThread;
import com.net.socket.communication.threads.ManageConnectServerThreads;
import com.net.socket.communication.utils.StreamUtil;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/9 8:38
 */
public class ClientImpl implements Client {

    private Socket socket;

    @Override
    public void clientLogin(User user, InetAddress address, Integer port) {
        // 创建socket连接
        try {
            socket = new Socket(address, port);
            // 通过socket创建输出流发送消息给服务端
            sendMsgToServer(socket,user);
            // 通过socket创建输入流读取服务端回送的信息
            receiveMsgFromServer(socket,user);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 拉取好友列表
     */
    @Override
    public void getFriendsList(String username) {
        // 通过用户名获取正在线程池管理的socket
        ConnectServerThread connectServerThread = ManageConnectServerThreads.getThreadByName(username);
        Socket socket = connectServerThread.getSocket();
        // 初始化消息
        Msg msg = new Msg("拉取好友列表", MsgEnum.FRIEND_LIST.type);
        // 向服务端发送请求
        this.sendRequestToServer(socket, msg);
    }

    /**
     * 与好友聊天
     */
    @Override
    public void chatWithFriend(String username, String friendName) {
        // 通过用户名获取正在线程池管理的socket
        ConnectServerThread connectServerThread = ManageConnectServerThreads.getThreadByName(username);
        Socket socket = connectServerThread.getSocket();
        // 初始化消息
        Msg msg = new Msg("与好友聊天", MsgEnum.CHAT_WITH_FRIEND.type);
        // 向服务端发送请求
        this.sendRequestToServer(socket, msg);
    }

    /**
     * 上传文件
     */
    @Override
    public void uploadFile(String username) {
        // 通过用户名获取socket连接
        ConnectServerThread connectServerThread = ManageConnectServerThreads.getThreadByName(username);
        // 获取socket
        Socket socket = connectServerThread.getSocket();
        // 发送上传文件请求
        this.sendFileToServer(socket);
    }


    /**
     * 通过socket创建输出流发送消息给服务端
     */
    private void sendMsgToServer(Socket socket, User user) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            // 将用户对象发送给服务端
            oos.writeObject(user);
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * 通过socket创建输入流接收服务端发送的消息
     */
    private void receiveMsgFromServer(Socket socket,User user) {
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            // 读取服务端回送的消息
            String serverMsg = (String)ois.readObject();
            System.out.println("【客户端】：" + serverMsg);
            // 登陆成功才保持连接
            if("登陆成功".equals(serverMsg)) {
                // 开启一个新线程去保持与服务端的连接
                ConnectServerThread connectServerThread = new ConnectServerThread(socket);
                connectServerThread.start();
                // 将该线程加入线程集合中
                ManageConnectServerThreads.addThread(user.getUsername(), connectServerThread);
            }else {
                // 断开连接
                socket.close();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    // 向服务端发送请求
    private void sendRequestToServer(Socket socket,Msg msg) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject(msg);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    // 向服务端发送文件
    private void sendFileToServer(Socket socket) {
        String filepath = "E:\\ownspace\\study\\net\\src\\main\\resources\\a.jpg";
        // 读取本地文件转成字节数组
        byte[] bytes = readLocalDocument(filepath);
        // 通过socket创建输出流将字节数组发送给服务端
        sendFile(socket,bytes);
    }

    // 读取本地文件转成字节数组
    private byte[] readLocalDocument(String filepath) {
        byte[] bytes = null;
        // 创建输入流读取本地文件
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filepath));
            // 将数据放入字节数组中
            bytes = StreamUtil.streamToArray(bis);
            System.out.println("【客户端】：" + bytes);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }

    // 通过socket创建输出流发送文件
    private void sendFile(Socket socket, byte[] bytes) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            // 初始化消息
            Msg msg = new Msg(bytes, MsgEnum.SEND_FILE.type);
            oos.writeObject(msg);
            /*BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            // 将字节数组写入输出流中
            bos.write(bytes);
            // 设置结束标记
            socket.shutdownOutput();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
