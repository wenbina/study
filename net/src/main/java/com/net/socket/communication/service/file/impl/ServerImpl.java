package com.net.socket.communication.service.file.impl;

import com.net.socket.communication.service.file.Server;
import com.net.socket.communication.utils.StreamUtil;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/10 14:13
 */
public class ServerImpl implements Server {

    /**
     * 下载文件
     * 1：与客户端连接创建socket
     * 2：通过socket创建字节输入流读取客户端发送的文件
     * 3：创建字节输出流将文件保存在指定位置
     * 4：通过socket创建输出流向客户端回送消息
     * 5：关闭流
     */
    @Override
    public void downloadFile(int port, String destPath) {
        try {
            // 监听客户端连接的端口
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("【服务端】：正在等待客户端连接...");
            // 与客户端连接成功后生成socket；若连接不成功会一直阻塞在这里
            Socket socket = serverSocket.accept();
            System.out.println("【服务端】：客户端连接成功");
            // 通过socket创建字节输入流读取客户端发送的文件
            byte[] bytes = this.readReceiveDocument(socket);
            String msg = "";
            if (bytes != null) {
                // 创建字节输出流将文件保存在指定位置
                this.saveDocument(destPath, bytes);
                msg = "文件接收成功";
            }else {
                msg = "文件接收失败";
            }
            System.out.println("【服务端】：" + msg);
            // 回送消息给客户端
            sendMsgToClient(socket,msg);
            // 关闭流
            socket.close();
            serverSocket.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 通过socket创建输入流读取客户端发送的文件
     */
    private byte[] readReceiveDocument(Socket socket) {
        byte[] bytes = null;
        try {
            BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());
            // 将输入流转成字节数组
            bytes = StreamUtil.streamToArray(bis);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return bytes;
    }

    /**
     * 保存文件到指定位置
     */
    private void saveDocument(String destPath, byte[] bytes) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(destPath));
            bos.write(bytes);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * 回送消息给客户端
     */
    private void sendMsgToClient(Socket socket, String msg) {
        try {
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            writer.write(msg);
            // 将数据刷新到数据通道：若客户端没有接收到回送消息的话应该就是数据没有刷新到数据通道
            writer.flush();
            // 设置结束标记
            socket.shutdownOutput();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
