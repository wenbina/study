package com.net.socket.communication.service.file.impl;

import com.net.socket.communication.service.file.Client;
import com.net.socket.communication.utils.StreamUtil;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/10 14:13
 */
public class ClientImpl implements Client {


    /**
     * 上传文件：
     * 1：通过输入流读取需要上传的文件
     * 2：通过socket创建输出流
     * 3：将文件通过输出流放到数据通道中
     * 4：通过socket创建输入流
     * 5：接收服务端返回的消息
     * 6：关闭流
     */
    @Override
    public void uploadFile(InetAddress address, int port, String filepath) {
        try {
            // 连接服务端创建socket
            Socket socket = new Socket(address, port);
            // 创建输入流读取上传文件
            byte[] bytes = readDocument(filepath);
            System.out.println("读取到的字节数组为：" + bytes);
            // 将字节文件通过socket创建的输出流发送给服务端
            if (bytes != null) {
                // 将字节文件通过socket创建的输出流发送给服务端
                sendFileToServer(socket,bytes);
            }
            // 读取服务端回送的消息
            String serverMsg = receiveServerMsg(socket);
            System.out.println("【客户端】：" + serverMsg);
            // 关闭socket
            socket.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }

    }

    /**
     * 创建输入流读取上传文件
     */
    private byte[] readDocument(String filepath) {
        byte[] bytes = null;
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(filepath));
            // 将上传文件转成字节
            bytes = StreamUtil.streamToArray(bis);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bytes;
    }

    /**
     * 将字节文件通过socket创建的输出流发送给服务端
     */
    private void sendFileToServer(Socket socket, byte[] bytes) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(socket.getOutputStream());
            // 将文件通过输出流写到数据通道
            bos.write(bytes);
            // 设置结束标记
            socket.shutdownOutput();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 读取服务端回送的消息
     */
    private String receiveServerMsg(Socket socket) {
        String receiveMsg = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            receiveMsg = reader.readLine();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return receiveMsg;
    }
}
