package com.net.socket.communication.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/12 17:51
 */
@Data
public class Msg implements Serializable {

    // 消息内容
    private Object content;

    // 消息类型
    private String msgType;

    public Msg() {
    }

    public Msg(Object content, String msgType) {
        this.content = content;
        this.msgType = msgType;
    }
}
