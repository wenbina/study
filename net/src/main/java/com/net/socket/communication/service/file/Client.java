package com.net.socket.communication.service.file;

import java.net.InetAddress;

/**
 * @Author: w
 * @Date: 2021/8/10 14:13
 */
public interface Client {

    void uploadFile(InetAddress address, int port, String filepath);

}
