package com.net.socket.communication.threads;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: w
 * @Date: 2021/8/10 9:02
 * 管理连接服务端线程集合
 */
public class ManageConnectServerThreads {

    public static Map<String,ConnectServerThread> threadMap = new HashMap<>();

    // 添加线程方法
    public static void addThread(String threadName,ConnectServerThread connectServerThread) {
        threadMap.put(threadName,connectServerThread);
    }

    // 移除线程方法
    public static void removeThread(String threadName) {
        threadMap.remove(threadName);
    }

    // 获取线程方法
    public static ConnectServerThread getThreadByName(String threadName) {
        return threadMap.get(threadName);
    }
}
