package com.net.socket.communication.service.communicate.impl;

import com.net.socket.communication.entity.User;
import com.net.socket.communication.service.communicate.Server;
import com.net.socket.communication.threads.ConnectClientThread;
import com.net.socket.communication.threads.ManageConnectClientThreads;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/9 8:38
 */
public class ServerImpl implements Server {

    private ServerSocket serverSocket;

    @Override
    public void ServerLogin() {
        try {
            // 监听某个端口：可以写在配置文件中
            serverSocket = new ServerSocket(9999);
            while (true) {
                //System.out.println("【服务端】：正在等待客户端连接...");
                // 获取客户端连接之后的socket：客户端没连接的话就会一直阻塞在这
                Socket socket = serverSocket.accept();
                //System.out.println("【服务端】：客户端连接成功");

                // 通过socket获取输入流读取客户端发送的消息
                Object clientMsg = this.readClientMsg(socket);
                User user = (User) clientMsg;

                // 初始化用户
                Map<String, String> userMap = this.initUserMap();
                // 校验用户合法性
                boolean isExist = this.checkUsernameAndPassword(user.getUsername(), user.getPassword(), userMap);

                String returnMsg = isExist ? "登陆成功" : "账号或密码有误";
                System.out.println("【服务端】：" + user.getUsername() + returnMsg);

                // 通过socket获取输出流发送消息给客户端
                this.sendMsgToClient(socket,returnMsg,user);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public Set<String> queryConnectClients() {
        return ManageConnectClientThreads.threadMap.keySet();
    }

    /**
     * 通过socket获取输入流读取客户端发送的消息
     */
    private Object readClientMsg(Socket socket) {
        Object clientMsg = new Object();
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            clientMsg = ois.readObject();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return clientMsg;
    }

    /**
     * 通过socket创建输出流想客户端发送消息
     */
    private void sendMsgToClient(Socket socket, String serverMsg,User user) {
        try {
            // 通过socket创建对象输出流
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            // 将数据写入数据通道
            oos.writeObject(serverMsg);
            // 开启一个新线程保持与客户端连接
            ConnectClientThread connectClientThread = new ConnectClientThread(socket);
            connectClientThread.start();
            // 将该线程加入自定义线程集合中，方便管理
            ManageConnectClientThreads.addThread(user.getUsername(),connectClientThread);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 校验账号密码
     */
    private boolean checkUsernameAndPassword(String username, String password, Map<String, String> userMap) {
        // 判断该账号是否存在
        boolean hasUser = userMap.containsKey(username);
        if (hasUser) {
            if (userMap.get(username).equals(password)) {
                hasUser = true;
            } else {
                hasUser = false;
            }
        }
        return hasUser;
    }

    /**
     * 初始化用户
     */
    private Map<String, String> initUserMap() {
        Map<String, String> userMap = new HashMap<>();
        userMap.put("zs", "123");
        userMap.put("ls", "123");
        userMap.put("ww", "123");
        return userMap;
    }
}
