package com.net.socket.communication.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/9 8:36
 */
@Data
public class User implements Serializable {

    private String username;

    private String password;
}
