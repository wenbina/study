package com.net.socket.communication.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: w
 * @Date: 2021/8/10 14:41
 * 流转字节与字节转流工具
 */
public class StreamUtil {

    /**
     * 流转字节
     */
    public static byte[] streamToArray(InputStream inputStream) throws IOException {
        // 创建字节输出流
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        byte[] bytes = new byte[1024];
        int len;
        while ((len = inputStream.read(bytes)) != -1) {  // 循环读取
            bos.write(bytes,0,len);  // 将读取到的数据写入bos
        }
        byte[] array = bos.toByteArray();  // 将bos转成字节数组
        bos.close();
        return array;
    }
}
