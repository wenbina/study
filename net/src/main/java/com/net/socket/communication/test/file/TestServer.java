package com.net.socket.communication.test.file;

import com.net.socket.communication.service.file.Server;
import com.net.socket.communication.service.file.impl.ServerImpl;

/**
 * @Author: w
 * @Date: 2021/8/10 15:41
 */
public class TestServer {

    public static void main(String[] args) {
        Server server = new ServerImpl();
        server.downloadFile(9999,"E:\\ownspace\\study\\net\\src\\main\\java\\com\\net\\socket\\communication\\service\\file\\receive\\a.jpg");
    }
}
