package com.net.socket.communication.service.communicate;

import com.net.socket.communication.entity.User;

import java.net.InetAddress;

/**
 * @Author: w
 * @Date: 2021/8/9 8:37
 */
public interface Client {

    // 登陆
    void clientLogin(User user, InetAddress address, Integer port);

    // 拉取好友列表
    void getFriendsList(String username);

    // 与某个好友聊天
    void chatWithFriend(String username,String friendName);

    // 上传文件
    void uploadFile(String username);

}
