package com.net.socket.communication.test.communicate;

import com.net.socket.communication.entity.User;
import com.net.socket.communication.service.communicate.Client;
import com.net.socket.communication.service.communicate.impl.ClientImpl;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author: w
 * @Date: 2021/8/9 9:05
 */
public class ClientTest1 {

    public static void main(String[] args) {
        Client client = new ClientImpl();
        User user = new User();
        user.setUsername("zs");
        user.setPassword("123");
        InetAddress localHost = null;
        try {
            localHost = InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        int port = 9999;
        client.clientLogin(user,localHost,port);
        // 拉取好友列表
        //client.getFriendsList(user.getUsername());
        // 好友聊天
        //client.chatWithFriend(user.getUsername(),"ls");
        // 发送文件
        client.uploadFile(user.getUsername());
    }
}
