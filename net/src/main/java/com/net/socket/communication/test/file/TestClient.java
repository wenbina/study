package com.net.socket.communication.test.file;

import com.net.socket.communication.service.file.Client;
import com.net.socket.communication.service.file.impl.ClientImpl;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author: w
 * @Date: 2021/8/10 15:40
 */
public class TestClient {

    public static void main(String[] args) throws UnknownHostException {
        Client client = new ClientImpl();
        client.uploadFile(InetAddress.getLocalHost(),9999,"E:\\ownspace\\study\\net\\src\\main\\resources\\a.jpg");
    }
}
