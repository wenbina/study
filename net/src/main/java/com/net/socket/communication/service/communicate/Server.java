package com.net.socket.communication.service.communicate;

import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/9 8:37
 */
public interface Server {

    void ServerLogin();

    // 查询全部在线客户端
    Set<String> queryConnectClients();
}
