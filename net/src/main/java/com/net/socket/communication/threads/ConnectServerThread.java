package com.net.socket.communication.threads;

import com.net.socket.communication.entity.Msg;
import com.net.socket.communication.enums.MsgEnum;
import com.net.socket.communication.utils.StreamUtil;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/9 8:39
 */
public class ConnectServerThread extends Thread{

    private Socket socket;

    public ConnectServerThread(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    /**
     * 保持与客户端的通信：创建输入流读取客户端回送的消息
     */
    @Override
    public void run() {
        while (true) {
            try {
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream()); // 在服务端没有消息回送时会阻塞在这个地方
                Msg serverMsg = (Msg)ois.readObject();
                // 根据服务端回传的消息做相应的操作
                System.out.println("【客户端】：" + serverMsg.getContent());
                //this.handClientRequest(serverMsg.getContent());
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }



}
