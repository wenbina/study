package com.net.socket.communication.service.file;

/**
 * @Author: w
 * @Date: 2021/8/10 14:12
 */
public interface Server {

    void downloadFile(int port, String destPath);

}
