package com.net.socket.communication.threads;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author: w
 * @Date: 2021/8/10 8:50
 * 管理连接客户端的线程集合
 * 注意：静态变量获取是要在同一个线程中的，如果你开启一个新的主线程去查询的话，静态变量是会被初始化的
 */
public class ManageConnectClientThreads {

    public static Map<String,ConnectClientThread> threadMap = new HashMap<>();

    // 添加线程到线程集合
    public static void addThread(String threadName, ConnectClientThread connectClientThread) {
        threadMap.put(threadName,connectClientThread);
    }

    // 从线程集合中移除线程
    public static void removeThread(String threadName) {
        threadMap.remove(threadName);
    }

    // 获取某个线程
    public static ConnectClientThread getThreadByName(String threadName) {
        return threadMap.get(threadName);
    }


}
