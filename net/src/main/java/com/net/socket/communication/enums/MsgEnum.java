package com.net.socket.communication.enums;

/**
 * @Author: w
 * @Date: 2021/8/10 17:49
 */
public enum MsgEnum {

    FRIEND_LIST("friend_list","拉取好友列表"),
    CHAT_WITH_FRIEND("chat_with_friend","与好友聊天"),
    SEND_FILE("send_file","发送文件");

    public String type;

    public String desc;

    MsgEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
