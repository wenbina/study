package com.net.socket.practice.udpPractice1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * @Author: w
 * @Date: 2021/8/6 22:41
 */
public class UDPReceive {

    public static void main(String[] args) throws IOException {
        // 创建一个datagramSocket对象，准备在8888端口接收数据
        DatagramSocket socket = new DatagramSocket(8888);
        // 构建一个DatagramPacket对象，准备接收数据
        byte[] bytes = new byte[1024];
        DatagramPacket datagramPacket = new DatagramPacket(bytes, bytes.length);
        // 调用接收方法，将通过网络传输的DatagramPacket对象填充到DatagramPacket对象：当有数据发送到该端口时，就会接收到对象，否则进行阻塞
        System.out.println("【接收端】：正在等待接收数据...");
        socket.receive(datagramPacket);
        // 将datagramPacket进行拆包，取出数据
        int length = datagramPacket.getLength();
        // 获取接收到的数据
        byte[] data = datagramPacket.getData();
        String string = new String(data, 0, length);
        String returnMsg;
        returnMsg = "四大名著".equals(string) ? "《三国演义》、《红楼梦》、《西游记》、《水浒传》" : "what?";
        bytes = returnMsg.getBytes();
        datagramPacket = new DatagramPacket(bytes,bytes.length, InetAddress.getLocalHost(),8889);
        socket.send(datagramPacket);

        // 关闭资源
        socket.close();
    }
}
