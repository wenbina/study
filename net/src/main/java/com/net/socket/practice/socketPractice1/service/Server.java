package com.net.socket.practice.socketPractice1.service;

import com.net.socket.practice.socketPractice1.entity.Msg;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/13 11:55
 */
public class Server {

    public static void main(String[] args) {
        try {
            // 获取图片的字节数组
            InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream("E:\\ownspace\\study\\net\\src\\main\\resources\\a.jpg"));
        } catch (Exception e) {

        }
    }

    public void downloadFile() {

    }

    // 通过socket创建对象输入流，读取客户端发送的消息
    private Object receiveClientMsg(Socket socket) {
        Object clientMsg = new Object();
        try {
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            Msg msg = (Msg)ois.readObject();
            // 获取上传文件
            byte[] bytes = (byte[])msg.getContent();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientMsg;
    }

    // 将字节文件写入到指定目录
    private void writeToDest(String destPath) {

    }
}
