package com.net.socket.practice.socketPractice1.service;

import com.net.socket.practice.socketPractice1.entity.Msg;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/13 11:55
 */
public class Client {

    // 读取本地需要上传的文件
    private void uploadFile() {
        try {
            Socket socket = new Socket(InetAddress.getLocalHost(), 9999);
            // 创建需要读取的文件
            File file = new File("E:\\ownspace\\study\\net\\src\\main\\resources\\a.jpg");
            // 创建输入流
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            // 将文件转成字节数组
            byte[] bytes = this.streamToArray(bis);
            // 通过socket创建输出流将文件内容传送给服务端
            sendFileToServer(socket,bytes);
            bis.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 创建字节输出流将读取的文件内容放入
    private byte[] streamToArray(InputStream inputStream) {
        byte [] array = null;
        try {
            // 创建字节输出流
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            // 创建字节数组用于接收文件内容
            byte[] bytes = new byte[1024];
            // 读取文件内容并将内容写入字节输出流中
            int len = 0;
            while ((len = inputStream.read()) != -1) {
                // 将数据写入到字节输出流中
                bos.write(bytes,0,len);
            }
            // 将字节输出流中的内容转成字节
            array = bos.toByteArray();
            // 关闭资源
            bos.close();
            return array;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return array;
    }

    // 通过socket创建输出流将文件传送给服务端
    private void sendFileToServer(Socket socket, byte[] bytes) {
        try {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            Msg msg = new Msg();
            msg.setMsgType(1);
            msg.setContent(bytes);
            oos.writeObject(msg);
            // 关闭资源
            oos.close();
        } catch (Exception e) {
           e.printStackTrace();
        }
    }
}
