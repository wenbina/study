package com.net.socket.practice.socketPractice1.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/13 11:56
 */
@Data
public class Msg implements Serializable {

    private Integer msgType;

    private Object content;
}
