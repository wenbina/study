package com.net.socket.practice.tcpPractice1;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/6 22:25
 */
public class SocketClient {

    public static void main(String[] args) throws IOException {
     connection("哈哈哈");
    }

    private static void connection(String msg) throws IOException {
        // 连接服务端
        Socket socket = new Socket(InetAddress.getLocalHost(), 8888);
        // 通过socket创建输出流
        OutputStream outputStream = socket.getOutputStream();
        // 将字节输出流转成字符输出流
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
        writer.write(msg);
        // 设置结束标记
        writer.newLine();
        // 将数据刷新到数据通道
        writer.flush();

        // 通过socket获取输入流
        InputStream inputStream = socket.getInputStream();
        // 将字节输入流转成字符输入流
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        // 读取服务端返回的数据
        String s = reader.readLine();
        System.out.println("【服务端】：" + s);

        // 关闭相应流
        reader.close();
        writer.close();
        socket.close();
    }
}
