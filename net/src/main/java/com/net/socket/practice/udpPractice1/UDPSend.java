package com.net.socket.practice.udpPractice1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * @Author: w
 * @Date: 2021/8/6 22:56
 */
public class UDPSend {

    public static void main(String[] args) throws IOException {
        // 创建一个datagramSocket对象，准备在8888端口接收数据
        DatagramSocket socket = new DatagramSocket(8889);

        // 将需要发送的数据封装到DatagramPacket对象
        byte[] data = "四大名著".getBytes();
        DatagramPacket datagramPacket = new DatagramPacket(data,data.length, InetAddress.getLocalHost(),8888);
        // 发送数据
        socket.send(datagramPacket);

        // 接收数据
        byte[] bytes = new byte[1024];
        datagramPacket = new DatagramPacket(bytes,bytes.length);

        // 接收数据
        socket.receive(datagramPacket);
        // 进行拆包
        int length = datagramPacket.getLength();
        data = datagramPacket.getData();
        String string = new String(data, 0, length);
        System.out.println("【接收端】：" + string);

        // 关闭资源
        socket.close();
    }
}
