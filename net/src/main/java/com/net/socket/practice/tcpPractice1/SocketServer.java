package com.net.socket.practice.tcpPractice1;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/6 22:13
 */
public class SocketServer {

    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        System.out.println("【服务端】：正在等待客户端连接...");
        // 客户端连接成功
        Socket socket = serverSocket.accept();
        System.out.println("【服务端】：客户端连接成功");
        // 通过输入流读取客户端发送的消息
        InputStream inputStream = socket.getInputStream();
        // 将字节流转成字符流
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        // 读取客户端发送内容
        String s = reader.readLine();
        // 回复给客户端的消息
        String returnMsg;
        if ("name".equals(s)) {
            returnMsg = "张三";
        }else if ("hobby".equals(s)) {
            returnMsg = "编写java程序";
        }else {
            returnMsg = "你说啥呢";
        }
        System.out.println("【客户端】：" + s);
        // 通过socket创建输出流
        OutputStream outputStream = socket.getOutputStream();
        // 将输出流转成字符输出流
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream));
        writer.write(returnMsg);
        // 设置结束标记
        writer.newLine();
        // 将数据刷新到数据通道中
        writer.flush();

        // 关闭相关流
        writer.close();
        reader.close();
        socket.close();
        serverSocket.close();
    }
}
