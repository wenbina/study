package com.net.basic.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * @Author: w
 * @Date: 2021/8/13 11:17
 */
public class WriterUse {

    public void write() {
        try {
            // 创建写入目标文件
            File file = new File("E:\\ownspace\\study\\net\\src\\main\\java\\com\\net\\basic\\writer\\files\\writer.txt");
            // 建立数据通道
            FileWriter fileWriter = new FileWriter(file);
            // 将数据写入数据通道中
            //writeByFileWriter(fileWriter);
            writeByBufferWriter(fileWriter);
            // 关闭资源
            fileWriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 直接通过文件输出流写出
    private void writeByFileWriter(FileWriter fileWriter) {
        try {
            // 将数据写入数据通道中
            fileWriter.write("hello，writer");
            // 将数据刷新到数据通道
            fileWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 通过字符输出流写出
    private void writeByBufferWriter(FileWriter fileWriter) {
        try {
            // 将数据写入数据通道中
            BufferedWriter writer = new BufferedWriter(fileWriter);
            writer.write("hello，writer");
            // 将数据刷新到数据通道
            writer.flush();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}
