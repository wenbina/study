package com.net.basic;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * @Author: w
 * @Date: 2021/8/4 22:34
 * INetAddress类的使用
 */
public class INetClassAddress {

    public static void main(String[] args) {
        getLocalInetAddress();
        getInetAddressByHostName();
        getInetAddressByDomain();
        getAddress();
    }

    /**
     * 获取本机InetAddress对象
     */
    private static void getLocalInetAddress() {
        try {
            InetAddress localHost = InetAddress.getLocalHost();
            System.out.println(localHost);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据主机名获取InetAddress对象
     */
    private static void getInetAddressByHostName() {
        try {
            InetAddress localHostByName = InetAddress.getByName("LAPTOP-OOPUO0DL");
            System.out.println(localHostByName);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 根据域名返回InetAddress对象
     */
    private static void getInetAddressByDomain() {
        try {
            InetAddress domainInetAddress = InetAddress.getByName("www.baidu.com");
            System.out.println(domainInetAddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * 通过InetAddress对象获取对应地址
     */
    private static void getAddress() {
        InetAddress domainInetAddress = null;
        try {
            domainInetAddress = InetAddress.getByName("www.baidu.com");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        String hostAddress = domainInetAddress.getHostAddress();  // 获取ip地址
        String hostName = domainInetAddress.getHostName();  // 获取域名
        System.out.println(hostAddress);
        System.out.println(hostName);
    }
}
