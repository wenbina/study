输入字符流:
---------| Reader   所有输入字符流的基类。  抽象类。
----------------| FileReader 读取文件数据的输入字符流。 
----------------| BufferedReader 缓冲输入字符流     
该类出现的目的是为了提高读取文件数据的效率与拓展FileReader的(readLine)功能。  这个类的也只不过是在内部维护了一个8kb的字符数组而已。
