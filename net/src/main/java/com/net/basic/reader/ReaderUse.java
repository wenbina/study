package com.net.basic.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * @Author: w
 * @Date: 2021/8/13 11:02
 */
public class ReaderUse {

    public void readMethod() {
        try {
            // 创建读取文件
            File file = new File("E:\\ownspace\\study\\words.txt");
            // 创建字符输入流
            FileReader reader = new FileReader(file);
            // 文件输入流
            //this.readByFileReader(reader);
            // 缓冲字符流
            readByBufferReader(reader);
            // 关闭资源
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 通过fileReader读取文件内容
    private void readByFileReader(FileReader reader) {
        try {
            int len = 0;
            // 循环读取文件
            while ((len = reader.read()) != -1) {
                System.out.print((char)len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 通过缓冲输入流读取文件内容
    private void readByBufferReader(FileReader reader) {
        try {
            // 将文件输入流转成换成输入流  使用缓冲字符流提高效率
            BufferedReader bufferedReader = new BufferedReader(reader);
            System.out.println(bufferedReader.readLine());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
