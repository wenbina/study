package com.net.basic.outputstream.buffer;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

/**
 * @Author: w
 * @Date: 2021/8/13 10:48
 * 使用BufferedOutputStream的步骤：
 * 	 	1. 找到目标文件
 * 	 	2. 建立数据的输出通道
 * 	BufferedOutputStream 要注意的细节
 * 		1. 使用BufferedOutStream写数据的时候，它的write方法是是先把数据写到它内部维护的字节数组中。
 * 	 	2. 需要调用flush方法或者是close方法、 或者是内部维护的字节数组已经填满数据的时候。
 */
public class BufferOutputStreamUse {

    public void write() {
        try {
            // 创建写出目标文件位置
            File file = new File("E:\\ownspace\\study\\net\\src\\main\\java\\com\\net\\basic\\outputstream\\buffer\\files\\words.txt");
            // 建立数据通道
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            // 将数据写入到数据通道中
            bos.write("hello bufferedOutputStream".getBytes());
            // 将数据刷新到数据通道
            bos.flush();
            // 关闭资源
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
