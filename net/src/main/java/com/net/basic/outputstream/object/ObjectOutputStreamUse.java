package com.net.basic.outputstream.object;

import com.net.basic.entity.Student;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * @Author: w
 * @Date: 2021/8/13 11:38
 * 对象输入流使用
 */
public class ObjectOutputStreamUse {

    public void write() {
        try {
            // 创建写入目标文件
            File file = new File("E:\\ownspace\\study\\net\\src\\main\\java\\com\\net\\receive\\student.txt");
            // 创建数据通道
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file));
            // 创建数据
            Student student = new Student();
            student.setName("张三");
            student.setSex("男");
            student.setAge(18);
            // 将数据写入到数据通道中
            oos.writeObject(student);
            // 将数据刷新到数据通道
            oos.flush();
            // 关闭
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
