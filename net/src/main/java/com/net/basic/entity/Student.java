package com.net.basic.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/13 11:34
 */
@Data
public class Student implements Serializable {

    private String name;

    private Integer age;

    private String sex;
}
