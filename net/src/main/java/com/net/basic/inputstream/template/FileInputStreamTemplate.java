package com.net.basic.inputstream.template;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

/**
 * @Author: w
 * @Date: 2021/8/13 8:56
 * 模板方法：抽象：使用FileInputStream的步骤是固定的，只有读取的方式不一样，所以使用模板方法复用代码
 */
public abstract class FileInputStreamTemplate {

    // 基础方法
    public void basicMethod() {
        // 找到目标文件
        File file = new File("E:\\ownspace\\study\\words.txt");
        try {
            // 创建输入流将文件写入到数据通道中
            FileInputStream fis = new FileInputStream(file);
            // 读取数据
            readMethod(fis);
            // 关闭资源
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 读取方法
    public abstract void readMethod(FileInputStream fis);
}
