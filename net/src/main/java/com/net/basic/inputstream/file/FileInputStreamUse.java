package com.net.basic.inputstream.file;

import com.net.basic.inputstream.template.FileInputStreamTemplate;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @Author: w
 * @Date: 2021/8/13 8:47
 * 文件输入流使用
 *
 * 使用FileInputStream读取文件数据的步骤：
 * 1. 找到目标文件
 * 2. 建立数据的输入通道。
 * 3. 读取文件中的数据。
 * 4. 关闭 资源.
 */
public class FileInputStreamUse extends FileInputStreamTemplate {

    @Override
    public void readMethod(FileInputStream fis) {
        // 1：基础读取文件方式：读取单个字符
        //readNormal(fis);
        // 2：循环读取完整文件，读取完之后会返回-1
        //readAll(fis);
        // 3：使用缓冲数组读取完整文件
        //readNormalByBuffer(fis);
        // 4：使用缓冲数组读取完整文件
        readAllByBuffer(fis);

    }

    /**
     * 正常读取方式：只读取一个字符
     */
    private void readNormal(FileInputStream fis) {
        try {
            System.out.println((char) fis.read());
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }


    /**
     * 循环读取完整文件，读取完之后会返回-1
     */
    private void readAll(FileInputStream fis) {
        try {
            int content = 0;
            while ((content = fis.read()) != -1) {
                System.out.print((char) content);
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * 使用缓冲数组读取
     */
    private void readNormalByBuffer(FileInputStream fis) {
        try {
            byte[] bytes = new byte[1024];
            //如果使用read读取数据传入字节数组，那么数据是存储到字节数组中的，而这时候read方法的返回值是表示的是本次读取了几个字节数据到字节数组中。
            int read = fis.read(bytes);
            String content = new String(bytes, 0, read);
            System.out.println(content);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    /**
     * 使用缓冲数组读取完整文件
     */
    private void readAllByBuffer(FileInputStream fis) {
        try {
            byte[] bytes = new byte[1024];
            int length = 0;
            while ((length = fis.read(bytes)) != -1) {
                System.out.println(new String(bytes, 0, length));
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

}
