package com.net.basic.inputstream.buffer;

import com.net.basic.inputstream.template.BufferInputStreamTemplate;


/**
 * @Author: w
 * @Date: 2021/8/13 10:45
 */
public class TestBufferInputStream {

    public static void main(String[] args) {
        BufferInputStreamTemplate bis = new BufferInputStreamUse();
        bis.basicMethod();
    }
}
