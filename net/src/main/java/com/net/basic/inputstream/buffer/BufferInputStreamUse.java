package com.net.basic.inputstream.buffer;

import com.net.basic.inputstream.template.BufferInputStreamTemplate;

import java.io.BufferedInputStream;

/**
 * @Author: w
 * @Date: 2021/8/13 10:34
 * 缓冲输入流使用
 * 使用步骤
 * 1. 找到目标文件。
 * 2. 建立数据 的输入通道
 * 3. 建立缓冲 输入字节流流
 * 4. 关闭资源
 */
public class BufferInputStreamUse extends BufferInputStreamTemplate {

    @Override
    public void readMethod(BufferedInputStream bis) {
        try {
            int len = 0;
            while ((len = bis.read()) != -1) {
                System.out.print((char) len);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
