package com.net.basic.inputstream.template;

import java.io.*;

/**
 * @Author: w
 * @Date: 2021/8/13 10:36
 * 缓冲输入流模板
 */
public abstract class BufferInputStreamTemplate {

    // 基础方法
    public void basicMethod() {
        // 寻找目标文件
        File file = new File("E:\\ownspace\\study\\words.txt");
        try {
            // 创建缓冲输入流
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            // 调用读取方法
            readMethod(bis);
            // 关闭资源
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 读取方法
    public abstract void readMethod(BufferedInputStream bis);
}
