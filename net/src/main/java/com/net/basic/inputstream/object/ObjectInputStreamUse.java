package com.net.basic.inputstream.object;

import com.net.basic.entity.Student;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInputStream;

/**
 * @Author: w
 * @Date: 2021/8/13 11:33
 * 对象输入流使用
 */
public class ObjectInputStreamUse {


    public void readMethod() {
        try {
            // 创建读取目标文件
            File file = new File("E:\\ownspace\\study\\net\\src\\main\\java\\com\\net\\receive\\student.txt");
            // 创建读取对象
            ObjectInputStream oos = new ObjectInputStream(new FileInputStream(file));
            // 读取
            Student student = (Student)oos.readObject();
            System.out.println(student.getName());
            // 关闭
            oos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
