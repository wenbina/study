package com.net.basic.inputstream.file;

import com.net.basic.inputstream.template.FileInputStreamTemplate;

/**
 * @Author: w
 * @Date: 2021/8/13 9:18
 */
public class TestFileInputStream {

    public static void main(String[] args) {
        FileInputStreamTemplate fileInputStreamUse = new FileInputStreamUse();
        fileInputStreamUse.basicMethod();
    }
}
