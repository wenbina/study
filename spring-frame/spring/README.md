引言：
ioc引出：对象之间耦合度过高
对于调用一个其他类的方法，我们首先是new一个对象，然后通过这个对象调用这个方法；这样就会造成一个耦合度高的问题；

ioc过程
1：创建xml配置文件，配置需要创建的对象
<bean id = "dao" class = "com.spring.UserDao"></bean>
2：创建工厂类，通过xml解析和反射动态生成对象
class UserFactory {
    public static UserDao getDao() {
       String classValue = class属性值;  // xml解析
       Class clazz = Class.forName(classValue);  // 2：通过反射创建对象
       return (UserDao)clazz.newInstance();
    }
}

bean管理
（1）spring创建对象
（2）spring注入属性（为对象中的成员变量赋值）
bean管理实现方式：
（1）基于xml配置文件方式实现
    （1）创建对象
        <!--配置对象创建：创建对象的时候默认执行无参构造方法-->
        <bean id="user" class="com.spring.ioc.entity.User"></bean>
    （2）属性注入：DI：依赖注入：方式：1：setter；2：有参构造
        <!--使用setter方式注入属性-->
        <bean id="user" class="com.spring.ioc.entity.User">
            <!--使用setter方式注入属性-->
            <property name="username" value="张三"></property>
            <property name="age" value="18"></property>
        </bean>
        <!--使用有参构造方式注入属性-->
        <bean id="user" class="com.spring.ioc.entity.User">
            <!--使用有参构造方式注入属性-->
            <constructor-arg name="username" value="张三"></constructor-arg>
            <constructor-arg name="age" value="18"></constructor-arg>
        </bean>
（2）基于注解方式实现







###IOC
####1：IOC概念
####2：IOC原理

###AOP
####1：AOP概念
aop：面向切面，不修改源码进行功能增强；利用aop可以对业务逻辑得各个部分进行隔离，从而使得业务逻辑各部分之间得耦合度降低，提高程序得可重用性
####2：AOP原理
使用动态代理实现
（1）有接口情况：使用jdk动态代理（创建接口实现类代理对象）
（2）无接口情况：使用cglib动态代理（创建当前子类的代理对象）

bean的作用域
在spring中，我们可以设置bean是单实例还是多实例：默认情况下是单实例
1：设置为单实例时，加载spring配置文件时就会创建单实例对象
2：设置为多实例时，在调用getBean的时候才会创建多个实例对象


bean的生命周期
1：创建：通过无参构造创建bean实例
2：属性注入：为bean的属性设置值和对其他bean引用（调用set方法）
3：调用bean的初始化方法（进行配置）
4：使用bean
5：容器关闭，调用bean的销毁方法
bean的后置处理器
1：创建：通过无参构造创建bean实例
2：属性注入：为bean的属性设置值和对其他bean引用（调用set方法）
3：把bean的实例传递给bean后置处理器postProcessBeforeInitialization
4：调用bean的初始化方法（进行配置）
5：把bean的实例传递给bean后置处理器postProcessAfterInitialization
6：使用bean
7：容器关闭，调用bean的销毁方法

自动装配常用的两个值
byName根据属性名称注入，注入值bean得id值和类属性名称一样
byType根据属性类型注入

spring针对bean管理中创建对象提供得注解
@Component：普通类
@Service：业务层
@Controller：web层
@Repository：dao层

基于注解方式实现属性注入
@Autowired：根据属性类型进行自动装配
@Qualifier：根据属性名称进行注入
@Resource：既可以根据类型注入，也可以根据名称注入（默认根据类型注入，通过@Rescource(name="xxx")）
@Value：注入普通类型属性


bean的创建流程和阶段
动态代理
spring事务



