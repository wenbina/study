package com.spring;

import com.spring.aspectJ.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

/**
 * @Author: w
 * @Date: 2021/9/1 22:55
 */
@SpringBootTest()
@RunWith(SpringRunner.class)
public class TestAspect {

    @Resource
    private User user;

    @Test
    public void testProxy() {
        this.user.add();
        this.user.delete();
    }
}
