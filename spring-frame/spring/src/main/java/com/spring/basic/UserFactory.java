package com.spring.basic;

import com.spring.basic.service.UserService;

/**
 * @Author: w
 * @Date: 2021/8/29 16:43
 */
public class UserFactory {

    public static UserService getUserService() {
        return new UserService();
    }
}
