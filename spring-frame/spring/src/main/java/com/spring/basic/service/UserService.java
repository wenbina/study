package com.spring.basic.service;

/**
 * @Author: w
 * @Date: 2021/8/29 16:45
 */
public class UserService {

    public void queryUser(String username) {
        System.out.println("您查询的用户名为：" + username);
    }
}
