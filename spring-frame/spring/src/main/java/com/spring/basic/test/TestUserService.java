package com.spring.basic.test;

import com.spring.basic.UserFactory;
import com.spring.basic.service.UserService;

/**
 * @Author: w
 * @Date: 2021/8/29 16:46
 */
public class TestUserService {

    public static void main(String[] args) {
        String username = "张三";
        normalMethod(username);
        factoryMethod(username);
    }

    /**
     * 普通方式调用方法：耦合度太高
     */
    private static void normalMethod(String username) {
        UserService userService = new UserService();
        userService.queryUser(username);
    }

    /**
     * 利用工厂模式解耦
     * 降低了UserService与TestUserService之间的耦合度
     */
    private static void factoryMethod(String username) {
        UserService userService = UserFactory.getUserService();
        userService.queryUser(username);
    }
}
