package com.spring.ioc.test;

import com.spring.ioc.entity.User;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author: w
 * @Date: 2021/8/29 21:19
 */
public class TestUser {


    public static void main(String[] args) {
        useApplicationContext();
        useBeanFactory();
    }

    /**
     * 方式一：ApplicationContext是BeanFactory接口的子接口
     * 加载配置文件的时候就会将配置文件中的对象进行创建
     */
    private static void useApplicationContext() {
        // ClassPathXmlApplicationContext[只能读放在web-info/classes目录下的配置文件]和FileSystemXmlApplicationContext的区别
        //classpath:前缀是不需要的,默认就是指项目的classpath路径下面;
        //如果要使用绝对路径,需要加上file:前缀表示这是绝对路径;
        // 加载spring配置文件
        ApplicationContext context = new ClassPathXmlApplicationContext("file:E:\\ownspace\\study\\spring-frame\\spring\\src\\main\\java\\com\\spring\\ioc\\bookSingle.xml");
        // 获取配置创建的对象
        User user = context.getBean("user", User.class);

        System.out.println(user);
    }

    /**
     * 方式二：BeanFactory接口：是spring的内部接口
     * 特点：加载配置文件的时候不会创建对象，在获取（使用）对象的时候才去创建（懒加载）
     */
    private static void useBeanFactory() {
        // ClassPathXmlApplicationContext[只能读放在web-info/classes目录下的配置文件]和FileSystemXmlApplicationContext的区别
        //classpath:前缀是不需要的,默认就是指项目的classpath路径下面;
        //如果要使用绝对路径,需要加上file:前缀表示这是绝对路径;
        // 加载spring配置文件
        BeanFactory factory = new ClassPathXmlApplicationContext("file:E:\\ownspace\\study\\spring-frame\\spring\\src\\main\\java\\com\\spring\\ioc\\bookSingle.xml");
        // 获取配置创建的对象
        User user = factory.getBean("user", User.class);

        System.out.println(user);
    }
}
