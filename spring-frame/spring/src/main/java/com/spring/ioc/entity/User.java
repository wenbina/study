package com.spring.ioc.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/29 21:17
 */
@Data
public class User {

    private String username;

    private Integer age;

    public User() {
    }

    public User(String username, Integer age) {
        this.username = username;
        this.age = age;
    }
}
