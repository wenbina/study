package com.spring.lifeCycle;

import com.spring.lifeCycle.entity.Orders;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author: w
 * @Date: 2021/8/30 22:43
 */
public class TestLifeCycle {

    public static void main(String[] args) {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("file:E:\\ownspace\\study\\spring-frame\\spring\\src\\main\\java\\com\\spring\\lifeCycle\\bean.xml");
        Orders orders = ctx.getBean("orders", Orders.class);
        System.out.println("步骤四：获取bean对象");
        System.out.println(orders);

        // 手动销毁bean
        ((ClassPathXmlApplicationContext) ctx).close();

    }
}
