package com.spring.lifeCycle.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/30 22:41
 */
public class Orders {

    private String orderNo;

    public Orders() {
        System.out.println("步骤一：调用无参构造创建bean实例");
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
        System.out.println("步骤二：调用set方法设置属性值");
    }

    // 创建执行化的初始化方法
    public void initMethod() {
        System.out.println("步骤三：执行初始化方法");
    }

    // 创建销毁方法
    public void destroyMethod() {
        System.out.println("步骤五：执行销毁方法");
    }
}
