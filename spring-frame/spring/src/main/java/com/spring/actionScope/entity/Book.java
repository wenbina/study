package com.spring.actionScope.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/30 22:22
 */
@Data
public class Book {

    private String name;

    private Double price;
}
