package com.spring.actionScope;

import com.spring.actionScope.entity.Book;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @Author: w
 * @Date: 2021/8/30 22:21
 * bean作用域：分为单实例和多实例：默认为单实例
 *
 * 单实例与多实例的区别
 * 1：设置为单实例时，加载spring配置文件时就会创建单实例对象
 * 2：设置为多实例时，在调用getBean的时候才会创建多个实例对象
 *
 * 分类
 * single
 * prototype
 * request
 * session
 */
public class TestBeanActionScope {

    public static void main(String[] args) {
        single();
        multi();
    }

    // 单例
    private static void single() {
        ApplicationContext context = new ClassPathXmlApplicationContext("file:E:\\ownspace\\study\\spring-frame\\spring\\src\\main\\java\\com\\spring\\actionScope\\bookSingle.xml");
        // 获取配置创建的对象
        Book book1 = context.getBean("book", Book.class);
        Book book2 = context.getBean("book", Book.class);

        System.out.println(book1.hashCode());
        System.out.println(book2.hashCode());
    }


    // 多实例
    private static void multi() {
        ApplicationContext context = new ClassPathXmlApplicationContext("file:E:\\ownspace\\study\\spring-frame\\spring\\src\\main\\java\\com\\spring\\actionScope\\bookMulti.xml");
        // 获取配置创建的对象
        Book book1 = context.getBean("book", Book.class);
        Book book2 = context.getBean("book", Book.class);

        System.out.println(book1.hashCode());
        System.out.println(book2.hashCode());
    }


}
