package com.spring.aspectJ;

import org.springframework.stereotype.Component;

/**
 * @Author: w
 * @Date: 2021/9/1 22:47
 */
@Component
public class User {

    public void add() {
        System.out.println("add方法执行...");
    }

    public void delete() {
        System.out.println("delete方法执行...");
    }
}
