package com.spring.aspectJ;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @Author: w
 * @Date: 2021/9/1 22:48
 * 增强类
 */
@Component
@Aspect //生成代理对象
public class UserProxy {

    // 前置通知
    @Before(value = "execution(* com.spring.aspectJ.User.add(..))")
    public void before() {
        System.out.println("before....");
    }

    // 前置通知
    @Before(value = "execution(* com.spring.aspectJ.User.delete(..))")
    public void beforeDelete() {
        System.out.println("before....");
    }
}
