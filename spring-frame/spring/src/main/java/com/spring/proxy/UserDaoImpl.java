package com.spring.proxy;

/**
 * @Author: w
 * @Date: 2021/9/1 22:18
 */
public class UserDaoImpl implements UserDao {

    @Override
    public Integer add(Integer a, Integer b) {
        System.out.println("add方法执行");
        return a + b;
    }
}
