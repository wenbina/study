package com.spring.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Author: w
 * @Date: 2021/9/1 22:19
 */
public class TestProxy {

    public static void main(String[] args) {
        // 创建接口实现类代理对象
        Class[] interfaces = {UserDao.class};
        UserDaoImpl userDao = new UserDaoImpl();
        UserDao dao = (UserDao)Proxy.newProxyInstance(TestProxy.class.getClassLoader(),interfaces, new MyInvocationHandler(userDao));
        Integer add = dao.add(1, 3);
        System.out.println(add);
    }
}


class MyInvocationHandler implements InvocationHandler {

    private Object obj;

    public MyInvocationHandler(Object obj) {
        this.obj = obj;
    }

    // 增强的逻辑
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        // 方法之前
        System.out.println("方法之前执行...");

        // 被增强方法执行
        Object invoke = method.invoke(obj, args);

        // 方法之后
        System.out.println("方法之后执行...");
        return invoke;
    }
}
