package com.spring.proxy;

/**
 * @Author: w
 * @Date: 2021/9/1 22:18
 */
public interface UserDao {

    Integer add(Integer a,Integer b);
}
