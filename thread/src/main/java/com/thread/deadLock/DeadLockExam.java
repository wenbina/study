package com.thread.deadLock;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author: w
 * @Date: 2021/8/16 8:59
 * 死锁
 */
@Slf4j
public class DeadLockExam {

    // 左边
    private static final Object LEFT = new Object();

    // 右边
    private static final Object RIGHT = new Object();

    public static void main(String[] args) {
        passBridge();
    }

    /**
     * 过独木桥
     * 两个人在桥两边分别出发：
     * A在左边出发；B在右边出发：两个人碰到一起谁也不愿意让谁导致死锁
     */
    private static void passBridge() {
        Thread thread1 = new Thread(() -> {
            synchronized (LEFT) {
                log.debug("{}：正在从左边出发...", Thread.currentThread().getName());
                synchronized (RIGHT) {
                    log.debug("{}：正在从右边出发...", Thread.currentThread().getName());
                }
            }
        }, "路人甲");

        Thread thread2 = new Thread(() -> {
            synchronized (RIGHT) {
                log.debug("{}：正在从右边出发...", Thread.currentThread().getName());
                synchronized (LEFT) {
                    log.debug("{}：正在从左边出发...", Thread.currentThread().getName());
                }
            }
        }, "路人乙");

        thread1.start();
        thread2.start();
    }
}
