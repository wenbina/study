package com.netty.bio.server;

import com.netty.bio.entity.Message;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Executors;

/**
 * @Author: w
 * @Date: 2021/8/23 8:44
 */
public class BioServer {

    /**
     * 版本一：客户端连接之后若没有发送信息会阻塞在read方法处：
     * 也就是说：单线程情况下，某个客户端若是连接了然后未向服务端发送信息，服务端就会阻塞在read方法处；
     * 即使有其他客户端连接进行发送消息也还是阻塞；
     * 改进：通过多线程创建多个通信通道，防止io阻塞在一处
     */
    public void receiveMsgVersion1(Integer port, Message message) {
        try {
            // 创建serverSocket建立连接
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("等待客户端连接...");
            // 客户端未连接之前都会阻塞在这个地方
            Socket socket = serverSocket.accept();
            // 读取客户端数据
            ObjectInputStream ois = this.readClientData(socket);

            // 发送信息给客户端
            ObjectOutputStream oos = this.sendToClientMessage(socket, message);

            // 关闭资源
            //this.closeResource(serverSocket, socket, ois, oos);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 版本二：通过多线程（线程池）方式：创建一个新的线程去处理与客户端的连接
     * <p>
     * 存在问题：一个线程处理一个连接，若大部分连接都不发送消息的话就会造成线程资源浪费
     */
    public void receiveMsgVersion2(Integer port, Message message) {
        try {
            // 创建serverSocket建立连接
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("等待客户端连接...");
            while (true) {
                // 客户端未连接之前都会阻塞在这个地方
                Socket socket = serverSocket.accept();
                // 采用线程池方式：好处就是可以线程复用，减少线程的创建和销毁
                Executors.newCachedThreadPool().submit(() -> {
                    // 读取客户端数据
                    ObjectInputStream ois = this.readClientData(socket);
                    // 发送信息给客户端
                    ObjectOutputStream oos = this.sendToClientMessage(socket, message);
                    // 关闭资源
                    //this.closeResource(serverSocket, socket, ois, oos);
                });
                // 采用多线程方式
                /*new Thread(() -> {
                    // 读取客户端数据
                    ObjectInputStream ois = this.readClientData(socket);
                    // 发送信息给客户端
                    ObjectOutputStream oos = this.sendToClientMessage(socket, message);
                    // 关闭资源
                    this.closeResource(serverSocket, socket, ois, oos);
                }).start();*/
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 读取客户端数据
    private ObjectInputStream readClientData(Socket socket) {
        try {
            // 通过socket获取输入流
            InputStream inputStream = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(inputStream);
            // 读取客户端传输数据：客户端没有数据发送也会阻塞在这里
            Object clientMsg = ois.readObject();
            System.out.println("客户端传输数据为：" + clientMsg);
            return ois;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 发送信息给客户端
    private ObjectOutputStream sendToClientMessage(Socket socket, Message message) {
        try {
            // 响应给客户端数据
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject(message);
            return oos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 关闭资源
    private void closeResource(ServerSocket serverSocket, Socket socket, ObjectInputStream ois, ObjectOutputStream oos) {
        // 关闭资源
        try {
            oos.close();
            ois.close();
            socket.close();
            serverSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
