package com.netty.bio.client;

import com.netty.bio.entity.Message;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/8/23 8:44
 */
public class BioClient {

    // 发送信息
    public void sendMessage(String host, Integer port, Message message) {
        try {
            // 创建socket对象
            Socket socket = new Socket(host, port);
            //通过socket创建输出流写数据
            ObjectOutputStream oos = this.sendToServerMsg(socket, message);
            // 读取服务端消息
            ObjectInputStream ois = this.readServerMsg(socket);
            // 关闭资源
            this.closeResource(socket,oos,ois);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 通过socket创建输出流写数据
    private ObjectOutputStream sendToServerMsg(Socket socket, Message message) {
        try {
            // 通过socket对象创建输出流
            OutputStream outputStream = socket.getOutputStream();
            // 将消息写入输出流中
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject(message);
            return oos;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 读取服务端消息
    private ObjectInputStream readServerMsg(Socket socket) {
        try {
            // 通过socket对象创建输入流
            InputStream inputStream = socket.getInputStream();
            // 读取输入流中的信息
            ObjectInputStream ois = new ObjectInputStream(inputStream);
            Object responseMsg = ois.readObject();
            System.out.println(responseMsg);
            return ois;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    // 关闭资源
    private void closeResource(Socket socket, ObjectOutputStream oos, ObjectInputStream ois) {
        try {
            // 关闭资源
            ois.close();
            oos.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
