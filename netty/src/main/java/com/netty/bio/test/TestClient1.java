package com.netty.bio.test;

import com.netty.bio.client.BioClient;
import com.netty.bio.entity.Message;

/**
 * @Author: w
 * @Date: 2021/8/23 9:36
 */
public class TestClient1 {

    public static void main(String[] args) {
        Message message = new Message();
        message.setContent("日子真操蛋，但你还是要努力啊");
        BioClient bioClient = new BioClient();
        testVersion1(bioClient,message);
    }

    private static void testVersion1(BioClient bioClient,Message message) {
        bioClient.sendMessage("localhost",9999,message);
    }
}
