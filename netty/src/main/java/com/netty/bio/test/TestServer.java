package com.netty.bio.test;

import com.netty.bio.entity.Message;
import com.netty.bio.server.BioServer;

/**
 * @Author: w
 * @Date: 2021/8/23 9:36
 */
public class TestServer {

    public static void main(String[] args) {
        Message message = new Message();
        message.setContent("是呀，即使日子再操蛋，你也得坚持下来去看更美好的生活啊");
        BioServer bioServer = new BioServer();
        testVersion1(bioServer,message);
        //testVersion2(bioServer,message);
    }

    // 版本1：单线程连接
    private static void testVersion1(BioServer bioServer,Message message) {
        bioServer.receiveMsgVersion1(9999,message);
    }

    // 版本2：多线程处理客户端连接
    private static void testVersion2(BioServer bioServer,Message message) {
        bioServer.receiveMsgVersion2(9999,message);
    }

}
