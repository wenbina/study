package com.netty.bio.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/23 8:44
 */
@Data
public class Message implements Serializable {

    private String content;

}
