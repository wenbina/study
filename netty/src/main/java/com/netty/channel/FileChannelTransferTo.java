package com.netty.channel;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;

/**
 * @Author: w
 * @Date: 2021/8/14 9:27
 * 文件传输
 */
public class FileChannelTransferTo {

    public static void main(String[] args) {
        try {
            // 创建输入流读取文件内容
            FileChannel fisChannel = new FileInputStream("from.txt").getChannel();
            // 创建输出流写入文件内容
            FileChannel fosChannel = new FileOutputStream("to.txt").getChannel();
            // 调用transfer进行写入：底层通过零拷贝优化：传输数据为2g
            // left 表示还剩余的字节
            for (long left = fisChannel.size(); left >0; ) {
                // 每次传输数据起始值为文件大小减去上次开始的位置，传输的数量就是还剩余的数量
                left -= fisChannel.transferTo(fisChannel.size() - left,left,fosChannel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
