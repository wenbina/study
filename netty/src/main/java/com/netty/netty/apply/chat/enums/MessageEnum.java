package com.netty.netty.apply.chat.enums;

/**
 * @Author: w
 * @Date: 2021/8/18 14:40
 */
public enum MessageEnum {

    LOGIN_MESSAGE("login_message","登陆消息"),
    FRIEND_LIST("friend_list","获取好友列表");

    public String type;

    public String desc;

    MessageEnum(String type, String desc) {
        this.type = type;
        this.desc = desc;
    }
}
