package com.netty.netty.apply.chat.handler;

import com.netty.netty.apply.chat.entity.JoinGroupRequestMessage;
import com.netty.netty.apply.chat.entity.JoinGroupResponseMessage;
import com.netty.netty.apply.chat.server.session.impl.Group;
import com.netty.netty.apply.chat.server.session.impl.GroupSessionFactoryImpl;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: w
 * @Date: 2021/8/20 17:12
 */
@ChannelHandler.Sharable
public class JoinGroupHandler extends SimpleChannelInboundHandler<JoinGroupRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, JoinGroupRequestMessage joinGroupRequestMessage) throws Exception {
        String groupName = joinGroupRequestMessage.getGroupName();
        String memberName = joinGroupRequestMessage.getMemberName();

        Group group = GroupSessionFactoryImpl.getGroupSession().joinGroup(groupName, memberName);
        JoinGroupResponseMessage joinGroupResponseMessage = new JoinGroupResponseMessage();
        if (group != null) {
            joinGroupResponseMessage.setIsSuccess(true);
        } else {
            joinGroupResponseMessage.setIsSuccess(false);
        }
        joinGroupResponseMessage.setGroupName(groupName);
        ctx.writeAndFlush(joinGroupResponseMessage);
    }
}
