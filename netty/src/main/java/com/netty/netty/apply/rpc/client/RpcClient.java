package com.netty.netty.apply.rpc.client;

import com.netty.netty.apply.rpc.entity.RpcRequestMessage;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * @Author: w
 * @Date: 2021/8/19 22:46
 */
public class RpcClient {

    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel channel) throws Exception {
                    channel.pipeline().addLast(new ObjectEncoder());
                    channel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                    channel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                        @Override
                        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                            System.out.println(msg);
                        }
                    });
                }
            });
            Channel channel = bootstrap.connect("localhost", 9999).sync().channel();
            RpcRequestMessage rpcRequestMessage = new RpcRequestMessage();
            rpcRequestMessage.setInterfaceName("com.netty.netty.apply.rpc.client.service.impl.HelloServiceImpl");
            rpcRequestMessage.setMethodName("hello");
            rpcRequestMessage.setParameterTypes(new Class[]{String.class});
            rpcRequestMessage.setParameterValue(new Object[] {"张三"});
            channel.writeAndFlush(rpcRequestMessage);

            channel.closeFuture().sync();
        } catch (Exception e) {
            System.out.println("客户端出现异常");
        } finally {
            group.shutdownGracefully();
        }
    }
}
