package com.netty.netty.apply.chat.server.service;

/**
 * @Author: w
 * @Date: 2021/8/18 14:44
 */
public interface UserService {

    // 登陆
    boolean login(String username, String password);
}
