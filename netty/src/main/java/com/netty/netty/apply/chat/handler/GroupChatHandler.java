package com.netty.netty.apply.chat.handler;

import cn.hutool.core.collection.CollectionUtil;
import com.netty.netty.apply.chat.entity.GroupChatRequestMessage;
import com.netty.netty.apply.chat.entity.GroupChatResponseMessage;
import com.netty.netty.apply.chat.server.session.impl.GroupSessionFactoryImpl;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/20 10:47
 */
@ChannelHandler.Sharable
public class GroupChatHandler extends SimpleChannelInboundHandler<GroupChatRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GroupChatRequestMessage groupChatRequestMessage) throws Exception {
        String groupName = groupChatRequestMessage.getGroupName();
        String content = groupChatRequestMessage.getContent();
        String send = groupChatRequestMessage.getSend();
        // 通过群聊名称获取全部channel
        Set<Channel> allChannel = GroupSessionFactoryImpl.getGroupSession().getAllChannel(groupName);
        if (CollectionUtil.isNotEmpty(allChannel)) {
            for (Channel channel : allChannel) {
                GroupChatResponseMessage groupChatResponseMessage = new GroupChatResponseMessage();
                groupChatResponseMessage.setSend(send);
                groupChatResponseMessage.setContent(content);
                channel.writeAndFlush(groupChatResponseMessage);
            }
        } else {
            GroupChatResponseMessage groupChatResponseMessage = new GroupChatResponseMessage();
            groupChatResponseMessage.setSend(send);
            groupChatResponseMessage.setContent("群聊不存在");
            ctx.writeAndFlush(groupChatResponseMessage);
        }
    }
}
