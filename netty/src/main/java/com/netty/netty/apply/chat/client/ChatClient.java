package com.netty.netty.apply.chat.client;

import com.netty.netty.apply.chat.entity.*;
import com.netty.netty.apply.chat.enums.MessageEnum;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import org.apache.commons.lang3.StringUtils;

import java.util.Scanner;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @Author: w
 * @Date: 2021/8/18 14:25
 * 客户端
 * 注意：传输对象里面不能存放Set或Map以及对象类型
 */
public class ChatClient {

    // 等待标记：等待登陆成功
    private static CountDownLatch WAIT_FLAG = new CountDownLatch(1);
    // 是否登陆成功
    private static AtomicBoolean LOGIN_SUCCESS = new AtomicBoolean(false);

    // 记录登陆人
    private static String USER;

    public static void main(String[] args) {
        startTemplate();
    }

    // 启动模板
    public static void startTemplate() {
        // 创建事件循环组
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            // 创建启动器
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    // 添加编码解码器
                    ch.pipeline().addLast(new ObjectEncoder());
                    ch.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                    // 添加读入事件处理器
                    ch.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                        // 与服务端建立连接之后会触发active事件
                        @Override
                        public void channelActive(ChannelHandlerContext ctx) throws Exception {
                            login(ctx);
                        }
                        // 读事件
                        @Override
                        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                            // 登陆消息
                            if (msg instanceof LoginResponseMessage) {
                                LoginResponseMessage loginResponseMessage = (LoginResponseMessage) msg;
                                readLoginResponseMsg(loginResponseMessage);
                            }
                            // 拉取好友
                            if (msg instanceof GetFriendsResponseMessage) {
                                GetFriendsResponseMessage getFriendsResponseMessage = (GetFriendsResponseMessage) msg;
                                readGetFriendsMsg(getFriendsResponseMessage);
                            }
                            // 发送信息
                            if (msg instanceof ChatResponseMessage) {
                                ChatResponseMessage chatResponseMessage = (ChatResponseMessage) msg;
                                readChatWithFriend(chatResponseMessage);
                            }
                            // 创建群聊
                            if (msg instanceof CreateGroupResponseMessage) {
                                CreateGroupResponseMessage createGroupResponseMessage = (CreateGroupResponseMessage) msg;
                                readCreateGroup(createGroupResponseMessage);
                            }
                            // 群聊消息
                            if (msg instanceof GroupChatResponseMessage) {
                                GroupChatResponseMessage groupChatResponseMessage = (GroupChatResponseMessage)msg;
                                readGroupMsg(groupChatResponseMessage);
                            }
                            // 退出群聊
                            if (msg instanceof RemoveGroupResponseMessage) {
                                RemoveGroupResponseMessage removeGroupResponseMessage = (RemoveGroupResponseMessage)msg;
                                readRemoveMsg(removeGroupResponseMessage);
                            }
                            // 加入群聊
                            if (msg instanceof JoinGroupResponseMessage) {
                                JoinGroupResponseMessage joinGroupResponseMessage = (JoinGroupResponseMessage) msg;
                                readJoinMsg(joinGroupResponseMessage);
                            }
                        }
                    });
                }
            });
            Channel channel = bootstrap.connect("localhost", 9999).sync().channel();
            channel.closeFuture().sync();
        } catch (Exception e) {
            System.out.println("客户端出现异常...");
        } finally {
            group.shutdownGracefully();
        }

    }

    // 输入登陆信息
    private static void login(ChannelHandlerContext ctx) {
        new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入您的账号");
            String username = scanner.nextLine();
            System.out.println("请输入您的密码");
            String password = scanner.nextLine();
            LoginRequestMessage requestMessage = new LoginRequestMessage();
            requestMessage.setUsername(username);
            requestMessage.setPassword(password);
            requestMessage.setMegType(MessageEnum.LOGIN_MESSAGE.type);
            ctx.writeAndFlush(requestMessage);
            // 进行等待
            try {
                WAIT_FLAG.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // 登陆失败
            if (!LOGIN_SUCCESS.get()) {
                ctx.channel().close();
                return;
            }
            USER = username;
            // 选择菜单
            menus(ctx);

        }, "system_in").start();
    }

    // 菜单选择功能
    private static void menus(ChannelHandlerContext ctx) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("==================请选择您的下一步操作=================");
            System.out.println("1：获取在线用户");
            System.out.println("2：与好友聊天");
            System.out.println("3：创建群聊");
            System.out.println("4：发送群消息");
            System.out.println("5：退出群聊");
            System.out.println("6：加入群聊");
            // 退出的时候发送消息给服务端移除channel
            System.out.println("7：退出");
            String option = scanner.nextLine();
            switch (option) {
                case "1":
                    getFriendListOption(ctx);
                    break;
                case "2":
                    chatWithFriendsOption(ctx,scanner);
                    break;
                case "3":
                    createGroupOption(ctx,scanner);
                    break;
                case "4":
                    sendMsgToGroupOption(ctx,scanner);
                    break;
                case "5":
                    removeGroupOption(ctx,scanner);
                    break;
                case "6":
                    joinGroupOption(ctx,scanner);
                    break;
                case "7":
                    quitOption(ctx);
                    break;
            }
        }
    }

    // 拉取好友列表
    private static void getFriendListOption(ChannelHandlerContext ctx) {
        GetFriendsMessage message = new GetFriendsMessage();
        ctx.writeAndFlush(message);
    }

    // 与好友聊天操作
    private static void chatWithFriendsOption(ChannelHandlerContext ctx,Scanner scanner) {
        System.out.println("请选择您的好友");
        String selectFriend = scanner.nextLine();
        System.out.println("请输入您需要发送的消息");
        String content = scanner.nextLine();
        ChatRequestMessage chatRequestMessage = new ChatRequestMessage();
        chatRequestMessage.setSend(USER);
        chatRequestMessage.setReceive(selectFriend);
        chatRequestMessage.setContent(content);
        ctx.writeAndFlush(chatRequestMessage);
    }

    // 创建群聊操作
    private static void createGroupOption(ChannelHandlerContext ctx,Scanner scanner) {
        System.out.println("请输入您创建群聊的名称");
        String groupName = scanner.nextLine();
        System.out.println("请输入群聊成员，并用，隔开");
        String members = scanner.nextLine();
        CreateGroupRequestMessage createGroupRequestMessage = new CreateGroupRequestMessage();
        createGroupRequestMessage.setGroupLeader(USER);
        createGroupRequestMessage.setGroupName(groupName);
        createGroupRequestMessage.setMembers(members);
        ctx.writeAndFlush(createGroupRequestMessage);
    }

    // 发送群消息
    private static void sendMsgToGroupOption(ChannelHandlerContext ctx,Scanner scanner) {
        System.out.println("请输入群聊的名称");
        String groupName = scanner.nextLine();
        System.out.println("请输入发送消息");
        String message = scanner.nextLine();
        GroupChatRequestMessage groupChatRequestMessage = new GroupChatRequestMessage();
        groupChatRequestMessage.setGroupName(groupName);
        groupChatRequestMessage.setContent(message);
        groupChatRequestMessage.setSend(USER);
        ctx.writeAndFlush(groupChatRequestMessage);
    }

    // 退出群聊
    private static void removeGroupOption(ChannelHandlerContext ctx,Scanner scanner) {
        System.out.println("请输入群聊的名称");
        String groupName = scanner.nextLine();
        RemoveGroupRequestMessage removeGroupRequestMessage = new RemoveGroupRequestMessage();
        removeGroupRequestMessage.setMemberName(USER);
        removeGroupRequestMessage.setGroupName(groupName);
        ctx.writeAndFlush(removeGroupRequestMessage);
    }

    // 加入群聊
    private static void joinGroupOption(ChannelHandlerContext ctx,Scanner scanner) {
        System.out.println("请输入群聊名称");
        String groupName = scanner.nextLine();
        JoinGroupRequestMessage joinGroupRequestMessage = new JoinGroupRequestMessage();
        joinGroupRequestMessage.setMemberName(USER);
        joinGroupRequestMessage.setGroupName(groupName);
        ctx.writeAndFlush(joinGroupRequestMessage);
    }

    // 退出
    private static void quitOption(ChannelHandlerContext ctx) {
        System.out.println("正在退出...");
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ctx.channel().close();
    }

    // 读取登陆消息
    private static void readLoginResponseMsg(LoginResponseMessage msg) {
        System.out.println(msg.getContent());
        if (msg.getIsSuccess()) {
            // 登陆成功：进行后续操作
            LOGIN_SUCCESS.set(true);
        }
        WAIT_FLAG.countDown();
    }

    // 读取拉取好友返回信息
    private static void readGetFriendsMsg(GetFriendsResponseMessage msg) {
        String newContent = msg.getContent().replace(USER + " ", "");
        if (StringUtils.isNotEmpty(newContent)) {
            System.out.println("您的好友：" + newContent + "正在线上");
        } else {
            System.out.println("暂无好友在线上");
        }
    }

    // 好友聊天
    private static void readChatWithFriend(ChatResponseMessage msg) {
        // 发送信息
        String send = msg.getSend();
        String content = msg.getContent();
        System.out.println("【" + send + "】：" + content);
    }

    // 创建群聊
    private static void readCreateGroup(CreateGroupResponseMessage msg) {
        System.out.println("【系统】：群聊" + msg.getContent());
    }

    // 读取群聊消息
    private static void readGroupMsg(GroupChatResponseMessage groupChatResponseMessage) {
        String showSend = USER.equals(groupChatResponseMessage.getSend()) ? "自己" : groupChatResponseMessage.getSend();
        System.out.println("【" + showSend + "】：" + groupChatResponseMessage.getContent());
    }

    // 读取退出群聊消息
    private static void readRemoveMsg(RemoveGroupResponseMessage removeGroupResponseMessage) {
        Boolean isSuccess = removeGroupResponseMessage.getIsSuccess();
        if (USER.equals(removeGroupResponseMessage.getReceive())) {
            if (isSuccess) {
                System.out.println("【系统】：您已退出群聊" + removeGroupResponseMessage.getGroupName());
            } else {
                System.out.println("退出群聊失败");
            }
        } else {
            System.out.println(removeGroupResponseMessage.getContent());
        }
    }

    // 加入群聊
    private static void readJoinMsg(JoinGroupResponseMessage joinGroupResponseMessage) {
        Boolean isSuccess = joinGroupResponseMessage.getIsSuccess();
        if (isSuccess) {
            System.out.println("【系统】：你已成功加入群聊" + joinGroupResponseMessage.getGroupName());
        } else {
            System.out.println("【系统】：加入群聊失败");
        }
    }


}
