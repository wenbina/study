package com.netty.netty.apply.chat.service;

import com.netty.netty.apply.chat.entity.Message;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

import java.net.InetSocketAddress;


/**
 * @Author: w
 * @Date: 2021/8/17 8:55
 *
 * 注意：
 * 1：使用SimpleChannelInboundHandler这个类的时候，需要看客户端发送什么类型的数据，比如String类型数据，我们就需要指定SimpleChannelInboundHandler的类也是String类
 *
 */
public class ChatServer {

    public static void main(String[] args) {
        // 定义循环事件组
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        // 创建启动器
        ServerBootstrap serverBootstrap = new ServerBootstrap();
        // 添加循环事件组
        serverBootstrap.group(boss,worker);
        // 设置channel
        serverBootstrap.channel(NioServerSocketChannel.class);
        // 设置处理器
        serverBootstrap.childHandler(new ChannelInitializer<NioSocketChannel>() {
            @Override
            protected void initChannel(NioSocketChannel channel) throws Exception {
                // 添加编码器、解码器
                channel.pipeline().addLast(new ObjectEncoder());
                channel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                channel.pipeline().addLast(new SimpleChannelInboundHandler<Message>() {
                    @Override
                    public void channelActive(ChannelHandlerContext ctx) throws Exception {
                        System.out.println("客户端连接成功...");
                    }

                    @Override
                    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Message msg) throws Exception {
                        System.out.println(msg);
                    }
                });
            }
        });
        serverBootstrap.bind(new InetSocketAddress(9999));
    }
}
