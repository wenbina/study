package com.netty.netty.apply.chat.server.session.impl;

import com.netty.netty.apply.chat.server.session.Session;

/**
 * @Author: w
 * @Date: 2021/8/18 17:06
 */
public abstract class SessionFactory {

    private static Session session = new SessionImpl();

    public static Session getSession() {
        return session;
    }
}
