package com.netty.netty.apply.rpc.handler;

import com.netty.netty.apply.rpc.client.service.HelloService;
import com.netty.netty.apply.rpc.client.service.impl.HelloServiceImpl;
import com.netty.netty.apply.rpc.entity.RpcRequestMessage;
import com.netty.netty.apply.rpc.entity.RpcResponseMessage;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * @Author: w
 * @Date: 2021/8/19 22:58
 */
@ChannelHandler.Sharable
public class RpcRequestHandler extends SimpleChannelInboundHandler<RpcRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RpcRequestMessage rpcRequestMessage) throws Exception {
        RpcResponseMessage rpcResponseMessage = new RpcResponseMessage();
        try {
            Class cla = Class.forName(rpcRequestMessage.getInterfaceName());
            HelloServiceImpl helloService = (HelloServiceImpl) cla.newInstance();
            Method hello = cla.getMethod(rpcRequestMessage.getMethodName(),rpcRequestMessage.getParameterTypes());
            Object invoke = hello.invoke(helloService, rpcRequestMessage.getParameterValue());
            rpcResponseMessage.setReturnValue(invoke);
        } catch (Exception e) {
            rpcResponseMessage.setExceptionValue(e);
        }
        ctx.writeAndFlush(rpcResponseMessage);
    }

    public static void main(String[] args) throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        RpcRequestMessage rpcRequestMessage = new RpcRequestMessage();
        rpcRequestMessage.setInterfaceName("com.netty.netty.apply.rpc.client.service.impl.HelloServiceImpl");
        rpcRequestMessage.setMethodName("hello");
        rpcRequestMessage.setParameterTypes(new Class[]{String.class});
        rpcRequestMessage.setParameterValue(new Object[] {"张三"});


        Class cla = Class.forName(rpcRequestMessage.getInterfaceName());
        HelloServiceImpl helloService = (HelloServiceImpl) cla.newInstance();
        Method hello = cla.getMethod(rpcRequestMessage.getMethodName(),rpcRequestMessage.getParameterTypes());
        Object invoke = hello.invoke(helloService, rpcRequestMessage.getParameterValue());
        System.out.println(invoke);


    }
}
