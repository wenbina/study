package com.netty.netty.apply.heartbeat;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.handler.timeout.IdleStateEvent;

/**
 * @Author: w
 * @Date: 2021/8/21 9:01
 * 心跳处理器
 */
public class HeartbeatHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        String responseMsg = null;
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent)evt;
            switch (event.state()) {
                case READER_IDLE:
                    responseMsg = "【系统】：您已经3秒未进行读取操作了";
                    System.out.println("读空闲..."); break;
                case WRITER_IDLE:
                    responseMsg = "【系统】：您已经5秒未进行写入操作了";
                    System.out.println("写空闲..."); break;
                case ALL_IDLE:
                    responseMsg = "【系统】：您已经7秒未进行读写操作了";
                    System.out.println("读写空闲..."); break;
            }
            ctx.channel().writeAndFlush(responseMsg);
        }
    }
}
