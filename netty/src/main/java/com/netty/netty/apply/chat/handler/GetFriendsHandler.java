package com.netty.netty.apply.chat.handler;

import cn.hutool.core.collection.CollectionUtil;
import com.netty.netty.apply.chat.entity.GetFriendsMessage;
import com.netty.netty.apply.chat.entity.GetFriendsResponseMessage;
import com.netty.netty.apply.chat.server.session.impl.SessionFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Set;


/**
 * @Author: w
 * @Date: 2021/8/18 18:25
 * 拉取好友列表处理器
 */
@ChannelHandler.Sharable
public class GetFriendsHandler extends SimpleChannelInboundHandler<GetFriendsMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, GetFriendsMessage msg) throws Exception {
        GetFriendsResponseMessage responseMsg = new GetFriendsResponseMessage();
        Set<String> onlineUsers = SessionFactory.getSession().getOnlineUsers();
        StringBuffer sb = new StringBuffer();
        if (CollectionUtil.isNotEmpty(onlineUsers)) {
            for (String onlineUser : onlineUsers) {
                sb.append(onlineUser + " ");
            }
        }
        if (sb.length() > 0) {
            responseMsg.setContent(sb.toString());
        } else {
            responseMsg.setContent("暂无好友在线上");
        }
        ctx.writeAndFlush(responseMsg);
    }
}
