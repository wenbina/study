package com.netty.netty.apply.chat.server.session;

import com.netty.netty.apply.chat.server.session.impl.Group;
import io.netty.channel.Channel;

import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/19 17:14
 */
public interface GroupSession {

    // 创建聊天组
    Group createGroup(String groupName, String groupLeader, Set<String> member);

    // 加入聊天组
    Group joinGroup(String groupName, String memberName);

    // 移除组成员
    Group removeMember(String groupName, String memberName);

    // 获取聊天组全部成员
    Set<String> getAllMembers(String groupName);

    // 获取聊天组内的全部channel
    Set<Channel> getAllChannel(String groupName);

}
