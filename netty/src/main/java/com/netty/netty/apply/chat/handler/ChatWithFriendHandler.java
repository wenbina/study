package com.netty.netty.apply.chat.handler;

import com.netty.netty.apply.chat.entity.ChatRequestMessage;
import com.netty.netty.apply.chat.entity.ChatResponseMessage;
import com.netty.netty.apply.chat.server.session.impl.SessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: w
 * @Date: 2021/8/19 11:10
 */
@ChannelHandler.Sharable
public class ChatWithFriendHandler extends SimpleChannelInboundHandler<ChatRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, ChatRequestMessage msg) throws Exception {
        String send = msg.getSend();
        String receive = msg.getReceive();
        String content = msg.getContent();

        // 通过用户名找到channel
        Channel channel = SessionFactory.getSession().getChannel(receive);
        ChatResponseMessage chatResponseMessage = new ChatResponseMessage();
        // 判断是否在线
        if (channel != null) {
            chatResponseMessage.setIsSuccess(true);
            chatResponseMessage.setSend(send);
            chatResponseMessage.setContent(content);
            channel.writeAndFlush(chatResponseMessage);
        } else {
            // 回送对方不在
            chatResponseMessage.setIsSuccess(false);
            chatResponseMessage.setContent("好友已下线，以为您进行留言");
            ctx.writeAndFlush(chatResponseMessage);
        }
    }
}
