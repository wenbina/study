package com.netty.netty.apply.chat.server.session.impl;

import com.netty.netty.apply.chat.server.session.GroupSession;

/**
 * @Author: w
 * @Date: 2021/8/20 10:12
 */
public abstract class GroupSessionFactoryImpl {

    private static GroupSessionImpl groupSession = new GroupSessionImpl();

    public static GroupSession getGroupSession() {
        return groupSession;
    }
}
