package com.netty.netty.apply.heartbeat;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * @Author: w
 * @Date: 2021/8/21 8:48
 */
public class Client {

    public static void main(String[] args) {
        // 创建事件循环组
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel channel) throws Exception {
                    channel.pipeline().addLast(new ObjectEncoder());
                    channel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                    // 添加处理器
                    channel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                        // 连接到服务端触发
                        @Override
                        public void channelActive(ChannelHandlerContext ctx) throws Exception {
                            //super.channelActive(ctx);
                        }
                        // 读取事件
                        @Override
                        public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                            System.out.println(msg);
                        }
                    });
                }
            });
            Channel channel = bootstrap.connect("localhost", 9999).sync().channel();
            channel.closeFuture().sync();
        } catch (Exception e) {
            System.out.println("客户端发生异常");
        } finally {
            group.shutdownGracefully();
        }




    }
}
