package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/20 10:18
 */
@Data
public class CreateGroupResponseMessage extends Message {

    private String groupName;
}
