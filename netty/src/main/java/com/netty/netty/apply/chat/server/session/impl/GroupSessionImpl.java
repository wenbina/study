package com.netty.netty.apply.chat.server.session.impl;

import com.netty.netty.apply.chat.server.session.GroupSession;
import io.netty.channel.Channel;

import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @Author: w
 * @Date: 2021/8/19 17:21
 */
public class GroupSessionImpl implements GroupSession {

    private final Map<String, Group> groupMap = new ConcurrentHashMap<>();

    @Override
    public Group createGroup(String groupName, String groupLeader, Set<String> member) {
        Group group = new Group();
        group.setName(groupName);
        group.setMembers(member);
        group.setGroupLeader(groupLeader);
        return groupMap.putIfAbsent(groupName,group);
    }

    @Override
    public Group joinGroup(String groupName, String memberName) {
        return groupMap.computeIfPresent(groupName, (key, value) -> {
            value.getMembers().add(memberName);
            return value;
        });
    }

    @Override
    public Group removeMember(String groupName, String memberName) {
        return groupMap.computeIfPresent(groupName, (key, value) -> {
            value.getMembers().remove(memberName);
            return value;
        });
    }

    @Override
    public Set<String> getAllMembers(String groupName) {
        return groupMap.get(groupName).getMembers();
    }

    @Override
    public Set<Channel> getAllChannel(String groupName) {
        Set<String> allMembers = this.getAllMembers(groupName);
        return allMembers.stream().map(member -> SessionFactory.getSession().getChannel(member)).filter(Objects::nonNull).collect(Collectors.toSet());
    }
}
