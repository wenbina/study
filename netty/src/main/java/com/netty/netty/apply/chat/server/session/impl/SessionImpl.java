package com.netty.netty.apply.chat.server.session.impl;

import com.netty.netty.apply.chat.server.session.Session;


import io.netty.channel.Channel;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: w
 * @Date: 2021/8/18 17:05
 */
public class SessionImpl implements Session {

    private final Map<String,Channel> usernameChannelMap = new ConcurrentHashMap<>();

    private final Map<Channel,String> channelUsernameMap = new ConcurrentHashMap<>();


    @Override
    public void bind(Channel channel, String username) {
        usernameChannelMap.put(username,channel);
        channelUsernameMap.put(channel,username);
    }

    @Override
    public void unbind(Channel channel) {
        channelUsernameMap.remove(channel);
    }

    @Override
    public Channel getChannel(String username) {
        return usernameChannelMap.get(username);
    }

    @Override
    public Set<String> getOnlineUsers() {
        return usernameChannelMap.keySet();
    }

    @Override
    public String getUsername(Channel channel) {
        return channelUsernameMap.get(channel);
    }
}
