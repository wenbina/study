package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/20 16:58
 */
@Data
public class JoinGroupResponseMessage extends Message {

    private String groupName;
}
