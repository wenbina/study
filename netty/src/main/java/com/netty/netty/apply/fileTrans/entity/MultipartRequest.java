package com.netty.netty.apply.fileTrans.entity;

import io.netty.handler.codec.http.multipart.FileUpload;
import lombok.Data;
import org.json.simple.JSONObject;

import java.util.Map;

/**
 * @Author: w
 * @Date: 2021/9/3 16:56
 */
@Data
public class MultipartRequest {

    private Map<String, FileUpload> fileUploads;

    private JSONObject params;

}
