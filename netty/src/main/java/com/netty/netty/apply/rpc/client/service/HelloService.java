package com.netty.netty.apply.rpc.client.service;

/**
 * @Author: w
 * @Date: 2021/8/19 23:04
 */
public interface HelloService {

    String hello(String name);
}
