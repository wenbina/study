package com.netty.netty.apply.rpc2.api.entity;

import com.netty.netty.apply.rpc.entity.Message;
import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/19 22:51
 */
@Data
public class RpcResponseMessage extends Message {

    /**
     * 返回值
     */
    private Object returnValue;

    /**
     * 异常值
     */
    private Exception exceptionValue;
}
