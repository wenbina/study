package com.netty.netty.apply.rpc.client.service.impl;

import com.netty.netty.apply.rpc.client.service.HelloService;

/**
 * @Author: w
 * @Date: 2021/8/19 23:05
 */
public class HelloServiceImpl implements HelloService {

    @Override
    public String hello(String name) {
        return "【" + name + "】：你好";
    }
}
