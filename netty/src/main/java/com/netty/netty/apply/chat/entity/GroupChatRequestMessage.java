package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/20 10:37
 */
@Data
public class GroupChatRequestMessage extends Message {

    // 群聊名称
    private String groupName;
}
