package com.netty.netty.apply.chat.handler;

import com.netty.netty.apply.chat.entity.CreateGroupRequestMessage;
import com.netty.netty.apply.chat.entity.CreateGroupResponseMessage;
import com.netty.netty.apply.chat.server.session.impl.Group;
import com.netty.netty.apply.chat.server.session.impl.GroupSessionFactoryImpl;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/20 8:39
 * 创建群聊处理器
 */
@ChannelHandler.Sharable
public class CreateGroupHandler extends SimpleChannelInboundHandler<CreateGroupRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, CreateGroupRequestMessage createGroupRequestMessage) throws Exception {
        String groupName = createGroupRequestMessage.getGroupName();
        String members = createGroupRequestMessage.getMembers();
        String groupLeader = createGroupRequestMessage.getGroupLeader();
        String[] split = members.split("，");
        Set<String> newMembers = new HashSet<>(Arrays.asList(split));
        // 把群主自己加入
        newMembers.add(groupLeader);
        Group group = GroupSessionFactoryImpl.getGroupSession().createGroup(groupName, groupLeader,newMembers);
        CreateGroupResponseMessage createGroupResponseMessage = new CreateGroupResponseMessage();
        if (group == null) {
            createGroupResponseMessage.setContent(groupName + " 创建成功");
            createGroupResponseMessage.setIsSuccess(true);
        } else {
            createGroupResponseMessage.setContent(groupName + "已存在，创建失败");
            createGroupResponseMessage.setIsSuccess(false);
        }
        ctx.writeAndFlush(createGroupResponseMessage);
    }
}
