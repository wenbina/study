package com.netty.netty.apply.rpc2.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/24 9:20
 */
@Data
public abstract class Message implements Serializable {

    // 是否成功
    public Boolean isSuccess;

    // 消息内容
    private String content;

    // 发送者
    private String send;

    // 接收者
    private String receive;
}
