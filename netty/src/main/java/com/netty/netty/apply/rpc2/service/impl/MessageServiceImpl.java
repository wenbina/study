package com.netty.netty.apply.rpc2.service.impl;

import com.netty.netty.apply.rpc2.service.MessageService;

/**
 * @Author: w
 * @Date: 2021/9/16 18:00
 */
public class MessageServiceImpl implements MessageService {

    @Override
    public void receiveMessage(String message) {
        System.out.println("客户端发送过来的数据为：" + message);
    }
}
