package com.netty.netty.apply.chat.entity;

import lombok.Data;


/**
 * @Author: w
 * @Date: 2021/8/18 14:23
 * 登陆请求
 */
@Data
public class LoginRequestMessage extends Message {

    private String username;

    private String password;
}
