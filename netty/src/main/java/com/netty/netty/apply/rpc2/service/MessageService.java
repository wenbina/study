package com.netty.netty.apply.rpc2.service;

/**
 * @Author: w
 * @Date: 2021/9/16 18:00
 */
public interface MessageService {

    void receiveMessage(String message);

}
