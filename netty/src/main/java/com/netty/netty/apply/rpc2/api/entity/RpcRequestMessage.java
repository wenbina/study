package com.netty.netty.apply.rpc2.api.entity;

import com.netty.netty.apply.rpc.entity.Message;
import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/19 22:48
 */
@Data
public class RpcRequestMessage extends Message {

    /**
     * 调用的接口的全限定名，服务端根据他找到实现
     */
    private String interfaceName;

    /**
     * 调用接口中的方法名
     */
    private String methodName;

    /**
     * 方法返回类型
     */
    private Class<?> returnType;

    /**
     * 方法参数类型数组
     */
    private Class[] parameterTypes;

    /**
     * 方法参数值数组
     */
    private Object[] parameterValue;
}
