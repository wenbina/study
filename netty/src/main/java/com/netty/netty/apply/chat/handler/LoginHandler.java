package com.netty.netty.apply.chat.handler;

import com.netty.netty.apply.chat.entity.LoginRequestMessage;
import com.netty.netty.apply.chat.entity.LoginResponseMessage;
import com.netty.netty.apply.chat.enums.MessageEnum;
import com.netty.netty.apply.chat.server.service.impl.UserServiceFactory;
import com.netty.netty.apply.chat.server.session.impl.SessionFactory;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: w
 * @Date: 2021/8/18 14:36
 */
@ChannelHandler.Sharable
public class LoginHandler extends SimpleChannelInboundHandler<LoginRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, LoginRequestMessage msg) throws Exception {
        String username = msg.getUsername();
        String password = msg.getPassword();
        // 获取userService
        boolean login = UserServiceFactory.getUserService().login(username, password);
        LoginResponseMessage responseMessage = new LoginResponseMessage();
        responseMessage.setMegType(MessageEnum.LOGIN_MESSAGE.type);
        if (login) {
            // 登陆成功
            responseMessage.setIsSuccess(true);
            responseMessage.setContent("登陆成功");
            // 登陆成功之后需要将连接信息放入session中
            SessionFactory.getSession().bind(ctx.channel(),username);
        } else {
            // 登陆失败
            responseMessage.setIsSuccess(false);
            responseMessage.setContent("账号或密码有误");
        }
        System.out.println("用户【"+username+"】已上线");
        // 将消息写出
        ctx.writeAndFlush(responseMessage);
    }
}
