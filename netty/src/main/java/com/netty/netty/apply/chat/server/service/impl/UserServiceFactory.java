package com.netty.netty.apply.chat.server.service.impl;

import com.netty.netty.apply.chat.server.service.UserService;

/**
 * @Author: w
 * @Date: 2021/8/18 14:47
 */
public abstract class UserServiceFactory {

    private static UserService userService = new UserServiceImpl();

    public static UserService getUserService() {
        return userService;
    }
}
