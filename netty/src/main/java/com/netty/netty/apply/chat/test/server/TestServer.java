package com.netty.netty.apply.chat.test.server;

import com.netty.netty.apply.chat.server.ChatServer;

/**
 * @Author: w
 * @Date: 2021/8/19 8:40
 * 服务端测试
 */
public class TestServer {

    public static void main(String[] args) {
        ChatServer.startTemplate();
    }
}
