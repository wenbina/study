package com.netty.netty.apply.chat.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/17 12:30
 */
@Data
public abstract class Message implements Serializable {

    // 是否成功
    public Boolean isSuccess;

    // 消息类型
    public String megType;

    // 消息内容
    private String content;

    // 发送者
    private String send;

    // 接收者
    private String receive;

}
