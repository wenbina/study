package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/19 11:09
 */
@Data
public class ChatResponseMessage extends Message {
}
