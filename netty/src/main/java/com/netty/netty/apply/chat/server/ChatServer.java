package com.netty.netty.apply.chat.server;

import com.netty.netty.apply.chat.handler.*;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;


/**
 * @Author: w
 * @Date: 2021/8/18 14:28
 */
public class ChatServer {

    // 登陆处理器
    private static LoginHandler LOGIN_HANDLER = new LoginHandler();
    // 获取好友列表
    private static GetFriendsHandler FRIENDS_HANDLER = new GetFriendsHandler();
    // 聊天
    private static ChatWithFriendHandler CHAT_HANDLER = new ChatWithFriendHandler();
    // 创建群聊
    private static CreateGroupHandler CREATE_GROUP_HANDLER = new CreateGroupHandler();
    // 发送群聊信息
    private static GroupChatHandler GROUP_CHAT_HANDLER = new GroupChatHandler();
    // 退出群聊
    private static RemoveGroupHandler REMOVE_GROUP_HANDLER = new RemoveGroupHandler();
    // 加入群聊
    private static JoinGroupHandler JOIN_GROUP_HANDLER = new JoinGroupHandler();

    public static void main(String[] args) {
        startTemplate();
    }

    /**
     * 服务端启动模板
     */
    public static void startTemplate() {
        // 创建事件循环组
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        try {
            // 创建启动器
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            // 设置事件循环组
            serverBootstrap.group(boss,worker);
            // 设置channel
            serverBootstrap.channel(NioServerSocketChannel.class);
            // 设置处理器
            serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel ch) throws Exception {
                    // 添加编码器以及解码器
                    ch.pipeline().addLast(new ObjectEncoder());
                    ch.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                    // 登陆处理器
                    ch.pipeline().addLast(LOGIN_HANDLER);
                    // 获取好友列表
                    ch.pipeline().addLast(FRIENDS_HANDLER);
                    // 聊天
                    ch.pipeline().addLast(CHAT_HANDLER);
                    // 创建群聊
                    ch.pipeline().addLast(CREATE_GROUP_HANDLER);
                    // 发送群聊信息
                    ch.pipeline().addLast(GROUP_CHAT_HANDLER);
                    // 退出群聊
                    ch.pipeline().addLast(REMOVE_GROUP_HANDLER);
                    // 加入群聊
                    ch.pipeline().addLast(JOIN_GROUP_HANDLER);
                }
            });
            // 绑定端口
            Channel channel = serverBootstrap.bind(9999).channel();
            // 关闭
            channel.closeFuture().sync();
        } catch (Exception e) {
            System.out.println("服务端出现异常...");
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }
    }
}
