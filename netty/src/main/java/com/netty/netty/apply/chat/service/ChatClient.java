package com.netty.netty.apply.chat.service;

import com.netty.netty.apply.chat.entity.LoginRequestMessage;
import com.netty.netty.apply.chat.entity.Message;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;


import java.net.InetSocketAddress;

/**
 * @Author: w
 * @Date: 2021/8/17 9:04
 */
public class ChatClient {

    public static void main(String[] args) throws InterruptedException {
        // 创建启动器
        Bootstrap bootstrap = new Bootstrap();
        // 设置事件循环组
        bootstrap.group(new NioEventLoopGroup());
        // 设置channel
        bootstrap.channel(NioSocketChannel.class);
        // 设置处理器
        bootstrap.handler(new ChannelInitializer<SocketChannel>() {
            @Override
            protected void initChannel(SocketChannel channel) throws Exception {
                // 添加对象编码解码器
                channel.pipeline().addLast(new ObjectEncoder());
                channel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                // 添加读取信息处理器
                channel.pipeline().addLast(new ChannelInboundHandlerAdapter() {
                    // 连接成功触发active事件
                    @Override
                    public void channelActive(ChannelHandlerContext ctx) throws Exception {
                        Message message = new LoginRequestMessage();
                        message.setContent("你好你好");
                        ctx.writeAndFlush(message);
                    }
                });
            }
        });
        Channel channel = bootstrap.connect(new InetSocketAddress("localhost", 9999)).sync().channel();
        channel.closeFuture().sync();
    }
}
