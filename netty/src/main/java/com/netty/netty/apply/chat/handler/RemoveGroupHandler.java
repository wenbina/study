package com.netty.netty.apply.chat.handler;

import com.netty.netty.apply.chat.entity.RemoveGroupRequestMessage;
import com.netty.netty.apply.chat.entity.RemoveGroupResponseMessage;
import com.netty.netty.apply.chat.server.session.impl.Group;
import com.netty.netty.apply.chat.server.session.impl.GroupSessionFactoryImpl;
import com.netty.netty.apply.chat.server.session.impl.SessionFactory;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: w
 * @Date: 2021/8/20 15:09
 */
@ChannelHandler.Sharable
public class RemoveGroupHandler extends SimpleChannelInboundHandler<RemoveGroupRequestMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, RemoveGroupRequestMessage removeGroupRequestMessage) throws Exception {
        String groupName = removeGroupRequestMessage.getGroupName();
        String memberName = removeGroupRequestMessage.getMemberName();

        Group group = GroupSessionFactoryImpl.getGroupSession().removeMember(groupName, memberName);
        RemoveGroupResponseMessage removeGroupResponseMessage = new RemoveGroupResponseMessage();

        // 通知群主
        String groupLeader = group.getGroupLeader();
        Channel channel = SessionFactory.getSession().getChannel(groupLeader);

        if (group != null) {
            // 移除成功
            removeGroupResponseMessage.setIsSuccess(true);
            removeGroupResponseMessage.setReceive(memberName);
            removeGroupResponseMessage.setContent("【系统】：" + memberName + "已退出群聊");
            removeGroupResponseMessage.setGroupName(groupName);
            channel.writeAndFlush(removeGroupResponseMessage);
        } else {
            // 移除失败
            removeGroupResponseMessage.setIsSuccess(false);
        }
        ctx.writeAndFlush(removeGroupResponseMessage);
    }
}
