package com.netty.netty.apply.chat.server.session;

import io.netty.channel.Channel;

import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/18 17:04
 */
public interface Session {

    /**
     * 绑定会话：channel与用户绑定
     */
    void bind(Channel channel, String username);

    /**
     * 解绑会话
     */
    void unbind(Channel channel);

    /**
     * 根据用户名获取channel
     */
    Channel getChannel(String username);

    /**
     * 获取在线用户列表
     */
    Set<String> getOnlineUsers();

    /**
     * 根据channel获取用户名
     */
    String getUsername(Channel channel);
}
