package com.netty.netty.apply.chat.enums;

/**
 * @Author: w
 * @Date: 2021/8/19 18:19
 */
public enum OptionEnum {

    GET_FRIEND_LIST("1","获取好友列表操作"),
    CHAT_WITH_FRIEND("2","与好友聊天操作");

    public String option;

    public String desc;

    OptionEnum(String option, String desc) {
        this.option = option;
        this.desc = desc;
    }
}
