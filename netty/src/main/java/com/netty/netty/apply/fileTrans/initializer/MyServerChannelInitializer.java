package com.netty.netty.apply.fileTrans.initializer;

import com.netty.netty.apply.fileTrans.handler.ServerFileHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;

/**
 * @Author: w
 * @Date: 2021/9/3 16:59
 */
public class MyServerChannelInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline pipeline = channel.pipeline();

        // 设置编解码器
        pipeline.addLast("encoder", new HttpResponseEncoder());
        pipeline.addLast("decoder", new HttpRequestDecoder());

        // 合并请求
        pipeline.addLast("aggregator", new HttpObjectAggregator(655300000));

        // 设置文件处理器
        pipeline.addLast(new ServerFileHandler());
    }
}
