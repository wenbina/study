package com.netty.netty.apply.rpc.server;

import com.netty.netty.apply.rpc.handler.RpcRequestHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

/**
 * @Author: w
 * @Date: 2021/8/19 22:46
 */
public class RpcServer {

    public static void main(String[] args) {
        NioEventLoopGroup boss = new NioEventLoopGroup();
        NioEventLoopGroup worker = new NioEventLoopGroup();
        RpcRequestHandler rpcRequestHandler = new RpcRequestHandler();

        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();
            serverBootstrap.group(boss,worker);
            serverBootstrap.channel(NioServerSocketChannel.class);
            serverBootstrap.childHandler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel channel) throws Exception {
                    channel.pipeline().addLast(new ObjectEncoder());
                    channel.pipeline().addLast(new ObjectDecoder(Integer.MAX_VALUE, ClassResolvers.cacheDisabled(null)));
                    channel.pipeline().addLast(rpcRequestHandler);
                }
            });
            Channel channel = serverBootstrap.bind(9999).sync().channel();
            channel.closeFuture().sync();
        } catch (Exception e) {
            System.out.println("服务端发生异常");
        } finally {
            boss.shutdownGracefully();
            worker.shutdownGracefully();
        }

    }
}
