package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/19 18:27
 * 创建群聊请求
 */
@Data
public class CreateGroupRequestMessage extends Message {

    // 群名称
    private String groupName;

    // 群聊成员
    private String members;

    // 群主
    private String groupLeader;
}
