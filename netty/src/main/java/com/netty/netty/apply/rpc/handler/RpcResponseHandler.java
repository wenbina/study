package com.netty.netty.apply.rpc.handler;

import com.netty.netty.apply.rpc.entity.RpcResponseMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * @Author: w
 * @Date: 2021/8/19 23:02
 */
public class RpcResponseHandler extends SimpleChannelInboundHandler<RpcResponseMessage> {

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, RpcResponseMessage rpcResponseMessage) throws Exception {
        System.out.println(rpcResponseMessage);
    }
}
