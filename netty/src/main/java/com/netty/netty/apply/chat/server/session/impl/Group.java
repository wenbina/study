package com.netty.netty.apply.chat.server.session.impl;

import lombok.Data;

import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/19 17:24
 */
@Data
public class Group {

    // 聊天室名称
    private String name;

    // 聊天室成员
    private Set<String> members;

    // 群主
    private String groupLeader;
}
