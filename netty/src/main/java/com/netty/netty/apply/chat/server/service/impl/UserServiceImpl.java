package com.netty.netty.apply.chat.server.service.impl;

import com.netty.netty.apply.chat.server.service.UserService;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: w
 * @Date: 2021/8/18 14:45
 */
public class UserServiceImpl implements UserService {

    private Map<String,String> allUserMap = new ConcurrentHashMap<>();

    {
        allUserMap.put("zs","123");
        allUserMap.put("ls","123");
        allUserMap.put("ww","123");
        allUserMap.put("zl","123");
        allUserMap.put("tq","123");
    }

    /**
     * 登陆
     */
    @Override
    public boolean login(String username, String password) {
        String pass = allUserMap.get(username);
        return password.equals(pass);
    }
}
