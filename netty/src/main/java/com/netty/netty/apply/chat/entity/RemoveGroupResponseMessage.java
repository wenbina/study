package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/20 15:17
 */
@Data
public class RemoveGroupResponseMessage extends Message {

    private String groupName;
}
