package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/20 16:56
 */
@Data
public class JoinGroupRequestMessage extends Message {

    private String groupName;

    private String memberName;
}
