package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/18 18:26
 */
@Data
public class GetFriendsMessage extends Message{

    // 好友列表
    private String friends;
}
