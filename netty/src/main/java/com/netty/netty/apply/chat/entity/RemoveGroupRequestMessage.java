package com.netty.netty.apply.chat.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/20 15:10
 */
@Data
public class RemoveGroupRequestMessage extends Message {

    // 群聊名
    private String groupName;

    // 退群成员名
    private String memberName;
}
