package com.netty.netty.eventLoop;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.DefaultEventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.Charset;
import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/8/15 15:14
 */
@Slf4j
public class EventLoopServer {

    public static void main(String[] args) {
        handleIOEvent();
    }

    /**
     * EventLoop处理普通任务和定时任务
     */
    private static void handleTask() {
        // 1：创建事件循环组
        // 可以处理io事件、普通任务、定时任务
        NioEventLoopGroup group = new NioEventLoopGroup(2);

        // 2：用于处理普通任务、定时任务
        //DefaultEventLoopGroup group = new DefaultEventLoopGroup();

        // 获取下一个事件循环对象：分配两个循环事件组的话，这两个循环事件组会交替执行
        log.debug("{}", group.next());
        log.debug("{}", group.next());

        // 执行普通任务
        group.next().submit(() -> {
            log.debug("执行普通任务");
        });

        // 执行定时任务：参数一：任务体；参数二：执行时间（0表示马上执行）；参数三：间隔时间；参数四：时间单位
        group.next().scheduleAtFixedRate(() -> {
            log.debug("执行定时任务...");
        }, 0, 1, TimeUnit.SECONDS);
    }

    /**
     * EventLoop处理IO事件
     */
    private static void handleIOEvent() {
        // 创建一个独立的EventLoopGroup专门去处理channel中耗时较长的事件（可能某个channel读事件耗时较长，那他可能会拖慢其他的channel的时长
        DefaultEventLoopGroup group = new DefaultEventLoopGroup();
        new ServerBootstrap()
                // 我们可以将事件循环组再次进行划分：分为boss和worker
                // boss只负责accept事件；worker只负责socketChannel上的读和写
                .group(new NioEventLoopGroup(), new NioEventLoopGroup())
                .channel(NioServerSocketChannel.class)
                .childHandler(new ChannelInitializer<NioSocketChannel>() {
                    @Override
                    protected void initChannel(NioSocketChannel channel) throws Exception {
                        channel.pipeline()
                                .addLast("handler1", new ChannelInboundHandlerAdapter() {
                                    @Override
                                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                        ByteBuf buf = (ByteBuf) msg;
                                        log.debug("============{}", buf.toString(Charset.defaultCharset()));
                                        // 将消息传给下一个handler
                                        ctx.fireChannelRead(msg);
                                    }
                                })
                                .addLast(group, "handler2", new ChannelInboundHandlerAdapter() {
                                    @Override
                                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                                        ByteBuf buf = (ByteBuf) msg;
                                        log.debug("============{}", buf.toString(Charset.defaultCharset()));
                                    }
                                });
                    }
                })
                .bind(9999);
    }
}
