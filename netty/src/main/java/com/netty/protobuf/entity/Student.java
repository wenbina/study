package com.netty.protobuf.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: w
 * @Date: 2021/8/21 22:37
 */
@Data
public class Student implements Serializable {

    private String name;

    private Integer age;

}
