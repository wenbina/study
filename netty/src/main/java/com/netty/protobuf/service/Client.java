package com.netty.protobuf.service;

import com.netty.protobuf.codec.StudentPOJO;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.protobuf.ProtobufDecoder;
import io.netty.handler.codec.protobuf.ProtobufEncoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;

/**
 * @Author: w
 * @Date: 2021/8/21 22:38
 */
public class Client {

    public static void main(String[] args) {
        NioEventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap bootstrap = new Bootstrap();
            bootstrap.group(group);
            bootstrap.channel(NioSocketChannel.class);
            bootstrap.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                protected void initChannel(SocketChannel channel) throws Exception {
                    /*channel.pipeline().addLast(new StringEncoder());
                    channel.pipeline().addLast(new StringDecoder());*/
                    channel.pipeline().addLast(new ProtobufEncoder());
                    channel.pipeline().addLast(new ProtobufDecoder(StudentPOJO.Student.getDefaultInstance()));
                    channel.pipeline().addLast(new ChannelInboundHandlerAdapter(){
                        // 连接时触发
                        @Override
                        public void channelActive(ChannelHandlerContext ctx) throws Exception {
                            // 发送一个student对象到服务器端
                            StudentPOJO.Student student = StudentPOJO.Student.newBuilder().setName("张三").setAge(18).build();
                            ctx.writeAndFlush(student);
                        }
                    });
                }
            });
            Channel channel = bootstrap.connect("localhost", 9999).sync().channel();
            channel.closeFuture().sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            group.shutdownGracefully();
        }
    }
}
