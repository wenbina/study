package com.netty.protostuff.entity;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/8/23 15:22
 */
@Data
public class Student {

    private String name;

    private Integer age;

}
