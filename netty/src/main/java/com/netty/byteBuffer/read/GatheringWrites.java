package com.netty.byteBuffer.read;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;

/**
 * @Author: w
 * @Date: 2021/8/12 23:16
 * 集中写入
 */
public class GatheringWrites {

    public static void main(String[] args) {
        ByteBuffer buffer1 = StandardCharsets.UTF_8.encode("hello");
        ByteBuffer buffer2 = StandardCharsets.UTF_8.encode("word");
        ByteBuffer buffer3 = StandardCharsets.UTF_8.encode("你好");

        try {
            FileChannel channel = new RandomAccessFile("words2.txt", "rw").getChannel();
            channel.write(new ByteBuffer[]{buffer1,buffer2,buffer3});
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
