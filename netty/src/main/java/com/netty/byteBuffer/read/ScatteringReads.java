package com.netty.byteBuffer.read;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author: w
 * @Date: 2021/8/12 23:12
 */
public class ScatteringReads {

    public static void main(String[] args) {
        try (FileChannel channel = new RandomAccessFile("words.txt", "r").getChannel()) {
            // 分散读取：将数据读取到多个byteBuffer
            ByteBuffer buffer1 = ByteBuffer.allocate(3);
            ByteBuffer buffer2 = ByteBuffer.allocate(3);
            ByteBuffer buffer3 = ByteBuffer.allocate(5);
            channel.read(new ByteBuffer[]{buffer1,buffer2,buffer3});
            buffer1.flip();
            buffer2.flip();
            buffer3.flip();
            System.out.println((char)buffer1.get());
            System.out.println((char)buffer2.get());
            System.out.println((char)buffer3.get());
        } catch (IOException ioException) {
        }
    }
}
