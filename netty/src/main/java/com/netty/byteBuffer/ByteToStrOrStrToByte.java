package com.netty.byteBuffer;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

/**
 * @Author: w
 * @Date: 2021/8/12 22:58
 * 字符串与byteBuffer的互转
 */
public class ByteToStrOrStrToByte {

    public static void main(String[] args) {
        //strToByte();
        byteToStr();

    }

    /**
     * byte转String
     */
    private static void byteToStr() {
        // 通过chatset：
        ByteBuffer buffer1 = StandardCharsets.UTF_8.encode("hello");
        String s = StandardCharsets.UTF_8.decode(buffer1).toString();
        System.out.println(s);
    }

    /**
     * String转为byte
     */
    private static void strToByte() {
        ByteBuffer buffer = ByteBuffer.allocate(10);
        // 1：直接将String转为byte
        buffer.put("hello".getBytes());
        // 切换为读模式
        buffer.flip();
        System.out.println((char)buffer.get());

        // 2：通过chatset：可以不用切换为读模式，会自动切换
        ByteBuffer buffer1 = StandardCharsets.UTF_8.encode("hello");
        System.out.println((char)buffer1.get());

        // 3：通过wrap：可以不用切换为读模式，会自动切换
        ByteBuffer buffer2 = ByteBuffer.wrap("hello".getBytes());
        System.out.println((char)buffer2.get());
    }
}
