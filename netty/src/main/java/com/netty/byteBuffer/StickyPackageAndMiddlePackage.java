package com.netty.byteBuffer;

import java.nio.ByteBuffer;

/**
 * @Author: w
 * @Date: 2021/8/14 8:49
 * 黏包和半包
 * 黏包：网络编程中发送消息可能多条消息一起发送，通过/n分割，这就会造成黏包
 * 半包：由于消息长度有限，有些完整信息会被拆分，这就叫做半包
 */
public class StickyPackageAndMiddlePackage {

    public static void main(String[] args) {
        ByteBuffer buffer = ByteBuffer.allocate(32);
        stickyAndMiddlePackage(buffer);
    }

    // 黏包与半包
    private static void stickyAndMiddlePackage(ByteBuffer buffer) {
        buffer.put("hello world\nI am zhangsan\nho".getBytes());
        splitPackage(buffer);
        buffer.put("w are you\n".getBytes());
        splitPackage(buffer);
    }

    // 切割
    private static ByteBuffer splitPackage(ByteBuffer source) {
        // 将写模式切换为读模式
        source.flip();
        ByteBuffer target = ByteBuffer.allocate(30);
        for (int i = 0; i < source.limit(); i++) {
            if (source.get(i) == '\n') {
                int len = i + 1 - source.position();
                // 将读取到的数据存入到新的byteBuffer
                target = ByteBuffer.allocate(len);
                for (int j = 0; j < len; j++) {
                    // get会将下标往后移
                    target.put(source.get());
                }

            }
        }
        // 保留上一次未读完的数据
        source.compact();
        return target;
    }


}
