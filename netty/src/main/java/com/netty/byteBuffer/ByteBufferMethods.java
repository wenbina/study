package com.netty.byteBuffer;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;

/**
 * @Author: w
 * @Date: 2021/6/8 22:59
 */
public class ByteBufferMethods {

    public static void main(String[] args) {
        //byteBufferAllocate();
        /*ByteBuffer buffer = ByteBuffer.allocate(10);
        write(buffer);
        // 注意：读取之前必须将写模式切换为读模式，因为下标已经移到写模式的下边：比如写入了两个数据，下标就是2，读取就是从2开始的
        buffer.flip();
        read(buffer);
        // compcat方法会将未读取的数据往前移
        buffer.compact();
        read(buffer);*/

        //rewindMethod();
        markAndResetMethod();
    }

    /**
     * class java.nio.HeapByteBuffer   -java堆内存：读写效率较低，受到GC的影响
     * class java.nio.DirectByteBuffer  --直接内存，读写效率高（少一次拷贝），不受GC影响
     */
    private static void byteBufferAllocate() {
        System.out.println(ByteBuffer.allocate(16).getClass());
        System.out.println(ByteBuffer.allocateDirect(16).getClass());
    }

    /**
     * 向buffer中写入数据  1：采用channel的read方法   2：采用buffer自己的put方法
     */
    private static void write(ByteBuffer buffer) {
        // 往buffer中放入一个字节
        buffer.put((byte) 0x61);
        // 往buffer中放入一个字节数组
        buffer.put("哈哈".getBytes());
    }

    /**
     * 读取buffer中的数据
     * get方法会让position读指针向后走，如果想重复读取数据：可以调用rewind方法将position重新置为0；或者调用get(index)方法获取指定索引位置的值
     * 注意get(index)是不会改变position的值
     */
    private static void read(ByteBuffer buffer) {
        System.out.println(buffer.get());
    }

    /**
     * rewind方法
     */
    private static void rewindMethod() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(10);
        byteBuffer.put(new byte[] {'a','b','c','d'});
        // 切换为读模式
        byteBuffer.flip();
        // 从头开始读
        System.out.println(byteBuffer.get(new byte[4]));
        // 重新读
        byteBuffer.rewind();
        System.out.println((char)byteBuffer.get());
    }

    /**
     * mark：标记，记录position位置
     * reset：将position重置到mark位置
     */
    private static void markAndResetMethod() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(10);
        byteBuffer.put(new byte[] {'a','b','c','d'});
        // 切换为读模式
        byteBuffer.flip();
        // 读取前两个：然后做标记
        System.out.println((char)byteBuffer.get());
        System.out.println((char)byteBuffer.get());
        byteBuffer.mark();
        System.out.println((char)byteBuffer.get());
        System.out.println((char)byteBuffer.get());
        // 重置到标记处进行重新读：到索引2
        byteBuffer.reset();
        System.out.println((char)byteBuffer.get());
        System.out.println((char)byteBuffer.get());
    }
}
