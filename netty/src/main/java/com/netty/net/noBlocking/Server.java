package com.netty.net.noBlocking;

import com.netty.utils.ByteBufferUtil;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: w
 * @Date: 2021/8/14 22:53
 * 非阻塞模式：通过设置服务器是否阻塞改变模式；主要影响阻塞的连接与读取这两个地方
 */
public class Server {

    public static void main(String[] args) {
        try {
            ByteBuffer buffer = ByteBuffer.allocate(16);
            // 创建服务器
            ServerSocketChannel ssc = ServerSocketChannel.open();
            // 绑定端口
            ssc.bind(new InetSocketAddress(9999));
            // 设置为非阻塞模式
            ssc.configureBlocking(false);

            // 定义客户端连接集合管理客户端连接
            List<SocketChannel> socketChannels = new ArrayList<>();
            while (true) {
                // 等待客户端连接：设置为非阻塞模式后，如果没有客户端连接会返回null
                SocketChannel socketChannel = ssc.accept();
                if (socketChannel != null) {
                    System.out.println("connected...");
                    // 将连接放入集合中
                    socketChannels.add(socketChannel);
                    // 将socketChannel设置为非阻塞：影响read
                    socketChannel.configureBlocking(false);

                    // 遍历集合中的客户端连接：读取客户端发送信息
                    for (SocketChannel channel : socketChannels) {
                        int read = channel.read(buffer); // 客户端没有发送信息会返回0
                        if (read > 0) {
                            // 切换为读模式
                            buffer.flip();
                            // 打印
                            ByteBufferUtil.debugAll(buffer);
                            // 切换为写模式
                            buffer.clear();
                            System.out.println("after read...");
                        }
                    }
                }

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
