package com.netty.net.seloctor;

import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

/**
 * @Author: w
 * @Date: 2021/8/14 23:01
 */
public class Client {

    public static void main(String[] args) {
        try {
            SocketChannel socketChannel = SocketChannel.open();
            socketChannel.connect(new InetSocketAddress("localhost",9999));
            //socketChannel.write(Charset.defaultCharset().encode("hello"));
            System.out.println("hello...");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
