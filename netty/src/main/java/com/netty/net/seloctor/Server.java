package com.netty.net.seloctor;

import com.netty.utils.ByteBufferUtil;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

/**
 * @Author: w
 * @Date: 2021/8/15 9:41
 * 多路复用
 */
public class Server {

    public static void main(String[] args) {
        try {
            // 创建Selector用于管理多个Channel
            Selector selector = Selector.open();
            // 建立ServerSocketChannel连接
            ServerSocketChannel ssc = ServerSocketChannel.open();
            // 设置为非阻塞模式
            ssc.configureBlocking(false);

            // 将socket连接注册到Selector中：SelectionKey就是将来事件发生之后，通过它可以知道事件和哪个Channel的事件
            SelectionKey selectionKey = ssc.register(selector, 0, null);
            // key只需要关注accept事件
            selectionKey.interestOps(SelectionKey.OP_ACCEPT);

            // 绑定端口
            ssc.bind(new InetSocketAddress(9999));

            while (true) {
                // select方法在没有事件发生的时候也会阻塞在此处，有事件发生的时候才会恢复运行：在有事件未处理的时候不会阻塞
                selector.select();
                // 处理事件：获取注册到Selector中的SelectionKey
                Set<SelectionKey> selectionKeys = selector.selectedKeys();

                Iterator<SelectionKey> iterator = selectionKeys.iterator();
                while (iterator.hasNext()) {
                    SelectionKey key = iterator.next();
                    // 移除已处理事件
                    iterator.remove();
                    // 判断事件是连接事件还是读取事件
                    if (key.isAcceptable()) { // accept事件
                        ServerSocketChannel channel = (ServerSocketChannel)key.channel();
                        SocketChannel sc = channel.accept();
                        // 设置为非阻塞模式
                        sc.configureBlocking(false);
                        SelectionKey scKey = sc.register(selector, 0, null);
                        // 设置感兴趣事件
                        scKey.interestOps(SelectionKey.OP_READ);
                    } else if (key.isReadable()) {  // read事件
                        System.out.println("read...");
                        // 处理客户端断开连接异常
                        try {
                            SocketChannel channel = (SocketChannel) key.channel();
                            ByteBuffer buffer = ByteBuffer.allocate(16);
                            // 如果是正常断开，read返回-1
                            int read = channel.read(buffer);
                            if (read == -1) {
                                key.cancel();
                            }else {
                                // 切换为读模式
                                buffer.flip();
                                // 打印
                                System.out.println(Charset.defaultCharset().decode(buffer));
                                //ByteBufferUtil.debugRead(buffer);
                            }
                        } catch (Exception e) {
                            // 异常断开
                            e.printStackTrace();
                            // 客户端连接断开，将key从keys中移除
                            key.cancel();
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
