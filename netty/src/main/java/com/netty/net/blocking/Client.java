package com.netty.net.blocking;

import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;

/**
 * @Author: w
 * @Date: 2021/8/14 22:28
 * 客户端
 */
public class Client {

    public static void main(String[] args) {
        try {
            // 创建Socket连接服务端
            SocketChannel socketChannel = SocketChannel.open();
            // 设置连接地址以及端口
            socketChannel.connect(new InetSocketAddress("localhost",9999));
            socketChannel.write(Charset.defaultCharset().encode("hello"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
