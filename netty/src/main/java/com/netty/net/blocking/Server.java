package com.netty.net.blocking;

import com.netty.utils.ByteBufferUtil;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: w
 * @Date: 2021/8/14 22:18
 * 服务端
 * 阻塞模式演示
 * 阻塞表现在两个地方
 * 1：ssc.accept(); 若客户端没有连接的话就会阻塞在这里
 * 2：channel.read(buffer); 若客户端没有发送消息也将阻塞在这里
 */
public class Server {

    public static void main(String[] args) {

        try {
            // 创建byteBuffer
            ByteBuffer buffer = ByteBuffer.allocate(16);

            // 创建服务器并指定端口
            ServerSocketChannel ssc = ServerSocketChannel.open();
            ssc.bind(new InetSocketAddress(9999));

            // 由于会有很多个客户端连接服务端，所以通过一个线程集合来管理连接的客户端
            List<SocketChannel> socketChannels = new ArrayList<>();
            // 通过循环来不断读取客户端发送的消息
            while (true) {
                System.out.println("connecting...");
                // 等待客户端连接：若客户端没有连接的话就会阻塞在这里
                SocketChannel socketChannel = ssc.accept();
                System.out.println("connected...");
                // 将与客户端的连接放入集合中
                socketChannels.add(socketChannel);

                // 遍历已连接的客户端进行数据读取
                for (SocketChannel channel : socketChannels) {
                    System.out.println("before read...");
                    channel.read(buffer);
                    // 切换为读模式
                    buffer.flip();
                    // 打印
                    ByteBufferUtil.debugAll(buffer);
                    // 切换为写模式
                    buffer.clear();
                    System.out.println("after read...");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
