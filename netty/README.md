一：文件编程
1：FileChannel：
（1）只能工作在阻塞模式下
（2）获取：不能打开FileChannel，必须通过FileInputStream、FileOutputStream或者RandomAccessFile来获取FileChannel，他们都有getChannel方法
    （1）通过FileInputStream获取的channel只能读
    （2）通过FileOutputStream获取的channel只能写
    （1）通过RandomAccessFile由构造器传入的参数决定
（2）读取：会从channel读取数据填充ByteBuffer，返回值表示读到了多少个字节，-1表示达到了文件的末尾
int readBytes = channel.read(buffer);
（3）写入
ByteBuffer buffer = ...;
buffer.put(...);  // 写入数据
buffer.flip(); // 切换读模式
// 通过while进行channel.write是因为write方法并不能保证一次将buffer中的内容全部写入channel中
while(buffer.hasRemaining()) {
    channel.write(buffer);
}
（4）关闭
channel必须关闭，不过调用了FileInputStream、FileOutputStream或者RandomAccessFile的close方法会间接调用channel的close方法
（5）位置
   （1）获取当前位置：long pos = channel.position();
   （2）设置当前位置时，如果设置为文件末尾：1：读取会返回-1；2：这时写入会追加内容，但是要注意如果position超过了文件末尾，再写入时在新内容和原末尾间会有空洞(00)
（6）大小
使用size方法获取文件大小
（7）强制写入
操作系统出于性能考虑，会将数据缓存，不是立刻写入磁盘（调用close方法才会写入磁盘）。可以调用force(true)方法将文件内容和元数据立即写入磁盘



