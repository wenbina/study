package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/8/2 8:49
 * 微信支付渠道
 */
public class WXPayChannel extends PayChannel {

    @Override
    public void trans() {
        // 微信支付的一系列操作
        payModel.security(1L);
    }

}
