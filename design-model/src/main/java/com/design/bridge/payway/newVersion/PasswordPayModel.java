package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/8/2 8:47
 * 密码支付
 */
public class PasswordPayModel implements IPayModel {

    @Override
    public void security(Long userId) {
        System.out.println("密码支付安全校验中...");
    }
}
