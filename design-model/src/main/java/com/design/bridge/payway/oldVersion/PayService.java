package com.design.bridge.payway.oldVersion;

/**
 * @Author: w
 * @Date: 2021/7/30 9:33
 * 支付方式：
 * 需求：
 * 现支付方式支持：支付宝、微信、银联支付；
 * 而每种支付方式又可以通过指纹、人脸和密码支付
 */
public class PayService {

    /**
     * channelType：支付通道
     * modelType：支付方式
     */
    public void pay(Integer channelType,Integer modelType) {
        switch (channelType) {
            // 支付宝
            case 1: aliPay(modelType); break;
            // 微信
            case 2: weChatPay(modelType); break;
            //
            case 3: otherPay(modelType); break;
        }
    }

    // 支付宝支付
    private void aliPay(Integer modelType) {
        switch (modelType) {
            case 1: System.out.println("【支付宝】：指纹支付..."); break;
            case 2: System.out.println("【支付宝】：人脸支付..."); break;
            case 3: System.out.println("【支付宝】：密码支付..."); break;
        }
    }

    // 微信支付
    private void weChatPay(Integer modelType) {
        switch (modelType) {
            case 1: System.out.println("【微信】：指纹支付..."); break;
            case 2: System.out.println("【微信】：人脸支付..."); break;
            case 3: System.out.println("【微信】：密码支付..."); break;
        }
    }

    // 银联支付
    private void otherPay(Integer modelType) {
        switch (modelType) {
            case 1: System.out.println("【银联】：指纹支付..."); break;
            case 2: System.out.println("【银联】：人脸支付..."); break;
            case 3: System.out.println("【银联】：密码支付..."); break;
        }
    }

}
