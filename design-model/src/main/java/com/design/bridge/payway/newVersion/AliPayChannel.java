package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/8/2 8:57
 * 支付宝支付渠道
 */
public class AliPayChannel extends PayChannel {

    @Override
    public void trans() {
        // 支付宝支付的一系列操作
        payModel.security(1L);
    }
}
