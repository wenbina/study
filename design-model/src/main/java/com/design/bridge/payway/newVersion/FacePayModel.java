package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/8/2 8:42
 * 人脸支付
 */
public class FacePayModel implements IPayModel {

    @Override
    public void security(Long userId) {
        System.out.println("人脸支付安全校验中...");
    }
}
