package com.design.bridge.payway.newVersion;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/7/30 11:30
 * 支付渠道：支付宝、微信、银联
 */
@Data
public abstract class PayChannel {

    protected String channelName;

    protected IPayModel payModel;

    public abstract void trans();

    // 采用模板模式
    public void noticeTemplate(Double amount) {
        System.out.print("【" + channelName + "】：");
        trans();
        System.out.println("您本次一共消费" + amount + "元");
    }
}
