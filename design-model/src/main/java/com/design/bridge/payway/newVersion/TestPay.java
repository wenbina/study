package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/8/2 8:48
 */
public class TestPay {

    public static void main(String[] args) {

        PayChannel wxPayChannel = new WXPayChannel();
        wxPayChannel.setChannelName("微信支付");
        wxPayChannel.setPayModel(new FacePayModel());
        wxPayChannel.noticeTemplate(500.0);

    }
}
