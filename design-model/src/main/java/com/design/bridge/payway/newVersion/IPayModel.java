package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/7/30 11:36
 */
public interface IPayModel {

    // 安全校验方法
    void security(Long userId);

}
