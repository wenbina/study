package com.design.bridge.payway.newVersion;

/**
 * @Author: w
 * @Date: 2021/8/2 8:46
 * 指纹支付
 */
public class FingerprintPayModel implements IPayModel {

    @Override
    public void security(Long userId) {
        System.out.println("指纹支付安全校验中...");
    }
}
