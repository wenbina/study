package com.design.strategy.handleMsg;

import lombok.Data;

/**
 * @Author: w
 * @Date: 2021/7/28 8:59
 * 消息上下文
 */
@Data
public class MsgContext {

    private HandleMsg handleMsg;

    public String handleMsg() {
        return handleMsg.handle();
    }
}
