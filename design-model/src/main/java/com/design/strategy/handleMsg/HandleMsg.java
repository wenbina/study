package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 8:57
 * 处理消息
 */
public interface HandleMsg {

    String handle();

}
