package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 9:11
 */
public class MsgTest {

    public static void main(String[] args) {
        MsgContext msgContext = new MsgContext();
        msgContext.setHandleMsg(new PrivateChatVideoMsg());
        String callBackMsg = msgContext.handleMsg();
        System.out.println(callBackMsg);
    }
}
