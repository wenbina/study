package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 9:08
 * 私聊视频信息
 */
public class PrivateChatVideoMsg implements HandleMsg {

    @Override
    public String handle() {
        return "您的好友xxx向你发送了视频，是否接收...";
    }
}
