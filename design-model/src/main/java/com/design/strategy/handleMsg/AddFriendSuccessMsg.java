package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 9:04
 * 添加好友成功消息
 */
public class AddFriendSuccessMsg implements HandleMsg {

    @Override
    public String handle() {
        return "xxx已经同意了您的好友申请...";
    }
}
