package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 9:02
 * 好友申请消息
 */
public class NewFriendMsg implements HandleMsg {

    @Override
    public String handle() {
        return "您有一条好友申请消息，请及时处理...";
    }
}
