package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 9:07
 * 私聊图片信息
 */
public class PrivateChatImageMsg implements HandleMsg {

    @Override
    public String handle() {
        return "您的好友xxx向你发送了一张图片";
    }
}
