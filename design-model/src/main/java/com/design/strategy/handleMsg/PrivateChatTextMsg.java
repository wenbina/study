package com.design.strategy.handleMsg;

/**
 * @Author: w
 * @Date: 2021/7/28 9:06
 * 私聊文本信息
 */
public class PrivateChatTextMsg implements HandleMsg {

    @Override
    public String handle() {
        return "您有一天信息未阅读...";
    }
}
