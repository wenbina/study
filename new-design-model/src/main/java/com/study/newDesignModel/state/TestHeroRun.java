package com.study.newDesignModel.state;

import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/26 10:35
 */
public class TestHeroRun {

    public static void main(String[] args) {
        Hero hero = new Hero();
        hero.startRun();
        // 加速跑动
        RunState speedUpState = new SpeedUpState();
        speedUpState.run(hero);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 减速跑动
        RunState speedDownState = new SpeedDownState();
        speedDownState.run(hero);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 眩晕
        RunState swimState = new SwimState();
        swimState.run(hero);
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // 停止跑动
        hero.stopRun();
    }
}
