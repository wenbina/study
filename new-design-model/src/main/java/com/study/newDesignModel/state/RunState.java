package com.study.newDesignModel.state;

/**
 * @Author: w
 * @Date: 2021/9/26 9:16
 * 跑动状态
 */
public interface RunState {

    void run(Hero hero);
}
