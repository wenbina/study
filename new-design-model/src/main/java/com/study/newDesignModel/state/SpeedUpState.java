package com.study.newDesignModel.state;

/**
 * @Author: w
 * @Date: 2021/9/26 9:17
 * 加速跑动状态
 */
public class SpeedUpState implements RunState {

    /**
     * 默认加速跑4秒之后切换回正常跑状态
     */
    @Override
    public void run(Hero hero) {
        System.out.println("--------------加速跑动---------------");
        try {
            Thread.sleep(4000);//假设加速持续4秒
        } catch (InterruptedException e) {}
        hero.setState(Hero.COMMON);
        System.out.println("------加速状态结束，变为正常状态------");
    }

}
