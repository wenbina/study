package com.study.newDesignModel.state;

/**
 * @Author: w
 * @Date: 2021/9/26 10:27
 */
public class SpeedDownState implements RunState {

    @Override
    public void run(Hero hero) {
        System.out.println("--------------减速跑动---------------");
        try {
            Thread.sleep(3000);//假设减速持续3秒
        } catch (InterruptedException e) {}
        hero.setState(Hero.COMMON);
        System.out.println("------减速状态结束，变为正常状态------");
    }
}
