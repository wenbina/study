package com.netty.nio.fileChannel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author: w
 * @Date: 2021/9/6 8:29
 * 文件拷贝
 * 需求：
 * 拷贝一个指定文件到另一个指定目录中
 * 实现：
 */
public class CopyDocument {

    public static void main(String[] args) {
        try {
            // 创建文件输入流
            FileInputStream fileInputStream = new FileInputStream("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\fileChannel\\1.txt");
            FileChannel fileInChannel = fileInputStream.getChannel();
            // 创建文件输出流
            FileOutputStream fileOutputStream = new FileOutputStream("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\fileChannel\\2.txt");
            FileChannel fileOuChannel = fileOutputStream.getChannel();
            // 创建byteBuffer
            ByteBuffer byteBuffer = ByteBuffer.allocate(64);
            // byteBuffer从channel中读取数据
            fileInChannel.read(byteBuffer);
            // 切换
            byteBuffer.flip();
            // 从byteBuffer写入数据到channel中
            fileOuChannel.write(byteBuffer);
            // 关闭
            fileOutputStream.close();
            fileInputStream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
