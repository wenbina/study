package com.netty.nio.fileChannel.socket;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @Author: w
 * @Date: 2021/9/6 8:43
 */
public class Client {

    public void startClient(String address, Integer port) {
        try {
            Socket socket = new Socket(address, port);
            // 创建输出流，发送需要下载的文件名
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write("1.txt".getBytes());

            // 创建输入流读取下载的文件
            InputStream inputStream = socket.getInputStream();
            byte[] bytes = new byte[64];
            inputStream.read(bytes);

            FileOutputStream fileOutputStream = new FileOutputStream("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\fileChannel\\1copy.txt");
            fileOutputStream.write(bytes);

            // 关闭资源
            fileOutputStream.close();
            socket.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
