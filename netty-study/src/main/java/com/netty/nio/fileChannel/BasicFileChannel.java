package com.netty.nio.fileChannel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author: w
 * @Date: 2021/9/5 16:42
 * 通过buteBuffer和FileChannel将数据写入到一个文件中
 */
public class BasicFileChannel {

    public static void main(String[] args) {
        //writeMethod();
        readMethod();
    }

    // 写入数据
    private static void writeMethod() {
        String str = "hello，FileChannel";
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\fileChannel\\file.txt");
            // 创建FileChannel
            FileChannel channel = fileOutputStream.getChannel();
            // 创建byteBuffer
            ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
            // 将数据写入到byteBuffer中
            byteBuffer.put(str.getBytes());
            // 切换模式
            byteBuffer.flip();
            // 将byteBuffer写入Channel中
            channel.write(byteBuffer);
            // 关闭资源
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 读取数据
    private static void readMethod() {
        try {
            File file = new File("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\fileChannel\\file.txt");
            FileInputStream fis = new FileInputStream(file);
            // 创建channel
            FileChannel channel = fis.getChannel();
            // 创建缓冲区
            ByteBuffer byteBuffer = ByteBuffer.allocate((int)file.length());
            // 将channel中的数据读取到byteBuffer
            channel.read(byteBuffer);
            System.out.println(new String(byteBuffer.array()));
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
