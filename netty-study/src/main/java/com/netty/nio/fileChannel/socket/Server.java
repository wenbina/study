package com.netty.nio.fileChannel.socket;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author: w
 * @Date: 2021/9/6 8:43
 * 服务端读取客户端需要的文件后写入到socket通道中
 */
public class Server {

    // 启动服务端
    public void startServer(Integer port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            Socket socket = serverSocket.accept();
            InputStream inputStream = socket.getInputStream();
            // 获取客户端发送的消息
            byte[] bytes = new byte[64];
            int read = inputStream.read(bytes);
            String filePath = new String(bytes,0,read);


            // 创建文件读取流
            FileInputStream fileInputStream = new FileInputStream("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\fileChannel\\" + filePath);
            // 创建通道
            FileChannel channel = fileInputStream.getChannel();
            // 创建byteBuffer
            ByteBuffer byteBuffer = ByteBuffer.allocate(64);
            // 将channel中的数据读取到byteBuffer
            channel.read(byteBuffer);
            // 切换
            byteBuffer.flip();
            // 通过socket创建输出流
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(byteBuffer.array());

            // 关闭资源
            socket.close();
            fileInputStream.close();
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }

    }
}
