package com.netty.nio.buffer;

import java.nio.IntBuffer;

/**
 * @Author: w
 * @Date: 2021/9/5 15:53
 */
public class BasicBuffer {

    public static void main(String[] args) {
        // 创建一个buffer：设置一个容量为5的buffer
        IntBuffer intBuffer = IntBuffer.allocate(5);

        for (int i = 0; i < intBuffer.capacity(); i++) {
            // 向buffer中放数据
            intBuffer.put(i * 2);
        }

        // 注意：写完数据之后需要切换为读模式
        intBuffer.flip();

        // 取数据
        while (intBuffer.hasRemaining()) {
            // get方法会将下标往后移
            System.out.println(intBuffer.get());
        }

    }
}
