package com.netty.nio.byteBuffer;

import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * @Author: w
 * @Date: 2021/9/8 22:40
 * 通过byteBuffer和channel来读取文件
 *
 * flip方法：反转缓冲区。首先将限制设置为当前位置，然后将位置设置为 0。如果已定义了标记，则丢弃该标记。 常与compact方法一起使用。通常情况下，在准备 从缓冲区中读取数据 时调用flip方法。
 * limit = position;
 * position = 0;
 * mark = -1;
 * return this;
 *
 * clear方法：clear方法将缓冲区清空，一般是在 重新写缓冲区 时调用
 * position = 0;
 * limit = capacity;
 * mark = -1;
 * return this;
 *
 * byteBuffer初始状态是写入模式
 *
 * 使用步骤
 * 1：向buteBuffer写入数据：调用channel.read(buffer)
 * 2：调用flip切换至读模式
 * 3：从buffer读取数据，调用buffer.get
 * 4：调用clear或compact切换至写模式
 *
 */
public class BasicByteBuffer {

    public static void main(String[] args) {
        readDocument();
    }

    private static void readDocument() {
        try {
            FileInputStream fileInputStream = new FileInputStream("E:\\ownspace\\study\\netty-study\\src\\main\\resources\\byteBuffer\\data.txt");
            // 创建byteBuffer
            ByteBuffer buffer = ByteBuffer.allocate(10);
            // 获取channel
            FileChannel channel = fileInputStream.getChannel();
            while (true) {
                // 将channel中的数据读取到buffer中
                int read = channel.read(buffer);
                if (read == -1) {
                    break;
                }
                // 切换至读模式
                buffer.flip();
                while (buffer.hasRemaining()) {
                    byte b = buffer.get();
                    System.out.print((char) b);
                }
                // buffer读完10个字节，切换至写模式
                buffer.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}
