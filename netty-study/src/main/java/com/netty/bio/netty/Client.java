package com.netty.bio.netty;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

/**
 * @Author: w
 * @Date: 2021/9/5 11:18
 */
public class Client {

    public void startClient(String address,Integer port) {
        try {
            Socket socket = new Socket(address, port);
            //TimeUnit.SECONDS.sleep(10);
            this.writeHandler(socket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 写处理器事件
    private void writeHandler(Socket socket) {
        try {
            OutputStream outputStream = socket.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(outputStream);
            oos.writeObject("hello");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
