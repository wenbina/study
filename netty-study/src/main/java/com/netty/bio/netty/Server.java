package com.netty.bio.netty;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @Author: w
 * @Date: 2021/9/5 11:18
 */
public class Server {

    public void startServer(Integer port) {
        // 创建线程池
        ExecutorService pool = Executors.newCachedThreadPool();

        try {
            ServerSocket serverSocket = new ServerSocket(port);

            System.out.println("服务器启动了");
            while (true) {
                // 与客户端建立连接
                final Socket socket = serverSocket.accept();
                System.out.println("客户端连接成功");
                // 创建一个新线程进行通信
                pool.submit(() -> {
                    this.readHandler(socket);
                });
            }
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    // 读事件处理器
    private void readHandler(Socket socket) {
        try {
            InputStream inputStream = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(inputStream);
            Object msg = ois.readObject();
            System.out.println(msg);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }

    }
}
