package com.netty.bio.test;

import com.netty.bio.netty.Server;

/**
 * @Author: w
 * @Date: 2021/9/5 11:27
 */
public class TestServer {

    public static void main(String[] args) {
        Server server = new Server();
        server.startServer(6666);
    }
}
