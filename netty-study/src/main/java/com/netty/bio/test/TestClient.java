package com.netty.bio.test;

import com.netty.bio.netty.Client;

/**
 * @Author: w
 * @Date: 2021/9/5 11:26
 */
public class TestClient {

    public static void main(String[] args) {
        Client client = new Client();
        client.startClient("localhost",6666);
    }
}
