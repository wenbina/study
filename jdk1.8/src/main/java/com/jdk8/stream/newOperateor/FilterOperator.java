package com.jdk8.stream.newOperateor;

import cn.hutool.core.collection.CollectionUtil;
import com.jdk8.stream.entity.Employee;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: w
 * @Date: 2021/8/16 14:46
 * 过滤操作：对于集合钟的某些数据，我们可以先进行过滤然后进行相关操作
 * 如：我们可以过滤map集合中value值为空的数据
 * 分两种情况：
 * 1：基本类型集合：过滤某些数据不符合的数据形成新集合
 * 2：对象：过滤对象某个字段不符合条件
 */
public class FilterOperator {

    public static void main(String[] args) {
        filterBasic();
        System.out.println("");
    }

    /**
     * 过滤集合中为奇数的数据然后形成新集合
     */
    private static void filterBasic() {
        // 初始化集合
        List<Integer> nums = initData();
        List<Integer> newNums = nums.stream().filter(num -> num % 2 == 0)
                .collect(Collectors.toList());
        if (CollectionUtil.isNotEmpty(newNums)) {
            newNums.forEach(System.out :: print);
        }
    }

    /**
     * 过滤工资小于6000和年纪大于23的员工，形成新集合
     */
    private static void filterEmployee() {
        List<Employee> employees = initObjectData();
        employees.stream().filter(employee -> employee.getAge() <= 23);
    }

    // 初始化集合
    private static List<Integer> initData() {
        return Arrays.asList(2,4,6,8,1,3,5,7);
    }

    // 初始化集合
    private static List<Employee> initObjectData() {
        List<Employee> maps = new ArrayList<>();
        Employee employee1 = new Employee();
        employee1.setName("张三");
        employee1.setSalary(5000);
        employee1.setAge(22);
        maps.add(employee1);

        Employee employee2 = new Employee();
        employee2.setName("李四");
        employee2.setSalary(6000);
        employee2.setAge(23);
        maps.add(employee2);

        Employee employee3 = new Employee();
        employee3.setName("王五");
        employee3.setSalary(7000);
        employee3.setAge(24);
        maps.add(employee3);
        return maps;
    }
}
